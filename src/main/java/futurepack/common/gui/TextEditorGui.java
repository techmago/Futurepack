package futurepack.common.gui;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.depend.api.helper.HelperComponent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Font;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.client.gui.narration.NarratableEntry;
import net.minecraft.client.gui.narration.NarrationElementOutput;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.resources.ResourceLocation;

public class TextEditorGui extends AbstractTextEditorGui implements NarratableEntry
{
	private Font font;
	
	private int width, height;
	private int lastX, lastY; 
	private int scrolled = 0;
	private int posX, posY;
	
	private int visibleLines;
	private final FormatedTextEditor editor;
	
	private ResourceLocation fontTexture;
	
	public TextEditorGui(int width, int height, FormatedTextEditor editor)
	{
		this.width = width;
		this.height = height;
		this.editor = editor;
		
		setFontRenderer(Minecraft.getInstance().font);
		setFont(HelperComponent.getUnicodeFont());
	}
	
	public void setPosition(int x, int y)
	{
		this.posX = x;
		this.posY = y;
	}
	
	public void setFontRenderer(Font font)
	{
		this.font = font;
		visibleLines = height / getLineHeight();
	}
	
	public void setFont(ResourceLocation fontTexture)
	{
		this.fontTexture = fontTexture;
	}
	
	@Override
	public FormatedTextEditor getEditor() 
	{
		return editor;
	}

	@Override
	public int getHoveredLine(double x, double y) 
	{
		y -= lastY;
		int line = (int) (y / getLineHeight());
		int base =  scrolled + line;
		int max = Math.max(0, getEditor().getLines() - 1);
		return Math.min(base, max);
	}

	@Override
	public int getHoveredLinePos(double x, double y) 
	{
		//for now binary search;
		x -= lastX;		
		int lineNum = getHoveredLine(x, y);
		String text = getEditor().getRawLine(lineNum);// getLineFormatted(lineNum);
		
		if( x >= widthString(text))
			return text.length();
		
		int relWidth = 0;
		int relPos = 0;
		int len;
		while( (len=text.length()) >= 2)
		{
			int split = len / 2;
			String p1 = text.substring(0, split);
			String p2 = text.substring(split);
			
			int w1 = relWidth + widthString(p1);
			
			if(x <= w1)
			{
				text = p1;
			}
			else
			{
				text = p2;
				relWidth = w1;
				relPos += split;
			}
		}
		
		return relPos;
	}

	@Override
	public boolean isInBounds(double mouseX, double mouseY) 
	{
		return HelperComponent.isInBox(mouseX, mouseY, lastX, lastY, lastX+width, lastY+height);
	}
	
	public void render(PoseStack matrixStack, int x, int y)
	{
		lastX = x;
		lastY = y;
		
		GuiComponent.fill(matrixStack, x-1, y-1, x+width+1, y+height+1, 0xFF101010);//black background
		
		int cx=x,cy=y;
		cy += (cursorLine-scrolled)*getLineHeight();
		GuiComponent.fill(matrixStack, cx, cy, cx+width, cy+getLineHeight(), 0xFF555555);//dark gray background for cursor-line
		
		if(cursorLine -scrolled < 0)
		{
			scrolled = cursorLine;
		}
		else if(cursorLine -scrolled >= visibleLines)
		{
			scrolled = 1+cursorLine -visibleLines;
		}
		
		for(int i=0;i<visibleLines;i++)
		{
			if(getEditor().getLines() == scrolled+i)
				break;
			String s = getEditor().getLineFormatted(scrolled+i);
			drawString(matrixStack, s, x, y+i*getLineHeight(), 0xFFFFFF);
			if(cursorLine == scrolled+i)
			{
				if(getEditor().getLines() > cursorLine)
				{
					s = getEditor().getRawLine(scrolled+i);
					try
					{
						cx += widthString(s.substring(0, cursorPos));
					}
					catch (StringIndexOutOfBoundsException e)
					{
						int len = s.length();
						int c = cursorPos;
						e.printStackTrace();
						
						if(c > len)
							cursorPos = len;
					}
				}
				else
				{
					cursorLine = Math.max(0, getEditor().getLines() -1);
				}
			}
			
		}
		
		if(isSelected())
		{
			final int white = 0xFFFFFFFF;
			final int black = 0xFF000000;
			TextArea selected = getSelected();
			
			if(selected.beginLine != selected.endLine)
			{
				String s1,s2;
				s1 = getEditor().getRawLine(selected.beginLine).substring(0, selected.beginPos);
				s2 = getEditor().getRawLine(selected.beginLine).substring(selected.beginPos);
				int xpos = x + widthString(s1);
				int ypos = (selected.beginLine-scrolled)*getLineHeight();
				if(ypos >= 0)
				{
					ypos += y;
					GuiComponent.fill(matrixStack, xpos, ypos, x+width, ypos+getLineHeight(), white);
					drawString(matrixStack, s2, xpos, ypos, black);
				}
				for(int line = selected.beginLine+1;line<=selected.endLine;line++)
				{
					ypos = (line-scrolled)*getLineHeight();
					if(ypos<0)
						continue;
					else
						ypos += y;
					GuiComponent.fill(matrixStack, x, ypos, x+width, ypos+getLineHeight(), white);
					drawString(matrixStack, getEditor().getRawLine(line), x, ypos, black);
				}
				s1 = getEditor().getRawLine(selected.endLine).substring(0, selected.endPos);
				s2 = getEditor().getRawLine(selected.endLine).substring(selected.endPos);
				xpos = x +widthString(s1);
				ypos = (selected.endLine-scrolled)*getLineHeight();
				if(ypos >= 0)
				{
					ypos += y;
					GuiComponent.fill(matrixStack, x, ypos, xpos, ypos+getLineHeight(), white);
//					font.drawString(matrixStack, s2, x, y, black);
					drawString(matrixStack, s1, x, ypos, black);
				}
				
			}
			else
			{
				String s1,s2;
				String text = getEditor().getRawLine(selected.beginLine);
				s1 = text.substring(0, selected.beginPos);
				s2 = text.substring(selected.beginPos, selected.endPos);
				
				int ypos = (selected.beginLine-scrolled)*getLineHeight();
				if(ypos>0)
				{
					ypos+= y;
					int xpos1 = x+widthString(s1);
					int xpos2 = xpos1 + widthString(s2);
					
					GuiComponent.fill(matrixStack, xpos1, ypos, xpos2, ypos+getLineHeight(), white);
					drawString(matrixStack, s2, xpos1, ypos, black);
				}
			}
		}
		
		if(isFocused())
		{
			//render cursor
			if( ((System.currentTimeMillis()/750) % 2) == 0 )
			GuiComponent.fill(matrixStack, cx, cy, cx+2, cy+getLineHeight(), 0x88FFFFFF);//transparent
		}
	}
	
	private void drawString(PoseStack stack, String text, float x, float y, int color)
	{
		font.draw(stack, applyFontStyle(text), x, y, color);
	}
	
	private Component applyFontStyle(String text)
	{
		return new TextComponent(text).setStyle(Style.EMPTY.withFont(fontTexture));
	}
	
	private int widthString(String text)
	{
		return font.width(applyFontStyle(text));
	}
	
	private int getLineHeight() 
	{
		return font.lineHeight;
	}
	

	@Override
	public boolean mouseScrolled(double x, double y, double dweel) 
	{
		if(isMouseOver(x, y))
		{
			if(dweel < 0)
			{
				do
				{
					if(scrolled + visibleLines < getEditor().getLines())
					{
						scrolled++;
						cursorLine++;
						cursorPos = Math.min(cursorPos, getEditor().getRawLine(cursorLine).length());
					}
					else
						break;
					
					dweel +=1D;
				}
				while(dweel <= -1);
			}
			else
			{
				do
				{
					if(scrolled > 0)
					{
						scrolled--;
						cursorLine--;
						cursorPos = Math.min(cursorPos, getEditor().getRawLine(cursorLine).length());
					}
					else
						break;
					
					dweel -= 1D;
				}
				while(dweel >= 1);
			}
		}
		return false;
	}


	@Override
	public void render(PoseStack matrixStack, double mouseX, double mouseY, float partialTicks) 
	{
		checkBounds();
		render(matrixStack, posX, posY);
	}
	
	private void checkBounds()
	{
		if(cursorLine >= getEditor().getLines())
			cursorLine = getEditor().getLines()-1;
		
		if(cursorLine < 0)
			cursorLine = 0;		
		
		int l = getEditor().getRawLine(cursorLine).length();
		
		if(cursorPos > l)
			cursorLine = l;
		else if(cursorPos < 0)
			cursorPos = 0;
	}

	@Override
	public void updateNarration(NarrationElementOutput pNarrationElementOutput)
	{	
	}

	@Override
	public NarrationPriority narrationPriority()
	{
		return NarrationPriority.NONE;
	}
}
