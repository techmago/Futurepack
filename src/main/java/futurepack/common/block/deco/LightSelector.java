package futurepack.common.block.deco;

import java.util.HashMap;
import java.util.Map;

import futurepack.api.ParentCoords;
import futurepack.api.interfaces.IBlockSelector;
import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.Vec3;

public class LightSelector implements IBlockSelector
{
	private final BlockPos start;
	private Map<Vec3, BlockPos> raytraceMap;
	private Map<BlockPos, Boolean> isValid;
	
	public LightSelector(BlockPos pos) 
	{
		start = pos;
		raytraceMap = new HashMap<>();
		isValid = new HashMap<>();
	}

	@Override
	public boolean isValidBlock(Level w, BlockPos pos, Material m, boolean diagonal, ParentCoords parent) 
	{
		if(diagonal)
			return false;
		if(isValidBase(w, pos))
		{
			return hasLineOfSightToStart(w, pos, diagonal);
		}
		return false;
	}
	
	@Override
	public boolean canContinue(Level w, BlockPos pos, Material m, boolean diagonal, ParentCoords parent) 
	{
		return start.distSqr(pos) < (15*15);
	}
	
	private boolean isValidBase(Level w, BlockPos pos)
	{
		return isValid.computeIfAbsent(pos, p ->
		{
			BlockState state = w.getBlockState(p);
			boolean glassy = state.getLightBlock(w, p) < 10;
			return glassy;
		});
	}
	
	public boolean hasLineOfSightToStart(Level w, BlockPos pos, boolean dia)
	{
		Vec3 vec = Vec3.atLowerCornerOf(pos.subtract(start));
		BlockPos hit = getRayHit(vec, w);
		
		double disHit = start.distSqr(hit);
		double disBlock = start.distSqr(pos);
		
		return disBlock < disHit;
	}
	
	@SuppressWarnings("unused")
	private BlockPos getBlockHitBase(Vec3 vec, Level w)
	{
		final boolean debug = false;
		
		final Vec3 start = new Vec3(this.start.getX()+0.5, this.start.getY()+0.5, this.start.getZ()+0.5);
		Vec3 test = start;
		double dis = 0;
		BlockPos lastP = this.start, pos = this.start;
		while(dis <= 15*15)
		{
			test = test.add(vec);
			dis = start.distanceToSqr(test);
			lastP = pos;
			pos = new BlockPos(test);
			if(pos.equals(lastP))
				continue;
			
			if(!isValidBase(w, pos))
			{
				break;
			}
		}
		
		if(debug && !w.isClientSide)
		{
			((ServerLevel)w).sendParticles(ParticleTypes.NOTE, test.x, test.y, test.z, 1, 0D, 0D, 0D, 0D);
		}
		return pos;
	}
	
	public BlockPos getRayHit(Vec3 vec, Level w)
	{
		vec = vec.normalize().scale(0.5);
		BlockPos d = raytraceMap.get(vec);
		if(d == null)
		{
			d = getBlockHitBase(vec, w);
			raytraceMap.put(vec, d);
			return d;
		}
		else
		{
			return d;
		}
	}
	
	
}
