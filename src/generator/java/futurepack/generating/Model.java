package futurepack.generating;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

class Model
{
	String parent_model;
	
	Map<String, String> textures;
	
	public Model(String parent, Map<String, String> textures)
	{
		parent_model = parent;
		this.textures = textures;
	}
	
	public Model(JsonElement json, JsonDeserializationContext context)
	{
		textures = new HashMap<String, String>();
		if(json.isJsonObject())
		{
			JsonObject obj = json.getAsJsonObject();
			if(obj.has("parent"))
			{
				parent_model = obj.get("parent").getAsString();
			}
			else
			{
				parent_model = "block/cube_all";
			}
			if(obj.has("textures"))
			{
				textures = context.deserialize(obj.get("textures"), Map.class);
			}
		}
		else if(json.isJsonPrimitive())
		{
			parent_model = "block/cube_all";
			textures = new HashMap<String, String>();
			textures.put("all", json.getAsString());
		}
		else
		{
			throw new IllegalArgumentException("Json Elemnt was no object nor String: " + json);
		}
	}
	
	public JsonObject asJson()
	{
		JsonObject obj = new JsonObject();
		obj.addProperty("parent", parent_model);
		if(textures!=null)
		{
			JsonElement textures = Blocks.gson.toJsonTree(this.textures);
			obj.add("textures", textures);
		}
		return obj;
	}
}