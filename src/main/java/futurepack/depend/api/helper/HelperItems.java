package futurepack.depend.api.helper;

import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.function.Predicate;

import futurepack.api.interfaces.IItemNeon;
import futurepack.api.interfaces.IItemSupport;
import futurepack.common.block.ItemMoveTicker;
import net.minecraft.core.Registry;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IForgeRegistryEntry;

public class HelperItems
{
	public static Component getTooltip(ItemStack it, IItemNeon neon)
	{
		return new TranslatableComponent("tooltip.futurepack.item.neon", neon.getNeon(it), neon.getMaxNeon(it));
	}

	public static Component getTooltip(ItemStack it, IItemSupport sp)
	{
		return new TranslatableComponent("tooltip.futurepack.item.support", sp.getSupport(it), sp.getMaxSupport(it));
	}

	public static void moveItemTo(Level world, Vec3 destination, Vec3 itemPos)
	{
		if(world.isClientSide)
			throw new IllegalStateException("This method is not allowed on Clients!");

		new ItemMoveTicker(world, destination, itemPos);
	}

	public static void disableItemSpawn()
	{
		HelperEntities.disableItemSpawn();
	}

	public static void enableItemSpawn()
	{
		HelperEntities.enableItemSpawn();
	}

	public static ResourceLocation getRegistryName(Item item)
	{
		return getName(ForgeRegistries.ITEMS, item) ;
	}

	public static ResourceLocation getRegistryName(Block block)
	{
		return getName(ForgeRegistries.BLOCKS, block) ;
	}

	public static ResourceLocation getRegistryName(Biome biome)
	{
		return getName(ForgeRegistries.BIOMES, biome) ;
	}

	public static ResourceLocation getRegistryName(BlockEntityType<?> tile)
	{
		return getName(ForgeRegistries.BLOCK_ENTITIES, tile) ;
	}

	public static ResourceLocation getRegistryName(Enchantment zauber)
	{
		return getName(ForgeRegistries.ENCHANTMENTS, zauber) ;
	}

	public static ResourceLocation getRegistryName(EntityType<?> entities)
	{
		return getName(ForgeRegistries.ENTITIES, entities) ;
	}

	public static ResourceLocation getRegistryName(SoundEvent sounds)
	{
		return getName(ForgeRegistries.SOUND_EVENTS, sounds) ;
	}

	private static <V extends IForgeRegistryEntry<V>> ResourceLocation getName(IForgeRegistry<V> registry, V entry)
	{
		return names!=null ?  names.getOrDefault(entry, registry.getKey(entry)) : registry.getKey(entry);
	}

	private static Map<Object, ResourceLocation> names = Collections.synchronizedMap(new WeakHashMap<>());

	public static <V extends IForgeRegistryEntry<V>> V setRegistryName(ForgeRegistryEntry<V> forgeRegistryEntry, String modID, String name)
	{
		names.put(forgeRegistryEntry, new ResourceLocation(modID, name));
		return forgeRegistryEntry.setRegistryName(modID, name);
	}

	public static void addItemRemover(Predicate<ItemEntity> stopDrops)
	{
		HelperEntities.addItemRemover(stopDrops);
	}

	public static void removeItemRemover(Predicate<ItemEntity> stopDrops)
	{
		HelperEntities.removeItemRemover(stopDrops);
	}
}
