package futurepack.common.gui.inventory;

import java.awt.Color;
import java.util.Random;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.block.modification.machines.TileEntityImproveComponents;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.SlotUses;
import futurepack.common.modification.PartsImprover;
import futurepack.common.modification.PartsManager;
import futurepack.depend.api.helper.HelperContainerSync;
import futurepack.depend.api.helper.HelperGui;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;

public class GuiImproveComponents extends GuiModificationBase<TileEntityImproveComponents> 
{
	private ParticleEngine2D particles =  new ParticleEngine2D();
	
	private ItemStack lastB = ItemStack.EMPTY, lastW = ItemStack.EMPTY;
	private PartsImprover.EnumPartConfig configB = null, configW = null;
	private float[] valuesB = null, valuesW = null;
	private int lastProgress = -1;
	private float laserX=-1, laserY=-1;
	
	public GuiImproveComponents(Player pl, TileEntityImproveComponents tile) 
	{
		super(new ContainerImproveComponents(pl.getInventory(), tile), "improve_components.png", pl.getInventory());
	}
	
	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY) 
	{
		int progress = tile().getProgress();
		laserX=-1;
		laserY=-1;
		
		super.renderBg(matrixStack, partialTicks, mouseX, mouseY);
		
		
		if(progress < lastProgress)
		{
			lastW = lastB = null;
			configW = configB = null;
			valuesW = valuesB = null;
		}
		
		
		if(tile().isComponentBScanned())
		{
			if(tile().getItem(2) != lastB && !tile().getItem(2).isEmpty())
			{
				configB = PartsImprover.EnumPartConfig.getPartType(PartsManager.getPartFromItem(lastB = tile().getItem(2)));
				valuesB = PartsImprover.analyzePart(lastB, null);
			}
		}
		else if(tile().getScanningB() > 0 && !tile().getItem(2).isEmpty())
		{
			//42, 12;
			PartsImprover.EnumPartConfig configB = PartsImprover.EnumPartConfig.getPartType(PartsManager.getPartFromItem(tile().getItem(2)));
			renderPart(leftPos+42, topPos+12, partialTicks, configB, new float[]{}, 0, 0, matrixStack);
			
			float progressB = progress(tile().getScanningB(), 0, tile().maxScanning);
			progressB = (float) (Math.cos(progressB *2* Math.PI) - 1F) * -0.5F;
			int h = (int) ((70 -4) * progressB);
			blit(matrixStack, leftPos+41, topPos+70 -h, 176, 144, 50, h);
		}
		
		if(tile().isComponentWScanned())
		{
			if(tile().getItem(3) != lastW && !tile().getItem(3).isEmpty())
			{
				configW = PartsImprover.EnumPartConfig.getPartType(PartsManager.getPartFromItem(lastW = tile().getItem(3)));
				valuesW = PartsImprover.analyzePart(lastW, null);
			}
		}
		else if(tile().getScanningW() > 0 && !tile().getItem(3).isEmpty())
		{
			PartsImprover.EnumPartConfig configW = PartsImprover.EnumPartConfig.getPartType(PartsManager.getPartFromItem(tile().getItem(3)));
			renderPart(leftPos+91, topPos+12, partialTicks, configW, new float[]{}, 0, 0, matrixStack);
			
			float progressW = progress(tile().getScanningW(), 0, tile().maxScanning);
			progressW = (float) (Math.cos(progressW *2* Math.PI) - 1F) * -0.5F;
			int h = (int) ((70 -4) * progressW);
			blit(matrixStack, leftPos+90, topPos+70 -h, 176, 144, 50, h);
		}
		
		if(configB!=null)
		{
//			float progressB = Math.min(0F, Math.max(1F, progress / 65F));
			float progressB = progress(progress, 0, 65);
//			progressB -= Math.min(0F, Math.max(1F, (progress - (tile().maxProgress - 65)) / 65F));
			progressB -= progress(progress, tile().maxProgress - 65, tile().maxProgress);
			
			//42, 12;
			renderPart(leftPos+42, topPos+12, partialTicks, configB, valuesB, tile().getComponentB(), progressB, matrixStack);
		}
		if(configW!=null)
		{
//			float progressW = Math.min(0F, Math.max(1F, (progress- 75) / 65F));
			float progressW = progress(progress, 75, 75+65);
//			progressW -= Math.min(0F, Math.max(1F, (progress - tile().maxProgress + 65 + 75) / 65F));
			progressW -= progress(progress, tile().maxProgress - 65 - 75, tile().maxProgress -75);
			//91, 12
			renderPart(leftPos+91, topPos+12, partialTicks, configW, valuesW, tile().getComponentW(), progressW, matrixStack);
		}
		
		
		//arm standby pos
		int xA = leftPos + 161;
		int yA = topPos + 60;
		
		if(configW!=null && configB !=null && valuesW != null && valuesB != null)
		{
			int[] xyB = configB.getXYofComponent(tile().getComponentB());
			int[] xyW = configW.getXYofComponent(tile().getComponentW());
			int x0 = xyB[0] + leftPos + 42;
			int y0 = xyB[1] + topPos + 12;
			int x1 = xyW[0] + leftPos + 91;
			int y1 = xyW[1] + topPos + 12;
			
			if(progress >= 2*70+55) // render values switched
			{
				int[] xy = configB.getXYofComponent(tile().getComponentB());
				int color = PartsImprover.EnumPartConfig.getColor(valuesW[tile().getComponentW()]);
				
				int px = leftPos+42 + xy[0];
				int py = topPos+12 + xy[1];
				
				fill(matrixStack, px+1, py+1, px+5, py+5, color);
				
				xy = configW.getXYofComponent(tile().getComponentW());
				color = PartsImprover.EnumPartConfig.getColor(valuesB[tile().getComponentB()]);
				
				px = leftPos+91 + xy[0];
				py = topPos+12 + xy[1];
				
				fill(matrixStack, px+1, py+1, px+5, py+5, color);
				RenderSystem.setShaderColor(1, 1, 1, 1);
			}
			
			if(laserX > 0 && laserY > 0)
			{
				GlStateManager._enableBlend();
				blit(matrixStack, (int)(laserX-3), (int)(laserY-3), 240, 0, 6, 2+ (topPos+70)-(int)(laserY-3));
				GlStateManager._disableBlend();
			}
			
//			if(progress < 2*70 || progress > tile().maxProgress - 2*70)// render cutter arm
//			{
//				float progressutter = progress(progress, 65, 75);
//				progressutter -= progress(progress, tile().maxProgress - 75, tile().maxProgress - 65);
//				
//				float x2 = x0 + (x1-x0) * progressutter;
//				float y2 = y0 + (y1-y0) * progressutter + 6;
//				
//				PartRenderer.fill(x2-2, y2, x2+8, y2+8, 0xFFFF00FF);
//				GlStateManager.color4f(1, 1, 1, 1);
//			}
			
			float progressArm1 = progress(progress, 2*70, 2*70 + 20);
			//wait 5
			float progressArm2 = progress(progress, 2*70 + 25, 2*70 + 25 + 20);
			//wait 10
			progressArm2 -= progress(progress, 2*70+55, 2*70+55 +20);
			//wait 5
			progressArm1 -= progress(progress, 2*70+80, 2*70+80+20);
			
			if(progress > 2*70 + 20 && progress < 2*70+80)
			{
				HelperGui.fill(matrixStack, x1, y1, x1+6, y1+6, 0xFF111111, 0);
				RenderSystem.setShaderColor(1, 1, 1, 1);
			}
			float x2 = xA + (x1-xA) * progressArm1 + (x0 - x1) * progressArm2 + 0.0001F;
			float y2 = yA + (y1-yA) * progressArm1 + (y0 - y1) * progressArm2 + 0.0001F;
			
			x2-=1;
			y2-=1;
			
			this.blit(matrixStack, (int)x2, topPos+4, 224, 0, 8, 68);
			this.blit(matrixStack, (int)x2, (int)y2, 232, 0, 8, 8);
		}
		else
		{
			this.blit(matrixStack, xA, topPos+4, 224, 0, 8, 68);
			this.blit(matrixStack, xA, yA, 232, 0, 8, 8);
		}
		//the yellow-black stripes
		this.blit(matrixStack, leftPos+22, topPos+4, 0, 176, 147, 2);
		this.blit(matrixStack, leftPos+22, topPos+70, 0, 166, 147, 9);
		
		this.lastProgress = progress;
		
		particles.render(partialTicks);
	}
	
	private float progress(int progress, float start, float end)
	{
		float delta = (end - start);
		float p = progress-start;
		float d = p / delta;
		return Math.min(1F, Math.max(0F, d));
	}
	
	private void renderPart(int x, int y, float partialTicks, PartsImprover.EnumPartConfig config, float[] values, int component, float progress, PoseStack matrixStack)
	{
		int w=48, h=48;
		int mx = 176, my = 48 * config.ordinal();
		
		this.blit(matrixStack, x, y, mx, my, w, h);
		
		for(int i=0;i<values.length;i++)
		{
			int[] xy = config.getXYofComponent(i);
			int color = PartsImprover.EnumPartConfig.getColor(values[i]) & 0xC0FFFFFF;
			
			int px = x + xy[0];
			int py = y + xy[1];
			
			fill(matrixStack, px, py, px+6, py+6, color);
			
			if(i == component)
			{
				float fx = px;
				float fy = py;
				
				float l1 = Math.min(1F, progress * 4 - 0F);
				float l2 = Math.min(1F, progress * 4 - 1F);
				float l3 = Math.min(1F, progress * 4 - 2F);
				float l4 = Math.min(1F, progress * 4 - 3F);
				
				if(l1>0)
					HelperGui.fill(fx, fy, fx+6*l1, fy+1, 0xFF111111, 0);
				if(l2>0)
					HelperGui.fill(fx+5, fy, fx+6, fy+6*l2, 0xFF111111, 0);
				if(l3>0)
					HelperGui.fill(fx+6 -6*l3, fy+5, fx+6, fy+6, 0xFF111111, 0);
				if(l4>0)
					HelperGui.fill(fx, fy+6-6*l4, fx+1, fy+6, 0xFF111111, 0);
				
				if(l1>0 && l4 < 1)
				{
					float ffx=fx+0.5F,ffy=fy+0.5F;
					
					if(l4>0)
					{
						ffy+=+6-6*l4;
					}
					else if(l3>0)
					{
						ffx+=6 -6*l3;
						ffy+=5;
					}
					else if(l2>0)
					{
						ffx+=5;
						ffy+=6*l2;
					}
					else if(l1>0)
					{
						ffx+=6*l1;
					}
					
					laserX = ffx;
					laserY = ffy;
					if(this.minecraft.level.random.nextInt(5) == 0)
						addSparks(ffx, ffy);
				}
			}
		}
		RenderSystem.setShaderColor(1, 1, 1, 1);
	}
	
	@Override
	public void containerTick() 
	{
		super.containerTick();
		particles.tick();
	}
	
	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks) 
	{
		
		
		super.render(matrixStack, mouseX, mouseY, partialTicks);
		
	}

	private void addSparks(float x, float y)
	{
		Random r = this.minecraft.level.random;
		particles.addPaticle(new ParticleEngine2D.Particle2D(true, 20 + r.nextInt(10)).setPostion(x, y).setMotion(((float)r.nextGaussian() * 0.5F)*2, (r.nextFloat() * -0.9F)*2).setColor(Color.HSBtoRGB((float)(30F+r.nextFloat()*30F)/ 360F, 0.5F + r.nextFloat() * 0.5F, 0.8F + r.nextFloat()*0.2F)));
	}
	
	@Override
	public TileEntityImproveComponents tile() 
	{
		return ((ContainerImproveComponents)getMenu()).tile;
	}
	
	public static class ContainerImproveComponents extends ContainerSyncBase
	{
		TileEntityImproveComponents tile;

		public ContainerImproveComponents(Inventory inv, TileEntityImproveComponents tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			
			this.addSlot(new SlotUses(tile, 0, 23, 8, 1));
			this.addSlot(new SlotUses(tile, 1, 142, 8, 1));
			this.addSlot(new SlotUses(tile, 2, 23, 28, 1));
			this.addSlot(new SlotUses(tile, 3, 142, 28, 1));
			this.addSlot(new SlotUses(tile, 4, 23, 48, 1));
			this.addSlot(new SlotUses(tile, 5, 142, 48, 1));

			HelperContainerSync.addInventorySlots(8, 84, inv, this::addSlot);
		}

		@Override
		public boolean stillValid(Player playerIn)
		{
			return HelperResearch.isUseable(playerIn, tile);
		}
		
		@Override
		public ItemStack quickMoveStack(Player playerIn, int index)
		{
			//TODO: add shift click to improver
			return ItemStack.EMPTY;
		}
	}



	
}
