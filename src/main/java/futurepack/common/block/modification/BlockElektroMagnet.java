package futurepack.common.block.modification;

import futurepack.api.interfaces.IBlockBothSidesTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class BlockElektroMagnet extends BlockRotateableTile implements IBlockBothSidesTickingEntity<TileEntityElektroMagnet>
{

	public BlockElektroMagnet(Block.Properties props) 
	{
		super(props);
	}

	@Override
	public BlockEntityType<TileEntityElektroMagnet> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.ELECTRO_MAGNET;
	}
	

}
