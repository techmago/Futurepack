package futurepack.depend.api.helper;

import java.util.function.BiConsumer;
import java.util.function.IntFunction;

import futurepack.api.FacingUtil;
import futurepack.api.interfaces.IChunkAtmosphere;
import futurepack.api.interfaces.IPlanet;
import futurepack.common.FPLog;
import futurepack.common.fluids.FPFluids;
import futurepack.common.spaceships.FPPlanetRegistry;
import futurepack.world.dimensions.atmosphere.AtmosphereManager;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraft.world.level.material.Fluid;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.FluidActionResult;
import net.minecraftforge.fluids.FluidAttributes;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidUtil;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler.FluidAction;
import net.minecraftforge.fluids.capability.IFluidHandlerItem;
import net.minecraftforge.items.IItemHandlerModifiable;

public class HelperFluid 
{
	public static boolean isFluidInside(Fluid fluid, ItemStack bucket)
    {
    	LazyOptional<IFluidHandlerItem> handler = getBucketHandler(bucket);
    	if(handler.isPresent())
    	{
    		IFluidHandlerItem fluidH = handler.orElseThrow(NullPointerException::new);
    		FluidStack result =  fluidH.drain(FluidAttributes.BUCKET_VOLUME, FluidAction.SIMULATE);
    		if(result != null)
    		{
    			return result.getFluid() == fluid && result.getAmount() > 0;
    		}
    	}
		return false;
    }
    
    public static LazyOptional<IFluidHandlerItem> getBucketHandler(ItemStack bucket)
    {
    	return bucket.getCapability(CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY);
    }
    
    public final static int MAX_OXYGEN_PER_BLOCK = 36000;
	public final static int MAX_MILIBUCKETS_PER_BLOCK = 1000;
	public final static int OXYGEN_PER_MILIBUCKET = MAX_OXYGEN_PER_BLOCK / MAX_MILIBUCKETS_PER_BLOCK; //36
	
    public static final float oxygen_2_gas_ratio = OXYGEN_PER_MILIBUCKET;
    
    /**
     * 
     * @param fluid in mili buckets
     * @return  the gas amount for atmosphere
     */
    public static int convertOxygenToGas(int fluid)
    {
		return (int) (fluid * oxygen_2_gas_ratio);
    }
    
    /**
     * @param gas the gas amount in the atmosphere
     * @return the fluid amount in mili buckets
     */
    public static int convertGasToOxygen(int gas)
    {
    	return (int) (gas / oxygen_2_gas_ratio);
    }
    
    /**
     * 
     * @param w
     * @param pos
     * @param amount max amount in mili buckets
     * @return
     */
    public static FluidStack removeOxygenFromBlock(ServerLevel w, BlockPos pos, int amount)
    {
    	IPlanet pl = FPPlanetRegistry.instance.getPlanetSafe(w);
		
		if(pl.hasBreathableAtmosphere())
		{
			return new FluidStack(FPFluids.oxygenGasStill, amount);
		}
		else
		{
			int maxGas = convertGasToOxygen(amount);
			if(maxGas <= 0)
			{
				return FluidStack.EMPTY;
			}
			LevelChunk c = w.getChunkAt(pos);
			LazyOptional<IChunkAtmosphere> opt = c.getCapability(AtmosphereManager.cap_ATMOSPHERE, null);
			if(opt.isPresent())
			{
				IChunkAtmosphere atm = opt.orElseThrow(NullPointerException::new);
				int availableAir = atm.getAirAt(pos.getX() & 15,  pos.getY() & 255, pos .getZ() & 15);
				int availableOxy = convertGasToOxygen(availableAir);
				if(availableOxy <= 0)
				{
					return FluidStack.EMPTY;
				}
				int possibleOxy = Math.min(amount, availableOxy);
				int possibleAir = convertOxygenToGas(possibleOxy);
				
				int air = atm.removeAir(pos.getX() & 15,  pos.getY() & 255, pos .getZ() & 15, possibleAir);
				
				if(air != possibleAir)
				{
					FPLog.logger.fatal("Something fucked up and the air I wanted to use is no longer available!");
				}
				int oxy = convertGasToOxygen(air);
				return new FluidStack(FPFluids.oxygenGasStill, oxy);
			}
			else
			{
				return FluidStack.EMPTY;
			}
		}
    }
    
    /**
     * 
     * @param w
     * @param pos
     * @param oxygen
     * @return the oxygen that was not inserted
     */
    public static FluidStack addOxygenToBlock(ServerLevel w, BlockPos pos, FluidStack oxygen)
    {
    	if(oxygen.isEmpty())
    		return oxygen;
    	
    	IPlanet pl = FPPlanetRegistry.instance.getPlanetSafe(w);
		
		if(pl.hasBreathableAtmosphere())
		{
			return FluidStack.EMPTY; //pump out everything
		}
		else
		{
			LevelChunk c = w.getChunkAt(pos);
			LazyOptional<IChunkAtmosphere> opt = c.getCapability(AtmosphereManager.cap_ATMOSPHERE, null);
			if(opt.isPresent())
			{
				IChunkAtmosphere atm = opt.orElseThrow(NullPointerException::new);
				int spaceForAir = atm.getMaxAir() - atm.getAirAt(pos.getX() & 15,  pos.getY() & 255, pos.getZ() & 15);
				int spaceForOxy = convertGasToOxygen(spaceForAir);
				
				int oxy = Math.min(spaceForOxy, oxygen.getAmount());
				int air = convertOxygenToGas(oxy);
				
				int addedAir = atm.addAir(pos.getX() & 15,  pos.getY() & 255, pos.getZ() & 15, air);
				
				if(addedAir != air)
				{
					FPLog.logger.fatal("Something fucked up and the air that should be added could not be added");
				}
				
				int addedOxy = convertGasToOxygen(addedAir);
				int stillThere = oxygen.getAmount() -  addedOxy;
				
				if(stillThere <= 0)
					return FluidStack.EMPTY;
				else
				{
					oxygen = oxygen.copy();
					oxygen.setAmount(stillThere);
					return oxygen;
				}
				
			}
			else
			{
				return oxygen;
			}
		}
    }
    
    
    
    public static void putFluidFromTankInBucket(IntFunction<ItemStack> getter, BiConsumer<Integer, ItemStack> setter, int slotBucketIn, int slotBucketOut, IFluidHandler tank, int maxAmount)
    {
    	if(!getter.apply(slotBucketIn).isEmpty() && getter.apply(slotBucketOut).isEmpty())
		{
			FluidActionResult stack = FluidUtil.tryFillContainer(getter.apply(slotBucketIn), tank, maxAmount, null, true);
			if(stack.success)
			{
				setter.accept(slotBucketIn, ItemStack.EMPTY);
				setter.accept(slotBucketOut, stack.result);
			}
		}
    }
    
    
    public static void putFluidFromTankInBucket(IItemHandlerModifiable handler, int slotBucketIn, int slotBucketOut, IFluidHandler tank, int maxAmount)
    {
    	putFluidFromTankInBucket((IntFunction<ItemStack>)handler::getStackInSlot, (BiConsumer<Integer, ItemStack>)handler::setStackInSlot, slotBucketIn, slotBucketOut, tank, maxAmount);
    }
    
    public static void putFluidFromBucketInTank(IItemHandlerModifiable handler, int slotBucketIn, int slotBucketOut, IFluidHandler tank, int maxAmount)
    {
    	if(!handler.getStackInSlot(slotBucketIn).isEmpty() && handler.getStackInSlot(slotBucketOut).isEmpty())
		{
			FluidActionResult result = FluidUtil.tryEmptyContainer(handler.getStackInSlot(slotBucketIn), tank, maxAmount, null, true);
			if(result.success)
			{
				handler.setStackInSlot(slotBucketIn, ItemStack.EMPTY);
				if(result.result.getCount()>0)
				{
					handler.setStackInSlot(slotBucketOut, result.result);
				}
			}
		}
    }
    
    public static void doFluidExtractAndImport(BlockEntity base, int maxAmount)
    {
    	if(maxAmount>0)
		{
			for(Direction face : FacingUtil.VALUES)
			{	
				IFluidHandler target = FluidUtil.getFluidHandler(base.getLevel(), base.getBlockPos().relative(face), face.getOpposite()).orElse(null);
				IFluidHandler src = base.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, face).orElse(null);
				if(target != null && src!=null)
				{
					if(!FluidUtil.tryFluidTransfer(target, src, maxAmount, true).isEmpty())//extract
					{
						base.setChanged();
					}
					if(!FluidUtil.tryFluidTransfer(src, target, maxAmount, true).isEmpty())//import
					{
						base.setChanged();
					}
				}
			}
		}
    }
}
