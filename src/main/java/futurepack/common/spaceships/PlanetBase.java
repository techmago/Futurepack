package futurepack.common.spaceships;

import futurepack.api.interfaces.IPlanet;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;

public class PlanetBase implements IPlanet
{
	//TODO: komplette atmosphere classe
	//TODO add planet gravity with biome overwrite since we have LivingEntity.ENTITY_GRAVITY
	private final ResourceLocation dimension;
	private final ResourceLocation tex;
	private final String name;
	private final String[] upgrades;

	private boolean canFly = true;
	private boolean breathAble = true;

	private float spreadVelocity = 1F;
	private float gravityVelocity = -0.1F;

	public PlanetBase(ResourceLocation dimension, ResourceLocation tex, String name, String[] upgrades)
	{
		super();
		this.dimension = dimension;
		this.tex = tex;
		this.name = name;
		this.upgrades = upgrades;
	}

	@Override
	public ResourceLocation getDimenionId()
	{
		return dimension;
	}

	@Override
	public ResourceLocation getTexture()
	{
		return tex;
	}

	@Override
	public String getName()
	{
		return this.name;
	}

	@Override
	public String[] getRequiredShipUpgrades()
	{
		return upgrades;
	}

	@Override
	public boolean hasBreathableAtmosphere()
	{
		return breathAble;
	}

	public PlanetBase setBreathableAtmosphere(boolean canBreath)
	{
		this.breathAble = canBreath;
		return this;
	}

	@Override
	public float getGravityVelocity()
	{
		return gravityVelocity;
	}

	@Override
	public float getSpreadVelocity()
	{
		return spreadVelocity;
	}

	@Override
	public boolean canFlyInDimension()
	{
		return canFly;
	}

	public PlanetBase setCanFlyInDimension(boolean canFly)
	{
		this.canFly = canFly;
		return this;
	}


	public PlanetBase setOxygenProprties(float spread, float gravity)
	{
		this.gravityVelocity = gravity;
		this.spreadVelocity = spread;
		return this;
	}

	public void write(FriendlyByteBuf buf)
	{
		buf.writeResourceLocation(dimension);
		buf.writeResourceLocation(tex);
		buf.writeUtf(name);

		buf.writeVarInt(upgrades.length);
		for(String s : upgrades)
		{
			buf.writeUtf(s);
		}
		buf.writeBoolean(breathAble);
		buf.writeFloat(spreadVelocity);
		buf.writeFloat(gravityVelocity);
		buf.writeBoolean(canFly);
	}

	public static PlanetBase read(FriendlyByteBuf buf)
	{
		ResourceLocation dim = buf.readResourceLocation();
		ResourceLocation tex = buf.readResourceLocation();
		String name = buf.readUtf();

		String[] upgrades = new String[buf.readVarInt()];
		for(int i=0;i<upgrades.length;i++)
		{
			 upgrades[i] = buf.readUtf();
		}

		PlanetBase base = new PlanetBase(dim, tex, name, upgrades);
		base.breathAble = buf.readBoolean();
		base.spreadVelocity = buf.readFloat();
		base.gravityVelocity = buf.readFloat();
		base.canFly = buf.readBoolean();

		return base;
	}
}
