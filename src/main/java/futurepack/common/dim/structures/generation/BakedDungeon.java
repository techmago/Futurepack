package futurepack.common.dim.structures.generation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import futurepack.common.FPLog;
import futurepack.common.dim.structures.OpenDoor;
import futurepack.common.dim.structures.StructureBase;
import futurepack.world.protection.FPDungeonProtection;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.levelgen.structure.BoundingBox;

public class BakedDungeon implements Cloneable
{

	private final Random rand;


	public BakedDungeon(Random random)
	{
		this.rand = random;
	}

	private ArrayList<DungeonRoom> structures = new ArrayList<DungeonRoom>(); 
	private ArrayList<BoundingBox> rooms;
	
	public ArrayList<OpenDoor> doors = new ArrayList<OpenDoor>();	
	public BoundingBox totalBox = null;
	
	public ArrayList<IDungeonEventListener> listener = new ArrayList<IDungeonEventListener>();
	
	public void addStructure(StructureBase structure, BlockPos pos, CompoundTag extra)
	{
		if(totalBox==null)
		{
			totalBox = BoundingBox.encapsulatingBoxes(Collections.singleton(structure.getBoundingBox(pos))).get();
		}
		else
		{
			totalBox.encapsulate(structure.getBoundingBox(pos));
		}
		
			
		structures.add(new DungeonRoom(structure, pos, extra));
		
		OpenDoor[] arr = structure.getRawDoors();
		for(OpenDoor door : arr)
		{
			addDoorSafe(door.offset(pos));
		}
		
		IDungeonEventListener lis = structure.getEventListener();
		if(lis!=null)
		{
			listener.add(lis);
		}
	}
	
	public void clear()
	{
		structures.clear();
		doors.clear();
		totalBox = null;
	}

	public void addDoorSafe(OpenDoor newDoor)
	{
		Direction face = newDoor.getDirection().getOpposite();
		BlockPos pos = newDoor.getPos().relative(newDoor.getDirection());
		
		//ja 2 verschiedenen listen
		Iterator<OpenDoor> iter = this.doors.iterator();
		while(iter.hasNext())
		{
			OpenDoor door = iter.next();
			if(newDoor.isSameSizeAndType(door) && face==door.getDirection() && pos.equals(door.getPos()))
			{
				iter.remove();
				return;				
			}
		}
		this.doors.add(newDoor);
	}
	
	public boolean isSpaceAvailable(BoundingBox box)
	{
		if(isOutside(totalBox, box))
		{
			return true;
		}
		for(int i=structures.size()-1;i>=0;i--)
		{
			DungeonRoom obj = structures.get(i);
			BoundingBox spawned = obj.structure.getBoundingBox(obj.pos);
			if(!isOutside(box, spawned))
			{
				return false;
			}
		}
		return true;
	}
	
	public boolean isOutside(BoundingBox box, BoundingBox outside)
	{
		return outside.maxX() < box.minX() || outside.minX() > box.maxX() || outside.maxY() < box.minY() || outside.minY() > box.maxY() || outside.maxZ() < box.minZ() || outside.minZ() > box.maxZ();
	}
	
	
	public void spawnDungeon(ServerLevel w, BlockPos startPos)
	{
		rooms = new ArrayList<>(structures.size());
		
		for(DungeonRoom obj : structures)
		{
			BlockPos start = startPos.offset(obj.pos);
			
			obj.structure.generate(w, start, rooms);
			obj.structure.addChestContentBase(w, start, this.rand, obj.extra, w.getServer().getLootTables());			
			FPDungeonProtection.addProtection(w, obj.structure.getBoundingBox(start));
		}
		listener.forEach(l -> l.onDungeonFinished(w, BakedDungeon.this, startPos));
	}

	public void spawnThreaded(final ServerLevel w, final BlockPos startPos, long max)
	{		
		rooms = new ArrayList<>(structures.size());
		
		w.getServer().submitAsync(new Runnable()
		{			
			int threadI = 0;
			
			@Override
			public void run()
			{
				if(threadI==0)
				{
					FPLog.logger.debug("Async Dungeon generation stared.");
				}
				else if(threadI>0)
				{
					FPLog.logger.debug("Async Dungeon generation %s of %s.", threadI, structures.size());
				}
				try
				{
					long time = System.currentTimeMillis();
					while(threadI<structures.size())
					{
						DungeonRoom obj = structures.get(threadI);
						
						BlockPos start = startPos.offset(obj.pos);
						
						FPLog.logger.debug("Placing " + obj.structure);
						boolean hide = obj.structure.hide;
						obj.structure.hide = false;
						obj.structure.generate(w, start, rooms);
						obj.structure.hide = hide;
						FPLog.logger.debug("Adding chest contents...");
						obj.structure.addChestContentBase(w, start, BakedDungeon.this.rand, obj.extra, w.getServer().getLootTables());
						FPLog.logger.debug("Adding protection...");
						FPDungeonProtection.addProtection(w, obj.structure.getBoundingBox(start));					
						threadI++;
						FPLog.logger.debug("Done");
						
						Runnable call = this;
						
						if(System.currentTimeMillis()-time >= max)
						{
							Thread t = new Thread(new Runnable()
							{			
								@Override
								public void run()
								{
									try
									{
										Thread.sleep(10);
									}
									catch (InterruptedException e) {
										e.printStackTrace();
									}
									w.getServer().submitAsync(call);
								}
							});
							t.setDaemon(true);
							t.start();
							break;
						}
					}
					
					if(threadI>=structures.size())
					{
						listener.forEach(l -> l.onDungeonFinished(w, BakedDungeon.this, startPos));
						
						FPLog.logger.debug("Async Dungeon generation done");
					}
				}
				catch(Throwable e)
				{
					e.printStackTrace();
					FPLog.logger.catching(e);
				}
			}
		});
	}
	
	public BoundingBox getBoundingBox(BlockPos startPos)
	{
		BoundingBox box = BoundingBox.encapsulatingBoxes(Collections.singleton(totalBox)).get();
		box.move(startPos.getX(), startPos.getY(), startPos.getZ());
		return box;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	protected BakedDungeon clone()
	{
		BakedDungeon copy = new BakedDungeon(this.rand);
		copy.structures = (ArrayList<DungeonRoom>) structures.clone();
		copy.doors = (ArrayList<OpenDoor>) doors.clone();
		copy.totalBox = BoundingBox.encapsulatingBoxes(Collections.singleton(totalBox)).get();
		
		return copy;
	}
	
	public List<BoundingBox> getAllStructureParts()
	{
		//stream<DungeonRoom> -> stream<MutableBoundingBox> -> MutableBoundingBox[]
		//return this.structures.stream().map(DungeonGeneratorBase::getBox).toArray(MutableBoundingBox[]::new);
		return rooms;
	}
	
	
}
