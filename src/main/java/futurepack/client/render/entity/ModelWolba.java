package futurepack.client.render.entity;

import com.google.common.collect.ImmutableList;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import futurepack.api.Constants;
import futurepack.common.entity.living.EntityWolba;
import net.minecraft.client.model.AgeableListModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.animal.Sheep;

public class ModelWolba extends AgeableListModel<EntityWolba>
{
	// This layer location should be baked with EntityRendererProvider.Context in
	// the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation(Constants.MOD_ID, "wolba"), "main");
	private final ModelPart wolba;

	public ModelWolba(ModelPart root) 
	{
		this.wolba = root.getChild("wolba");
		this.wolle = this.wolba.getChild("wool");
		this.hals = root.getChild("neck");
		
		this.bein_loh = this.wolba.getChild("leg_bl");
		this.bein_roh = this.wolba.getChild("leg_br");
		this.bein_lov = this.wolba.getChild("leg_fl");
		this.bein_rov = this.wolba.getChild("leg_fr");
	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition wolba = partdefinition.addOrReplaceChild("wolba",
				CubeListBuilder.create().texOffs(46, 0)
						.addBox(-3.0F, -12.0F, -4.5F, 6.0F, 5.0F, 3.0F, new CubeDeformation(0.0F)).texOffs(18, 8)
						.addBox(-2.5F, -11.5F, -1.5F, 5.0F, 4.0F, 3.0F, new CubeDeformation(0.0F)).texOffs(46, 0)
						.addBox(-3.0F, -12.0F, 1.5F, 6.0F, 5.0F, 3.0F, new CubeDeformation(0.0F)).texOffs(0, 0)
						.addBox(-2.5F, -11.5F, 4.5F, 5.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)),
				PartPose.offset(0.0F, 16.0F, 0.5F));

		PartDefinition neck = partdefinition.addOrReplaceChild("neck", CubeListBuilder.create(),	PartPose.offset(0.0F, 6.0F, -4.5F));

		PartDefinition neck_r1 = neck.addOrReplaceChild("neck_r1",
				CubeListBuilder.create().texOffs(20, 0).addBox(-1.5F, -1.5F, -1.0F, 3.0F, 3.0F, 4.0F,	new CubeDeformation(0.0F)),		PartPose.offsetAndRotation(0.0F, -1.0F, -2.0F, -0.4461F, 0.0F, 0.0F));

		PartDefinition head = neck.addOrReplaceChild("head",
				CubeListBuilder.create().texOffs(16, 33)
						.addBox(-3.0F, -1.0F, -1.0F, 4.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)).texOffs(0, 17)
						.addBox(-2.5F, 1.0F, -2.0F, 3.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)),
				PartPose.offset(1.0F, -2.0F, -5.0F));

		PartDefinition mouse = head.addOrReplaceChild("mouse", CubeListBuilder.create(),
				PartPose.offset(-1.0F, 3.0F, 1.0F));

		PartDefinition mouse_r1 = mouse.addOrReplaceChild("mouse_r1",
						CubeListBuilder.create().texOffs(0, 34).addBox(-1.5F, -0.1F, -3.0F, 3.0F, 1.0F, 4.0F,	new CubeDeformation(0.0F)),
						PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.1487F, 0.0F, 0.0F));

		PartDefinition antler_l = head.addOrReplaceChild("antler_l", CubeListBuilder.create(),
				PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition antler_l1_r1 = antler_l.addOrReplaceChild("antler_l1_r1",
						CubeListBuilder.create().texOffs(0, 12).addBox(-0.5F, 0.0F, -0.5F, 1.0F, 2.0F, 1.0F,
								new CubeDeformation(0.0F)),
						PartPose.offsetAndRotation(1.0F, -1.0F, 1.0F, 1.0129F, 0.0F, 0.0F));

		PartDefinition antler_l2_r1 = antler_l.addOrReplaceChild("antler_l2_r1",
				CubeListBuilder.create().texOffs(0, 12).addBox(-0.5F, -1.0F, -0.5F, 1.0F, 2.0F, 1.0F,
						new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(1.0F, -1.0F, 0.0F, -0.1396F, 0.0F, 0.0F));

		PartDefinition antler_big_r1 = antler_l.addOrReplaceChild("antler_big_r1",
						CubeListBuilder.create().texOffs(46, 11).addBox(-0.49F, -1.0F, 0.0F, 1.0F, 1.0F, 4.0F,
								new CubeDeformation(0.0F)),
						PartPose.offsetAndRotation(1.0F, 0.0F, 0.0F, 0.3346F, 0.0F, 0.0F));

		PartDefinition antler_r = head.addOrReplaceChild("antler_r", CubeListBuilder.create(),
				PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition antler_r1_r1 = antler_r.addOrReplaceChild("antler_r1_r1",
				CubeListBuilder.create().texOffs(0, 12).addBox(-0.5F, 0.0F, -0.5F, 1.0F, 2.0F, 1.0F,
						new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(-3.0F, -1.0F, 1.0F, 1.0129F, 0.0F, 0.0F));

		PartDefinition antler_r2_r1 = antler_r.addOrReplaceChild("antler_r2_r1",
				CubeListBuilder.create().texOffs(0, 12).addBox(-0.5F, -1.0F, -0.5F, 1.0F, 2.0F, 1.0F,
						new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(-3.0F, -1.0F, 0.0F, -0.1396F, 0.0F, 0.0F));

		PartDefinition antler_big_r2 = antler_r.addOrReplaceChild("antler_big_r2",
						CubeListBuilder.create().texOffs(46, 11).addBox(-0.51F, -1.0F, 0.0F, 1.0F, 1.0F, 4.0F,
								new CubeDeformation(0.0F)),
						PartPose.offsetAndRotation(-3.0F, 0.0F, 0.0F, 0.3346F, 0.0F, 0.0F));

		PartDefinition wool = wolba.addOrReplaceChild("wool", CubeListBuilder.create()
				.texOffs(0, 16).addBox(-3.5F, -0.5F, -1.0F, 7.0F, 5.0F, 11.0F, new CubeDeformation(0.0F))
				.texOffs(37, 21).addBox(-2.5F, -1.5F, -0.5F, 5.0F, 2.0F, 10.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 4.0F, -4.5F));

		PartDefinition tail = wolba.addOrReplaceChild("tail", CubeListBuilder.create().texOffs(66, 0).addBox(-1.0F,	0.0F, 0.0F, 2.0F, 8.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -12.0F, 4.5F));

		PartDefinition leg_fr = wolba.addOrReplaceChild("leg_fr", CubeListBuilder.create().texOffs(36, 0).addBox(-1.0F,	-1.0F, -1.0F, 2.0F, 7.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(-2.3F, -10.0F, -3.0F));

		PartDefinition shin_fr = leg_fr.addOrReplaceChild("shin_fr", CubeListBuilder.create().texOffs(36, 9).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 6.0F, 0.0F));

		PartDefinition leg_fl = wolba.addOrReplaceChild("leg_fl", CubeListBuilder.create().texOffs(36, 0).addBox(-1.0F,	-1.0F, -1.0F, 2.0F, 7.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(2.3F, -10.0F, -3.0F));

		PartDefinition shin_fl = leg_fl.addOrReplaceChild("shin_fl", CubeListBuilder.create().texOffs(36, 9).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 6.0F, 0.0F));

		PartDefinition leg_br = wolba.addOrReplaceChild("leg_br", CubeListBuilder.create().texOffs(36, 0).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 7.0F, 2.0F,	new CubeDeformation(0.0F)),	PartPose.offsetAndRotation(-2.3F, -10.0F, 3.0F, 0.0F, 0.0F, 0.0F));

		PartDefinition shin_br = leg_br.addOrReplaceChild("shin_br", CubeListBuilder.create().texOffs(36, 9).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 6.0F, 0.0F));

		PartDefinition leg_bl = wolba.addOrReplaceChild("leg_bl", CubeListBuilder.create().texOffs(36, 0).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 7.0F, 2.0F,new CubeDeformation(0.0F)),	PartPose.offsetAndRotation(2.3F, -10.0F, 3.0F, 0.0F, 0.0F, 0.0F));

		PartDefinition shin_bl = leg_bl.addOrReplaceChild("shin_bl", CubeListBuilder.create().texOffs(36, 9).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 6.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

//	// fields
//	ModelPart korpus_h;
//	ModelPart korpus_v;
//	ModelPart korpus_m;
//	ModelPart hinterteil;
//
	ModelPart hals;
//
//	ModelPart kopf;
//
//	ModelPart schnautze;
//	ModelPart kiefer;
//	// ModelRenderer schnautze;
//
	ModelPart bein_loh;
//	ModelPart bein_luh;
//
	ModelPart bein_roh;
//	ModelPart bein_ruh;
//
	ModelPart bein_lov;
//	ModelPart bein_luv;
//
	ModelPart bein_rov;
//	ModelPart bein_ruv;
//
	ModelPart wolle;
//
//	ModelPart gewei_r1;
//	ModelPart gewei_r2;
//	ModelPart gewei_r3;
//
//	ModelPart gewei_l1;
//	ModelPart gewei_l2;
//	ModelPart gewei_l3;
//
//	ModelPart schwanz;

	private float headRotationAngleX;
	private float[] fur_color;
	private boolean wool_on;

	private ImmutableList<ModelPart> parts;

	/*
	public ModelWolba()
	{
		texWidth = 128;
		texHeight = 128;

		korpus_h = new ModelPart(this, 46, 0);
		korpus_h.addBox(-2F, 0F, 0F, 6, 5, 3);
		korpus_h.setPos(0F, 0F + 4F, 11F - 8F);
		korpus_h.setTexSize(128, 128);
		korpus_h.mirror = true;
		setRotation(korpus_h, 0F, 0F, 0F);

		korpus_m = new ModelPart(this, 18, 8);
		korpus_m.addBox(-1.5F, 0.5F, 0F, 5, 4, 3);
		korpus_m.setPos(0F, 0F + 4F, 8F - 8F);
		korpus_m.setTexSize(128, 128);
		korpus_m.mirror = true;
		setRotation(korpus_m, 0F, 0F, 0F);

		hinterteil = new ModelPart(this, 0, 0);
		hinterteil.addBox(-1.5F, 0.5F, 0F, 5, 4, 1);
		hinterteil.setPos(0F, 0F + 4F, 14F - 8F);
		hinterteil.setTexSize(128, 128);
		hinterteil.mirror = true;
		setRotation(hinterteil, 0F, 0F, 0F);

		korpus_v = new ModelPart(this, 46, 0);
		korpus_v.addBox(-3F, -1F, 0F, 6, 5, 3);
		korpus_v.setPos(1F, 1F + 4F, 5F - 8F);
		korpus_v.setTexSize(128, 128);
		korpus_v.mirror = true;
		setRotation(korpus_v, 0F, 0F, 0F);

		hals = new ModelPart(this, 20, 0);
		hals.addBox(-1.5F, -1.5F, -4F, 3, 3, 4);
		hals.setPos(1F, 2.3F + 4F, 6F - 8F);
		hals.setTexSize(128, 128);
		hals.mirror = true;
		setRotation(hals, -0.4461433F, 0F, 0F);

		kopf = new ModelPart(this, 16, 33);
		kopf.addBox(-2F, -2F, -4F, 4, 4, 4);
		kopf.setPos(1F, 1.5F + 1F + 4F, 3F - 8F);
		kopf.setTexSize(128, 128);
		kopf.mirror = true;
		setRotation(kopf, 0.4461433F, 0F, 0F);

		schnautze = new ModelPart(this, 0, 17);
		schnautze.addBox(-1.5F, -1F, -1F, 3, 2, 1);
		schnautze.setPos(0F, -0.8F + 2F, 2F + -1F - 8F);
		schnautze.setTexSize(128, 128);
		schnautze.mirror = true;
		setRotation(schnautze, 0F, 0F, 0F);

		kiefer = new ModelPart(this, 0, 34);
		kiefer.addBox(-1.5F, 0F, -2F, 3, 1, 4);
		kiefer.setPos(0F, -0.8F + 3F, 2F + 0F - 8F);
		kiefer.setTexSize(128, 128);
		kiefer.mirror = true;
		setRotation(kiefer, 0.1487144F, 0F, 0F);

		bein_loh = new ModelPart(this, 36, 0);
		bein_loh.addBox(-1F, -1F, -1F, 2, 7, 2);
		bein_loh.setPos(3.3F, 2F + 4F, 12.5F - 8F);
		bein_loh.setTexSize(128, 128);
		bein_loh.mirror = true;
		setRotation(bein_loh, 0F, 0F, 0F);

		bein_luh = new ModelPart(this, 36, 9);
		bein_luh.addBox(-1F, 0F, -1F, 2, 4, 2);
		bein_luh.setPos(3.3F, 8F + 4F, 12.5F - 8F);
		bein_luh.setTexSize(128, 128);
		bein_luh.mirror = true;
		setRotation(bein_luh, 0F, 0F, 0F);

		addChild(bein_loh, bein_luh);

		bein_roh = new ModelPart(this, 36, 0);
		bein_roh.addBox(-1F, -1F, -1F, 2, 7, 2);
		bein_roh.setPos(-1.3F, 2F + 4F, 12.5F - 8F);
		bein_roh.setTexSize(128, 128);
		bein_roh.mirror = true;
		setRotation(bein_roh, 0F, 0F, 0F);

		bein_ruh = new ModelPart(this, 36, 9);
		bein_ruh.addBox(-1F, 0F, -1F, 2, 4, 2);
		bein_ruh.setPos(-1.3F, 8F + 4F, 12.5F - 8F);
		bein_ruh.setTexSize(128, 128);
		bein_ruh.mirror = true;
		setRotation(bein_ruh, 0F, 0F, 0F);

		addChild(bein_roh, bein_ruh);

		bein_lov = new ModelPart(this, 36, 0);
		bein_lov.addBox(-1F, -1F, -1F, 2, 7, 2);
		bein_lov.setPos(3.3F, 2F + 4F, 6.5F - 8F);
		bein_lov.setTexSize(128, 128);
		bein_lov.mirror = true;
		setRotation(bein_lov, 0F, 0F, 0F);

		bein_luv = new ModelPart(this, 36, 9);
		bein_luv.addBox(-1F, 0F, -1F, 2, 4, 2);
		bein_luv.setPos(3.3F, 8F + 4F, 6.5F - 8F);
		bein_luv.setTexSize(128, 128);
		bein_luv.mirror = true;
		setRotation(bein_luv, 0F, 0F, 0F);

		addChild(bein_lov, bein_luv);

		bein_rov = new ModelPart(this, 36, 0);
		bein_rov.addBox(-1F, -1F, -1F, 2, 7, 2);
		bein_rov.setPos(-1.3F, 2F + 4F, 6.5F - 8F);
		bein_rov.setTexSize(128, 128);
		bein_rov.mirror = true;
		setRotation(bein_rov, 0F, 0F, 0F);

		bein_ruv = new ModelPart(this, 36, 9);
		bein_ruv.addBox(-1F, 0F, -1F, 2, 4, 2);
		bein_ruv.setPos(-1.3F, 8F + 4F, 6.5F - 8F);
		bein_ruv.setTexSize(128, 128);
		bein_ruv.mirror = true;
		setRotation(bein_ruv, 0F, 0F, 0F);

		addChild(bein_rov, bein_ruv);

		schwanz = new ModelPart(this, 66, 0);
		schwanz.addBox(-1F, -1F, -2F, 2, 8, 2);
		schwanz.setPos(1F, 2F + 4F, 15.5F - 8F);
		schwanz.setTexSize(128, 128);
		schwanz.mirror = true;
		setRotation(schwanz, 0.1487144F, 0F, 0F);

		wolle1 = new ModelPart(this, 0, 16);
		wolle1.addBox(-3.5F, -0.49F, -1F, 7, 5, 11.1f);
		wolle1.setPos(1F, 0F + 4F, 5F - 8F);
		wolle1.setTexSize(128, 128);
		wolle1.mirror = true;
		setRotation(wolle1, 0F, 0F, 0F);

		wolle2 = new ModelPart(this, 37, 21);
		wolle2.addBox(-2.5F, -1.5F, -0.5F, 5, 2, 10);
		wolle2.setPos(1F, 0F + 4F, 5F - 8F);
		wolle2.setTexSize(128, 128);
		wolle2.mirror = true;
		setRotation(wolle2, 0F, 0F, 0F);

		// 0F, -0.8F + 2F, 2F + -1F - 8F

		gewei_r1 = new ModelPart(this, 0, 12);
		gewei_r1.addBox(-0.5F, -1F, -0.5F, 1, 2, 1);
		gewei_r1.setPos(-1F, -0.8F + -1F + 6F, 0F + -1F - 7F);
		gewei_r1.setTexSize(128, 128);
		gewei_r1.mirror = true;
		setRotation(gewei_r1, -0.1396263F, 0F, 0F);

		gewei_r2 = new ModelPart(this, 46, 11);
		gewei_r2.addBox(-0.55F, -1F, 0F, 1, 1, 4);
		gewei_r2.setPos(-1F, -0.8F + 0F + 6F, 0F + -1F - 7F);
		gewei_r2.setTexSize(128, 128);
		gewei_r2.mirror = true;
		setRotation(gewei_r2, 0.3346075F, 0F, 0F);

		gewei_r3 = new ModelPart(this, 0, 12);
		gewei_r3.addBox(-0.60F, 0F, -0.5F, 1, 2, 1);
		gewei_r3.setPos(-1F, -0.8F + -1F + 6F, 1F + -1F - 7F);
		gewei_r3.setTexSize(128, 128);
		gewei_r3.mirror = true;
		setRotation(gewei_r3, 1.012911F, 0F, 0F);

		gewei_l1 = new ModelPart(this, 0, 12);
		gewei_l1.addBox(-0.5F, -1F, -0.5F, 1, 2, 1);
		gewei_l1.setPos(3F, -0.8F + -1F + 6F, 0F + -1F - 7F);
		gewei_l1.setTexSize(128, 128);
		gewei_l1.mirror = true;
		setRotation(gewei_l1, -0.1396263F, 0F, 0F);

		gewei_l2 = new ModelPart(this, 46, 11);
		gewei_l2.addBox(-0.45F, -1F, 0F, 1, 1, 4);
		gewei_l2.setPos(3F, -0.8F + 0F + 6F, 0F + -1F - 7F);
		gewei_l2.setTexSize(128, 128);
		gewei_l2.mirror = true;
		setRotation(gewei_l2, 0.3346075F, 0F, 0F);

		gewei_l3 = new ModelPart(this, 0, 12);
		gewei_l3.addBox(-0.40F, 0F, -0.5F, 1, 2, 1);
		gewei_l3.setPos(3F, -0.8F + -1F + 6F, 1F + -1F - 7F);
		gewei_l3.setTexSize(128, 128);
		gewei_l3.mirror = true;
		setRotation(gewei_l3, 1.012911F, 0F, 0F);

		addChild(kopf, gewei_r1);
		addChild(kopf, gewei_r2);
		addChild(kopf, gewei_r3);

		addChild(kopf, gewei_l1);
		addChild(kopf, gewei_l2);
		addChild(kopf, gewei_l3);

		addChild(hals, kopf);
		addChild(kopf, schnautze);
		addChild(kopf, kiefer);

		Builder<ModelPart> builder = ImmutableList.builder();
		builder.add(korpus_h);
		builder.add(korpus_v);
		builder.add(korpus_m);
		builder.add(hinterteil);
//        builder.add(hals);
		builder.add(bein_loh);
		builder.add(bein_roh);
		builder.add(bein_lov);
		builder.add(bein_rov);
		builder.add(schwanz);
		this.parts = builder.build();
	}
*/

	@Override
	public void setupAnim(EntityWolba entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		this.hals.xRot = -0.4461433F + headPitch * 0.017453292F;
		this.hals.yRot = netHeadYaw * 0.017453292F;

		this.bein_roh.xRot = Mth.cos(limbSwing * 0.662F * 3F) * (1.4F / 3F) * limbSwingAmount;
		this.bein_loh.xRot = Mth.cos(limbSwing * 0.662F * 3F + (float) Math.PI) * (1.4F / 3F) * limbSwingAmount;
		this.bein_rov.xRot = Mth.cos(limbSwing * 0.662F * 3F + (float) Math.PI) * (1.4F / 3F) * limbSwingAmount;
		this.bein_lov.xRot = Mth.cos(limbSwing * 0.662F * 3F) * (1.4F / 3F) * limbSwingAmount;
	}

	@Override
	public void prepareMobModel(EntityWolba entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTickTime)
	{
		super.prepareMobModel(entitylivingbaseIn, limbSwing, limbSwingAmount, partialTickTime);

		EntityWolba wolba = (entitylivingbaseIn);

		this.headRotationAngleX = wolba.getHeadEatAngleScale(partialTickTime);

		fur_color = Sheep.getColorArray(wolba.getColor());
		this.wolle.visible = false;
		wool_on = !wolba.isSheared();
	}

	@Override
	public void renderToBuffer(PoseStack matrixStackIn, VertexConsumer bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha)
	{
		super.renderToBuffer(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn, red, green, blue, alpha);

		if (wool_on)
		{
			matrixStackIn.pushPose();

			this.wolle.visible = true;
			
			this.wolle.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn, fur_color[0], fur_color[1],	fur_color[2], 1);

			matrixStackIn.popPose();
		}

	}

	@Override
	protected Iterable<ModelPart> headParts()
	{
		return ImmutableList.of(hals);
	}

	@Override
	protected Iterable<ModelPart> bodyParts()
	{
		return ImmutableList.of(wolba);
	}
}
