package futurepack.common.block.misc;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RotatedPillarBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockFish extends RotatedPillarBlock
{
	private final VoxelShape[][] boxes;
	public static final IntegerProperty AGE_0_3 = BlockStateProperties.AGE_3;

	public BlockFish(Block.Properties props) 
	{
		super(props);
		boxes = new VoxelShape[3][4];
		registerDefaultState(super.stateDefinition.any().setValue(AGE_0_3, 0));
		for(Direction.Axis a: Direction.Axis.values())
		{
			for(int i=0;i<boxes[a.ordinal()].length;i++)
			{
				float h = 1 - (i*0.25F);
				float x=1,y=1,z=1;
				switch(a)
				{
				case X:x=h;break;
				case Y:y=h;break;
				case Z:z=h;break;
				}
				boxes[a.ordinal()][i] = Shapes.box(0, 0, 0, x,y,z);
			}
		}
	}
	
//	@Override
//	public void setBlockBoundsForItemRender()
//	{
//		this.setBlockBounds(0,0,0,1,1,1);
//	}
//	 
//	@Override
//	public AxisAlignedBB getCollisionBoundingBox(World w, BlockPos pos, IBlockState state) 
//	{
//		float m = getMetaFromState(state);
//		m =1-( m / 4F);
//		return new AxisAlignedBB(0, 0, 0, 1, m, 1).offset(pos.getX(),pos.getY(),pos.getZ());
//	}
//	
//	@Override
//	public void setBlockBoundsBasedOnState(IWorldReader w, BlockPos pos) 
//	{
//		float m = getMetaFromState(w.getBlockState(pos));
//		m =1-( m / 4F);
//		setBlockBounds(0, 0, 0, 1, m, 1);
//	}
//	
//	@Override
//	public void addCollisionBoxesToList(World worldIn, BlockPos pos, IBlockState state, AxisAlignedBB mask, List list, Entity collidingEntity)
//    {
//        this.setBlockBoundsBasedOnState(worldIn, pos);
//        super.addCollisionBoxesToList(worldIn, pos, state, mask, list, collidingEntity);
//    }
//	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext sel)
	{
		int m = state.getValue(AGE_0_3);
		Direction.Axis axis = state.getValue(AXIS);
		return boxes[axis.ordinal()][m];
	}

	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if (pl.canEat(false))
        {
			int meta = state.getValue(AGE_0_3);
			if(meta+1>=4)
			{
				w.removeBlock(pos, false);
			}
			else
			{
				w.setBlockAndUpdate(pos, state.setValue(AGE_0_3, meta+1));
			}
			pl.getFoodData().eat(5, 0.2F);
			
			return InteractionResult.SUCCESS;
        }
		return InteractionResult.PASS;
	}
	
//	@Override
//	public IBlockState getStateFromMeta(int meta)
//    {
//        return this.getDefaultState().withProperty(AGE_0_3, meta%4).withProperty(AXIS, EnumFacing.Axis.values()[meta/4]);
//    }
//	
//	@Override
//    public int getMetaFromState(IBlockState state)
//    {
//		EnumFacing.Axis axis = state.getValue(AXIS);
//		int m = state.getValue(AGE_0_3);
//        return m + axis.ordinal()*4;
//    }

	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(AGE_0_3);
	}
}
