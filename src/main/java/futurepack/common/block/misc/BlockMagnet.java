package futurepack.common.block.misc;

import futurepack.api.interfaces.IBlockBothSidesTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class BlockMagnet extends BlockRotateableTile implements IBlockBothSidesTickingEntity<TileEntityMagnet>
{
	public BlockMagnet(Block.Properties props) 
	{
		super(props);
	}

	@Override
	public BlockEntityType<TileEntityMagnet> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.MAGNET;
	}
}
