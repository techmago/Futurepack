package futurepack.common.block.misc;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockHoldingTile;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class BlockStructureFixHelper extends BlockHoldingTile implements IBlockServerOnlyTickingEntity<TileEntityStructureFixHelper>
{

	public BlockStructureFixHelper(Properties builder) 
	{
		super(builder);
	}

	@Override
	public BlockEntityType<TileEntityStructureFixHelper> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.STRUCTURE_FIX_HELPER;
	}

}
