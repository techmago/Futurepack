package futurepack.common.block.inventory;

import java.util.ArrayList;
import java.util.List;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.interfaces.IItemSupport;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.block.modification.machines.OptiBenchCraftingBase;
import futurepack.common.block.modification.machines.TileEntityOptiBench;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.util.LazyOptional;

public class TileEntityOptiBenchCraftingModule extends TileEntityInventoryLogistics implements ITilePropertyStorage
{
    protected int progress = 0;//max 10

    private LazyOptional<OptiBenchCraftingBase> optCrafting;
    private BlockPos optOptiBench;
    private int cachedDistance = -1;

	private ArrayList<ItemStack> overfilling = new ArrayList<>();

	public TileEntityOptiBenchCraftingModule(BlockEntityType<? extends TileEntityOptiBenchCraftingModule> t, BlockPos pos, BlockState state) 
	{
		super(t, pos, state);
	}

	public TileEntityOptiBenchCraftingModule(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.OPTIBENCH_CRAFTING_MODULE, pos, state);
	}


	
	@Override
	protected int getInventorySize()
	{
		return 11;
	}


    @Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(optCrafting);
		super.setRemoved();
	}

    private LazyOptional<OptiBenchCraftingBase> getCrafting()
    {
        if(optCrafting == null)
        {
            optCrafting = LazyOptional.of(() -> new Crafting());
		    optCrafting.addListener(p -> optCrafting=null);
			return optCrafting;
        }
        else
        {
            return optCrafting;
        }
    }

	@Override
	public void configureLogisticStorage(LogisticStorage storage) 
	{
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ITEMS);
		storage.setModeForFace(Direction.DOWN, EnumLogisticIO.OUT, EnumLogisticType.ITEMS);
	}
	
	@Override
	public String getGUITitle() 
    {
		return "block.futurepack.optibench_crafting_module";
	}

	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		ListTag l = new ListTag();
		for(int i=0;i<overfilling.size();i++)
		{
			if(overfilling.get(i)!=null && !overfilling.get(i).isEmpty())
			{
				CompoundTag tag = new CompoundTag();
				overfilling.get(i).save(tag);
				l.add(tag);
			}
		}
		nbt.put("overfilling", l);
		 nbt.putInt("progress", progress);
		return super.writeDataUnsynced(nbt);
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		overfilling.clear();
		ListTag l = nbt.getList("overfilling", 10);
		for(int i=0;i<l.size();i++)
		{
			CompoundTag tag = l.getCompound(i);
			ItemStack is = ItemStack.of(tag);
			overfilling.add(is);
		}
		progress = nbt.getInt("progress");
		super.readDataUnsynced(nbt);
	}
	
	
    public class Crafting extends OptiBenchCraftingBase
	{
		@Override
		protected ItemStack getBlueprintItem()
		{
			return items.get(10);
		}

		@Override
		protected NonNullList<ItemStack> getAllCraftingItems()
		{
			return items;
		}
		
		@Override
		protected int getCraftingSlots()
		{
			return 9;
		}
		
		@Override
		protected int getListOffset()
		{
			return 0;
		}
		
		@Override
		protected ItemStack getOutputSlot()
		{
			return items.get(9);
		}
		
		@Override
		protected void setOutputSlot(ItemStack stack)
		{
			setItem(9, stack);
		}

		@Override
		protected List<ItemStack> getOverfillingItems()
		{
			return overfilling;
		}

		@Override
		protected void setProgress(int progress)
		{
			TileEntityOptiBenchCraftingModule.this.progress = progress;
		}

		@Override
		protected int getProgress()
		{
			return TileEntityOptiBenchCraftingModule.this.progress;
		}

		@Override
		protected int getMaxProgress()
		{
			return 10;
		}
		
		@Override
		protected void markDirty()
		{
			TileEntityOptiBenchCraftingModule.this.setChanged();
		}
		
		@Override
		public FPTileEntityBase getTile()
		{
			return TileEntityOptiBenchCraftingModule.this;
		}
	}

    @Override
	public boolean canPlaceItem(int slot, ItemStack var2) 
	{
		if(slot>=0 && slot<=8 && !var2.isEmpty())
		{
			return getCrafting().orElse(null).isItemValidForCraftingSlot(slot, var2, level);
			
		}
		if(slot==10)
		{
			return var2.getItem() instanceof IItemSupport;
		}
		return slot != 9;
	}

    private Direction getInput()
	{
		BlockState state = getBlockState();
		if(!state.hasProperty(BlockRotateableTile.FACING))
		{
			return Direction.UP;
		}
		Direction face = state.getValue(BlockRotateableTile.FACING);
		return face;
	}

	public void onNeighborChange()
	{
        TileEntityOptiBench b = getOptiBench(0);
        if(b!=null)
        {
            b.registerCrafter(getCrafting());
        }
    }

    protected TileEntityOptiBench getOptiBench(int distance)
    {
        if(optOptiBench!=null)
        {
            BlockEntity t = level.getBlockEntity(optOptiBench);
            if(t!=null && t instanceof TileEntityOptiBench)
            {
                return distance + cachedDistance <=6 ? (TileEntityOptiBench)t : null;
            }
            else
            {
                optOptiBench = null;
                cachedDistance = -1;
            }
        }

        if(distance <= 6)
        {
            BlockEntity t = level.getBlockEntity(worldPosition.relative(getInput(), -1));
            if(t!=null)
            {
                if(t instanceof TileEntityOptiBench)
                {
                    optOptiBench = worldPosition.relative(getInput(), -1);
                    cachedDistance = 1;
                    return (TileEntityOptiBench)t;
                } 
                else if(t instanceof TileEntityOptiBenchCraftingModule)
                {
                    TileEntityOptiBench bench = ((TileEntityOptiBenchCraftingModule)t).getOptiBench(distance+1);
                    if(bench!=null)
                    {
                        optOptiBench = bench.getBlockPos();
                        cachedDistance = ((TileEntityOptiBenchCraftingModule)t).cachedDistance +1;
                        return bench;
                    }
                }
            }
        }

        return null;
    }

		@Override
	public int getProperty(int id) 
	{
		switch (id)
		{
		case 0:		
			return this.progress;
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:		
			this.progress = value;
			break;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount() 
	{
		return 1;
	}

	@Override
	public void setItem(int slot, ItemStack item) 
	{
		super.setItem(slot, item);
		if(optCrafting!=null)
		{
			optCrafting.ifPresent(p -> p.updateRecipe(TileEntityOptiBenchCraftingModule.this.level));
		}
	}
	
	@Override
	public boolean canTakeItemThroughFace(int slot, ItemStack var2, Direction side)
	{
		return super.canTakeItemThroughFace(slot, var2, side) && slot == 9;
	}
	
	@Override
	public int[] getSlotsForFace(Direction var1)
	{
		return new int[]{0,1,2,3,4,5,6,7,8,9};
	}

}
