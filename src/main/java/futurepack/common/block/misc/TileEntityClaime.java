package futurepack.common.block.misc;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;

public class TileEntityClaime extends FPTileEntityBase implements ITilePropertyStorage
{
	public static final List<WeakReference<TileEntityClaime>> claims = Collections.synchronizedList(new ArrayList<>());
	
	private static int ID = 0;
	
	public int x=2,y=2,z=2;
	public int mx=0,my=0,mz=0;
	public boolean renderAll=true;
	public String name = "Claime";
//	private Ticket chunckLoader;
	private String playerName = "Minecraft";
	
	public TileEntityClaime(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.CLAIME, pos, state);
		name+=ID;
		ID++;
		claims.add(new WeakReference<TileEntityClaime>(this));
	}
	
	public void checkBounds() 
	{
		if(x<2)
			x=2;
		if(y<2)
			y=2;
		if(z<2)
			z=2;
		
	}
	

	
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		nbt.putInt("sx", x);
		nbt.putInt("sy", y);
		nbt.putInt("sz", z);
		nbt.putInt("mx", mx);
		nbt.putInt("my", my);
		nbt.putInt("mz", mz);
		nbt.putBoolean("render", renderAll);
		nbt.putString("name", name);
		nbt.putString("player", playerName);
		return nbt;
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		x = nbt.getInt("sx");
		y = nbt.getInt("sy");
		z = nbt.getInt("sz");
		mx = nbt.getInt("mx");
		my = nbt.getInt("my");
		mz = nbt.getInt("mz");
		renderAll = nbt.getBoolean("render");
		name = nbt.getString("name");
		playerName = nbt.getString("player");
		
		checkBounds();
	}
	
	@Override
	public AABB getRenderBoundingBox() 
	{
		return new AABB(worldPosition.getX()-x, worldPosition.getY()-y, worldPosition.getZ()-z, worldPosition.getX()+x+1, worldPosition.getY()+y, worldPosition.getZ()+z+1).move(mx, my, mz);
	}

	public void BroudcastData() 
	{
		setChanged();
	}
	
	@Override
	public void onChunkUnloaded()
	{
//		if(chunckLoader!=null)
//		{
//			ForgeChunkManager.unforceChunk(chunckLoader, new ChunkPos(pos.getX()>>4, pos.getZ()>>4));
//		}
	}

	@Override
	public int getProperty(int id)
	{
		checkBounds();
		
		switch (id)
		{
		case 0:
			return x;
		case 1:
			return y;
		case 2:
			return z;
		case 3:
			return mx;
		case 4:
			return my;
		case 5:
			return mz;
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			x = value;
			break;
		case 1:
			y = value;
			break;
		case 2:
			z = value;
			break;
		case 3:
			mx = value;
			break;
		case 4:
			my = value;
			break;
		case 5:
			mz = value;
			break;
		default:
			break;
		}
		
		checkBounds();
	}

	@Override
	public int getPropertyCount() 
	{
		return 6;
	}

	public void onPlaced(LivingEntity placer)
	{
		playerName = placer.getName().getString();
	}
	
}
