package futurepack.extensions.crafttweaker;

import java.util.Arrays;

import org.openzen.zencode.java.ZenCodeType;

import com.blamejared.crafttweaker.api.annotation.ZenRegister;
import com.blamejared.crafttweaker.api.ingredient.IIngredient;
import com.blamejared.crafttweaker.api.item.IItemStack;

import futurepack.depend.api.ItemPredicate;
import futurepack.depend.api.ListPredicate;
import net.minecraft.world.item.ItemStack;

@ZenRegister
@ZenCodeType.Name("mods.futurepack.Recipes")
public class CrafttweakerExtension
{
	public static ListPredicate convert(IIngredient ingredient)
	{
		return new ListPredicate(true, Arrays.stream(ingredient.getItems())
				.map(IItemStack::getInternal)
				.map(ItemPredicate::new)
				.toArray(ItemPredicate[]::new));
	}

	public static ListPredicate[] convert(IIngredient...ingredient)
	{
		return Arrays.stream(ingredient)
				.map(CrafttweakerExtension::convert)
				.toArray(ListPredicate[]::new);
	}
	
	public static ItemStack[] convert(IItemStack...ingredient)
	{
		return Arrays.stream(ingredient).map(IItemStack::getInternal).toArray(ItemStack[]::new);
	}
}
