package futurepack.world.dimensions;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.google.common.collect.Lists;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import futurepack.common.FPLog;
import net.minecraft.core.Holder;
import net.minecraft.core.HolderSet;
import net.minecraft.core.Registry;
import net.minecraft.core.RegistryCodecs;
import net.minecraft.resources.RegistryOps;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.levelgen.structure.StructureSet;

public class AsteroidBeltSettings
{
	public static final Codec<AsteroidBeltSettings> CODEC = RecordCodecBuilder.<AsteroidBeltSettings>create((p_236938_0_) -> 
	{
		return p_236938_0_.group(RegistryOps.retrieveRegistry(Registry.BIOME_REGISTRY).forGetter((p_242874_0_) -> 
		{
			return p_242874_0_.biome_registry;
		}),
				RegistryCodecs.homogeneousList(Registry.STRUCTURE_SET_REGISTRY).optionalFieldOf("structure_overrides").forGetter(AsteroidBeltSettings::structureOverrides),
				AsteroidLayer.CODEC.listOf().fieldOf("asteroids").forGetter(AsteroidBeltSettings::getAsteroidLayers),
				Biome.CODEC.optionalFieldOf("biome").orElseGet(Optional::empty).forGetter((set) -> {
					return Optional.of(set.biomeToUse);
				}))
				.apply(p_236938_0_, AsteroidBeltSettings::new);
	   }).stable();
	
	private final Registry<Biome> biome_registry;
	private final List<AsteroidLayer> asteroidLayers = Lists.newArrayList();
	private Holder<Biome> biomeToUse;
	
	private final Optional<HolderSet<StructureSet>> structureOverrides;   
	
	
	public AsteroidBeltSettings(Registry<Biome> reg, Optional<HolderSet<StructureSet>> p_i242012_2_, List<AsteroidLayer> p_i242012_3_, Optional<Holder<Biome>> p_i242012_6_) 
	{
		this(p_i242012_2_, reg);


		this.asteroidLayers.addAll(p_i242012_3_);
//		this.updateLayers();
		if (!p_i242012_6_.isPresent()) {
			FPLog.logger.error("Unknown biome, defaulting to plains");
			this.biomeToUse = reg.getHolderOrThrow(Biomes.PLAINS);
		} else {
			this.biomeToUse = p_i242012_6_.get();
		}

	}	
	
	public AsteroidBeltSettings withSeed(long seed)
	{
		ArrayList<AsteroidLayer> layer = new ArrayList<>(asteroidLayers.size());
		for(AsteroidLayer old : asteroidLayers)
		{
			layer.add(old.withSeed(seed));
		}
		return new AsteroidBeltSettings(biome_registry, structureOverrides, layer, Optional.of(biomeToUse));
	}
	
	public AsteroidBeltSettings(Optional<HolderSet<StructureSet>> p_i242011_1_, Registry<Biome> reg)
	{
		this.biome_registry = reg;
		this.structureOverrides = p_i242011_1_;
		this.biomeToUse = reg.getHolderOrThrow(Biomes.PLAINS);
	}
	
	public List<AsteroidLayer> getAsteroidLayers() 
	{
		return this.asteroidLayers;
	}
	
	public Holder<Biome> getBiomeSupplier()
	{
		return biomeToUse;
	}

	public Optional<HolderSet<StructureSet>> structureOverrides() 
	{
		return structureOverrides;
	}
}
