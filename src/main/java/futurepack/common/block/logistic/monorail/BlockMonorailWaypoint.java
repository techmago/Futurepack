package futurepack.common.block.logistic.monorail;

import futurepack.api.interfaces.IBlockMonocartWaypoint;
import futurepack.common.entity.monocart.EntityMonocartBase;
import futurepack.common.sync.FPGuiHandler;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.phys.BlockHitResult;

public class BlockMonorailWaypoint extends BlockMonorailBasic implements IBlockMonocartWaypoint, EntityBlock
{
	public static final BooleanProperty powered = BlockStateProperties.POWERED;
	
	public BlockMonorailWaypoint(Properties props) 
	{
		super(props);
	}

	
	@Override
	public boolean isBendable()
	{
		return false;
	}

	@Override
	public void onMonocartPasses(Level w, BlockPos pos, BlockState state, EntityMonocartBase cart)
	{
		boolean rd = w.hasNeighborSignal(pos);
		if (cart.isPaused() != rd ) 
		{
			cart.setPaused(rd);
		}
		super.onMonocartPasses(w, pos, state, cart);
	}

	@Override
	public void neighborChanged(BlockState state, Level w, BlockPos pos, Block neighborBlock, BlockPos nBlock, boolean isMoving)
	{
		boolean rd = w.hasNeighborSignal(pos);
		state = state.setValue(powered, rd);
		w.setBlockAndUpdate(pos, state);
		super.neighborChanged(state, w, pos, neighborBlock, nBlock, isMoving);
	}
	
	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		//if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.RENAME_WAYPOINT.openGui(pl, pos);
		}	
		return InteractionResult.SUCCESS;
	}
	
	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new TileEntityMonorailWaypoint(pos, state);
	}
	
//	@Override
//	public IBlockState getStateFromMeta(int meta)
//    {
//		if(meta>5)
//		{
//			return this.getDefaultState().with(powered, true).with(State_bendless, EnumMonorailStates.getFromMeta(meta%6));
//		}
//        return this.getDefaultState().with(powered, false).with(State_bendless, EnumMonorailStates.getFromMeta(meta));
//    }
//
//	@Override
//    public int getMetaFromState(IBlockState state)
//    {
//        return state.get(State_bendless).getMeta() + (state.get(powered)? 6 : 0);
//    }
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		builder.add(powered);
		super.createBlockStateDefinition(builder);
	}

	@Override
	public Component getName(Level w, BlockPos pos, BlockState state)
	{
		return ((TileEntityMonorailWaypoint)w.getBlockEntity(pos)).getName();
	}

	@Override
	public boolean isWaypoint(Level w, BlockPos pos, BlockState state)
	{
		return true;
	}
	
}
