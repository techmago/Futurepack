package futurepack.common.block.modification.machines;

import futurepack.api.interfaces.IBlockBothSidesTickingEntity;
import futurepack.api.interfaces.IItemNeon;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockHoldingTile;
import futurepack.common.item.misc.ItemBatterie;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;

public class BlockIonCollector extends BlockHoldingTile implements IBlockBothSidesTickingEntity<TileEntityIonCollector>
{

	public BlockIonCollector(Block.Properties props) 
	{
		super(props);
	}

	@Override
	public InteractionResult use(BlockState state, Level worldIn, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.ION_COLLECTOR.openGui(pl, pos);
		}
		return InteractionResult.SUCCESS;
	}
	
	@Override
	public boolean hasAnalogOutputSignal(BlockState state)
    {
        return true;
    }
	
	@Override
	public int getAnalogOutputSignal(BlockState state, Level w, BlockPos pos)
    {
		TileEntityIonCollector tile = (TileEntityIonCollector)w.getBlockEntity(pos);
		if(tile.getItem(0)!=null&&tile.getItem(0).getItem() instanceof ItemBatterie)
		{
			IItemNeon neon = (IItemNeon) tile.getItem(0).getItem();
			float d = neon.getNeon(tile.getItem(0));
			if(d<=0)
				return 0;
			float m = neon.getMaxNeon(tile.getItem(0));
			
			return (int) ((d/m)*15);
		}
		
		return 0;
    }

	@Override
	public BlockEntityType<TileEntityIonCollector> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.ION_COLLECTOR;
	}
}
