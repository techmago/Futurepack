package futurepack.extensions.minetweaker;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import crafttweaker.CraftTweakerAPI;
import crafttweaker.IAction;
import crafttweaker.api.item.IIngredient;
import crafttweaker.api.item.IItemStack;
import crafttweaker.api.oredict.IOreDictEntry;
import futurepack.api.ItemPredicates;
import futurepack.depend.api.ItemStackPredicate;
import futurepack.depend.api.OreDictPredicate;
import net.minecraft.item.ItemStack;

public class ClassRegistry
{
	//public static IFuturepack bridge = new FuturepackBridge();
	
//	public static List<Class<?>> getClasses()
//	{
//		Class<?>[] cls = new Class[]
//				{
//						
//				};
//		return Arrays.asList(cls);
//	}

	private static List<IAction> ADDITIONS = new LinkedList<>();
	private static List<IAction> REMOVALS = new LinkedList<>();

	public static void initRecipeChanges() 
	{
		try
		{
			ADDITIONS.forEach(CraftTweakerAPI::apply);
			REMOVALS.forEach(CraftTweakerAPI::apply);
			ADDITIONS = null;
			REMOVALS = null;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void addRecipe(IAction action)
	{
		if (ADDITIONS != null)
		{
			ADDITIONS.add(action);
		}
		else
		{
			CraftTweakerAPI.apply(action);
		}
	}

	public static void removeRecipe(IAction action)
	{
		if (REMOVALS != null)
		{
			REMOVALS.add(action);
		}
		else
		{
			CraftTweakerAPI.apply(action);
		}
	}

	public static ItemPredicates getPredicate(IIngredient ingredient)
	{
		if (ingredient instanceof IOreDictEntry)
		{
			return new OreDictPredicate(((IOreDictEntry) ingredient).getName());
		}
		else if (ingredient instanceof IItemStack)
		{
			return new ItemStackPredicate(getItem((IItemStack) ingredient));
		}
		return null;
	}

	public static void onRecipeRemoveFail(ItemStack...its)
	{
		CraftTweakerAPI.logError(String.format("No Recipe found with %s as input", Arrays.toString(its)));
	}
	
	public static ItemStack getItem(IItemStack item)
	{
		try
		{
			Class<?> cls = Class.forName("crafttweaker.api.minecraft.CraftTweakerMC");
			Method m_get = cls.getMethod("getItemStack", IItemStack.class);
			return (ItemStack) m_get.invoke(null, item);
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;		
	}
	//minetweaker.api.minecraft.MineTweakerMC.getItemStack(IItemStack);
}
