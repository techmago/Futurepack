package futurepack.common.recipes.centrifuge;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import futurepack.api.ItemPredicateBase;
import futurepack.api.interfaces.IOptimizeable;
import futurepack.common.FPLog;
import futurepack.common.conditions.ConditionRegistry;
import futurepack.common.recipes.ISyncedRecipeManager;
import futurepack.depend.api.ItemPredicate;
import futurepack.depend.api.helper.HelperJSON;
import futurepack.depend.api.helper.HelperOreDict;
import futurepack.extensions.jei.FuturepackUids;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;

public class FPZentrifugeManager implements ISyncedRecipeManager<ZentrifugeRecipe>
{
	public static final String NAME = "centrifuge";
	
	public static FPZentrifugeManager instance = new FPZentrifugeManager();
	public ArrayList<ZentrifugeRecipe> recipes = new ArrayList<ZentrifugeRecipe>();

	public static ZentrifugeRecipe addRecipe(ItemPredicateBase in, ItemStack[] out, int support)
	{
		return instance.addZentrifugeRecipe(in, out, support);
	}
	
	public static ZentrifugeRecipe addRecipe(ItemStack in, ItemStack[] out, int support)
	{
		return instance.addZentrifugeRecipe(new ItemPredicate(in), out, support);
	}
	
	public ZentrifugeRecipe addZentrifugeRecipe(ItemPredicateBase in, ItemStack[] out, int support)
	{
		ZentrifugeRecipe z = new ZentrifugeRecipe(in, out, support);
		recipes.add(z);
		FPLog.logger.debug(z.toString());
		return z;
	}
	
	@Override
	public void addRecipe(ZentrifugeRecipe t) 
	{
		recipes.add(t);
	}
	
	@Override
	public Collection<ZentrifugeRecipe> getRecipes() 
	{
		return recipes;
	}
	
	@Override
	public ResourceLocation getName() 
	{
		return FuturepackUids.ZENTRIFUGE;
	}
	
	public ZentrifugeRecipe getMatchingRecipe(ItemStack it, boolean ignoreStackSize)
	{
		Iterator<ZentrifugeRecipe> iter = recipes.iterator();
		ZentrifugeRecipe recipe = null;
		while(iter.hasNext())
		{
			recipe = iter.next();
			if(recipe.matches(it, ignoreStackSize))
			{
				recipe.addPoint();
				if(recipe.getPoints() > 1000)
					resort();
				return recipe;
			}
		}
		return null;
	}
	
	private void resort()
	{
		final ArrayList<ZentrifugeRecipe> copy = new ArrayList<ZentrifugeRecipe>(recipes);
		Thread t = new Thread(new Runnable()
		{	
			@Override
			public void run()
			{
				IOptimizeable.sortList(copy);
				copy.forEach(IOptimizeable::resetPoints);
				recipes = copy;
			}
		});
		t.start();
	}
	
	public static void init(JsonArray recipes)
	{
		instance = new FPZentrifugeManager();
		FPLog.logger.info("Setup Zentrifuge Recipes");
		for(JsonElement elm : recipes)
		{
			setupObject(elm);
		}
	}
	
	private static void setupObject(JsonElement o)
	{
		if(o.isJsonObject())
		{
			JsonObject obj = o.getAsJsonObject();
			if(!ConditionRegistry.checkCondition(obj))
				return;
			
			ItemPredicateBase in = HelperJSON.getItemPredicateFromJSON(obj.get("input"));
			
			List<ItemStack> stacks = in.collectAcceptedItems(new ArrayList<>());
			if(stacks.isEmpty())
			{
				FPLog.logger.warn("Broken centrifuge recipe with empty input %s", obj.get("input").toString());
				return;
			}
			stacks.removeIf(s -> s==null||s.isEmpty());
			if(stacks.isEmpty())
			{
				FPLog.logger.warn("Broken centrifuge recipe with empty input %s", obj.get("input").toString());
				return;
			}
			
			JsonArray arr = obj.get("output").getAsJsonArray();
			List<ItemStack>[] out = new List[arr.size()];
			for(int i=0;i<arr.size();i++)
			{
				out[i] = HelperJSON.getItemFromJSON(arr.get(i), false);
				if(out[i]==null)
					return;
			}
			
			ItemStack[] singleOut = new ItemStack[out.length];
			for(int i=0;i<singleOut.length;i++)
			{
				if(out[i].isEmpty())
				{
					continue;
				}
				int original = out[i].get(0).getCount();
				singleOut[i] = HelperOreDict.FuturepackConveter.getChangedItem(out[i].get(0)).copy();
				singleOut[i].setCount(original);
			}
			int support = obj.get("support").getAsInt();
			int time=-1;
			if(obj.has("time"))
			{
				time = obj.get("time").getAsInt();
			}

			ZentrifugeRecipe zr = instance.addZentrifugeRecipe(in, singleOut, support);
			if(time>-1)
			{
				zr.setTime(time);
			}
		}
		else
		{
			FPLog.logger.error("Wrong JSON Type for a Recipe:  \"" + o + "\"");
		}
	}
}
