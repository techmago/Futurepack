package futurepack.common.block.inventory;

import java.util.function.Supplier;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperBoundingBoxes;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockWaterCooler extends BlockRotateableTile implements IBlockServerOnlyTickingEntity<TileEntityWaterCooler>
{
	private static final Supplier<double[][]> shape = () -> new double[][]{
		{0,0,0,16,3,16},
		{2,3,2,14,16,14},
		{4,3,0,12,13,16},
		{0,3,4,16,13,12}
	};
	public static final VoxelShape SHAPE_UP = HelperBoundingBoxes.createBlockShape(shape.get(), 0, 0);
	private final VoxelShape SHAPE_DOWN = HelperBoundingBoxes.createBlockShape(shape.get(), 180F, 0F);
	private final VoxelShape SHAPE_NORTH = HelperBoundingBoxes.createBlockShape(shape.get(), 90F, 0F);
	private final VoxelShape SHAPE_SOUTH = HelperBoundingBoxes.createBlockShape(shape.get(), 90F, 180F);
	private final VoxelShape SHAPE_WEST = HelperBoundingBoxes.createBlockShape(shape.get(), 90F, 90F);
	private final VoxelShape SHAPE_EAST = HelperBoundingBoxes.createBlockShape(shape.get(), 90F, 270F);
	
	
	public BlockWaterCooler(Block.Properties props) 
	{
		super(props);
	}
	
	@Override
	public InteractionResult use(BlockState state, Level worldIn, BlockPos pos, Player pl, InteractionHand handIn, BlockHitResult hit) 
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.WATER_COOLER.openGui(pl, pos);
		}
		return InteractionResult.SUCCESS;
	}
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context) 
	{
		switch(state.getValue(FACING))
		{
		case DOWN:
			return SHAPE_DOWN;
		case NORTH:
			return SHAPE_NORTH;
		case SOUTH:
			return SHAPE_SOUTH;
		case WEST:
			return SHAPE_WEST;
		case EAST:
			return SHAPE_EAST;
		case UP:
		default:
			return SHAPE_UP;
		}
	}

	@Override
	public BlockEntityType<TileEntityWaterCooler> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.WATER_COOLER;
	}
}
