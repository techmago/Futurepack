package futurepack.common.block.plants;

import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.block.Block;

public class ItemBurnableBlock extends BlockItem
{
	private final int burntime;
	
	public ItemBurnableBlock(Block blockIn, Properties builder, int burntime) 
	{
		super(blockIn, builder);
		this.burntime = burntime;
	}
	
	@Override
	public int getBurnTime(ItemStack itemStack, RecipeType<?> recipeType) 
	{
		return burntime;
	}
}
