package futurepack.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import com.google.common.collect.ImmutableList;
import com.mojang.datafixers.util.Pair;

import futurepack.api.Constants;
import net.minecraft.core.Holder;
import net.minecraft.core.Registry;
import net.minecraft.core.RegistryAccess;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.biome.Climate;
import net.minecraft.world.level.biome.MultiNoiseBiomeSource;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.NoiseBasedChunkGenerator;
import net.minecraft.world.level.levelgen.NoiseGeneratorSettings;
import net.minecraft.world.level.levelgen.structure.StructureSet;
import net.minecraft.world.level.levelgen.synth.NormalNoise;


public class FPWorldGenSettings
{

	public static final MultiNoiseBiomeSource.Preset PRESET_MENELAUS = new MultiNoiseBiomeSource.Preset(new ResourceLocation(Constants.MOD_ID, "menelaus"), (p_204283_) -> {
		return new Climate.ParameterList<>(ImmutableList.of(
				//temperature,humidity, continentalness(y_level), erosion(flat ness),depth (cave biome - 0 is at surface),  weirdness (how rare is it),  offset (how rare)) {

				Pair.of(Climate.parameters(1.0F, 0.0F, 0.7F, 0.8F, 0.0F, 0.0F, 0.0F), p_204283_.getOrCreateHolder(FPWorldData.MENELAUS)),
				Pair.of(Climate.parameters(1.0F, 0.0F, 0.7F, 0.8F, 0.0F, 0.0F, 0.0F), p_204283_.getOrCreateHolder(FPWorldData.MENELAUS_MUSHROOM)),
				Pair.of(Climate.parameters(1.0F, 0.4F, 0.1F, 0.34F, 0.0F, 0.0F, 0.0F), p_204283_.getOrCreateHolder(FPWorldData.MENELAUS_FOREST)),
				Pair.of(Climate.parameters(1.0F, 0.2F, 0.08F, 0.5F, 0.0F, 0.2F, 0.0F), p_204283_.getOrCreateHolder(FPWorldData.MENELAUS_FLAT)),
				Pair.of(Climate.parameters(1.0F, 0.2F, 1.0F, 1F, 0F, 0.1F, 0.0F), p_204283_.getOrCreateHolder(FPWorldData.MENELAUS_PLATAU)),
				Pair.of(Climate.parameters(1.0F, 0.3F, 0.5F, -0.1F, 0.0F, 0.0F, 0.0F), p_204283_.getOrCreateHolder(FPWorldData.MENELAUS_SEA)),
				Pair.of(Climate.parameters(0.9F, 0.5F, 0.5F, 0.7F, 0.0F, 0.9F, 0.0F), p_204283_.getOrCreateHolder(Biomes.FLOWER_FOREST)),
				Pair.of(Climate.parameters(0.7F, 0.4F, 0.5F, 0.7F, 0.0F, 0.5F, 0.0F), p_204283_.getOrCreateHolder(Biomes.SUNFLOWER_PLAINS))
				));
	});

//	public static final MultiNoiseBiomeSource.Preset PRESET_TYROS = new MultiNoiseBiomeSource.Preset(new ResourceLocation(Constants.MOD_ID, "tyros"), (p_204283_) -> {
//		return new Climate.ParameterList<>(ImmutableList.of(
//				//temperature,humidity, continentalness(y_level), erosion(flat ness),depth (cave biome - 0 is at surface),  weirdness (how rare is it),  offset (how rare)) {
//
//				Pair.of(Climate.parameters(1.0F, 1.0F, 0.2F, 0.21F, 0.0F, 0.0F, 0.0F), p_204283_.getOrCreateHolder(FPWorldData.TYROS)),
//				Pair.of(Climate.parameters(1.0F, 0.9F, 0.6F, 0.61F, 0.0F, 0.0F, 0.0F), p_204283_.getOrCreateHolder(FPWorldData.TYROS_MOUNTAIN)),
//				Pair.of(Climate.parameters(0.8F, 0.8F, 0.15F, 0.32F, 0.0F, 0.0F, 0.0F), p_204283_.getOrCreateHolder(FPWorldData.TYROS_PALIRIE_FOREST)),
//				Pair.of(Climate.parameters(0.5F, 0.5F, 0.2F, 0.31F, 0.0F, 0.2F, 0.0F), p_204283_.getOrCreateHolder(FPWorldData.TYROS_ROCK_DESERT)),
//				Pair.of(Climate.parameters(0.5F, 0.5F, 0.6F, 0.11F, 0F, 0.1F, 0.0F), p_204283_.getOrCreateHolder(FPWorldData.TYROS_ROCKDESERT_FLAT)),
//				Pair.of(Climate.parameters(0.8F, 0.8F, -0.2F, 0.1F, 0.0F, 0.0F, 0.0F), p_204283_.getOrCreateHolder(FPWorldData.TYROS_SWAMP))
//				));
//	});
	
	public static final MultiNoiseBiomeSource.Preset PRESET_TYROS = new MultiNoiseBiomeSource.Preset(new ResourceLocation(Constants.MOD_ID, "tyros"), mapVanillaBiomes(FPWorldData.TYROS, 
			Pair.of(Biomes.OCEAN, FPWorldData.TYROS_SWAMP),
			Pair.of(Biomes.DEEP_OCEAN, FPWorldData.TYROS_SWAMP),
			Pair.of(Biomes.BEACH, FPWorldData.TYROS_SWAMP),
			Pair.of(Biomes.SNOWY_BEACH, FPWorldData.TYROS_SWAMP),
			Pair.of(Biomes.RIVER, FPWorldData.TYROS_SWAMP),
			Pair.of(Biomes.SWAMP, FPWorldData.TYROS_SWAMP),
			Pair.of(Biomes.FLOWER_FOREST, FPWorldData.TYROS_SWAMP),
			Pair.of(Biomes.FROZEN_OCEAN, FPWorldData.TYROS_SWAMP),
			
			Pair.of(Biomes.WINDSWEPT_HILLS, FPWorldData.TYROS_MOUNTAIN),
			Pair.of(Biomes.WINDSWEPT_GRAVELLY_HILLS, FPWorldData.TYROS_MOUNTAIN),
			
			Pair.of(Biomes.BIRCH_FOREST, FPWorldData.TYROS_PALIRIE_FOREST),
			Pair.of(Biomes.FOREST, FPWorldData.TYROS_PALIRIE_FOREST),
			Pair.of(Biomes.DARK_FOREST, FPWorldData.TYROS_PALIRIE_FOREST),
			Pair.of(Biomes.OLD_GROWTH_BIRCH_FOREST, FPWorldData.TYROS_PALIRIE_FOREST),
			
			Pair.of(Biomes.DESERT, FPWorldData.TYROS_ROCK_DESERT),
			Pair.of(Biomes.BADLANDS, FPWorldData.TYROS_ROCK_DESERT),
			Pair.of(Biomes.ERODED_BADLANDS, FPWorldData.TYROS_ROCKDESERT_FLAT),
			Pair.of(Biomes.WOODED_BADLANDS, FPWorldData.TYROS_ROCKDESERT_FLAT),
			Pair.of(Biomes.SNOWY_TAIGA, FPWorldData.TYROS_ROCKDESERT_FLAT),
			Pair.of(Biomes.SNOWY_SLOPES, FPWorldData.TYROS_ROCK_DESERT),
			Pair.of(Biomes.STONY_SHORE, FPWorldData.TYROS_ROCK_DESERT)
			));
	
//	"minecraft:mountains": "futurepack:tyros_mountain",
//	"minecraft:mountain_edge": "futurepack:tyros_mountain",
//	"minecraft:jungle_hills": "futurepack:tyros_mountain",
//	"minecraft:snowy_taiga_hills": "futurepack:tyros_mountain",
//	"minecraft:gravelly_mountains": "futurepack:tyros_mountain",
//	"minecraft:modified_gravelly_mountains": "futurepack:tyros_mountain",

//	"minecraft:desert_hills": "futurepack:tyros_rockdesert_flat",


	public static NoiseBasedChunkGenerator makeDefaultMenelaus(RegistryAccess p_190040_, long p_190041_, boolean p_190042_)
	{
		return makeMenelaus(p_190040_, p_190041_, FPWorldData.SETTING_MENELAUS, p_190042_, PRESET_MENELAUS);
	}

	public static NoiseBasedChunkGenerator makeMenelaus(RegistryAccess p_190035_, long p_190036_, ResourceKey<NoiseGeneratorSettings> p_190037_, boolean p_190038_, MultiNoiseBiomeSource.Preset preset)
	{
		Registry<Biome> registry = p_190035_.registryOrThrow(Registry.BIOME_REGISTRY);
		Registry<StructureSet> registry1 = p_190035_.registryOrThrow(Registry.STRUCTURE_SET_REGISTRY);
		Registry<NoiseGeneratorSettings> registry2 = p_190035_.registryOrThrow(Registry.NOISE_GENERATOR_SETTINGS_REGISTRY);
		Registry<NormalNoise.NoiseParameters> registry3 = p_190035_.registryOrThrow(Registry.NOISE_REGISTRY);
		return new NoiseBasedChunkGenerator(registry1, registry3, preset.biomeSource(registry, p_190038_), p_190036_, registry2.getOrCreateHolder(p_190037_));
	}

	public static NoiseBasedChunkGenerator makeDefaultTyros(RegistryAccess p_190040_, long p_190041_, boolean p_190042_)
	{
		return makeMenelaus(p_190040_, p_190041_, FPWorldData.SETTING_TYROS, p_190042_, PRESET_TYROS);
	}

	
	public static Function<Registry<Biome>, Climate.ParameterList<Holder<Biome>>> mapVanillaBiomes(ResourceKey<Biome> fallback, Pair<ResourceKey<Biome>, ResourceKey<Biome>>...mappingA)
	{
		HashMap<ResourceKey<Biome>, ResourceKey<Biome>> mapping = new HashMap<>(mappingA.length);
		for(var p : mappingA)
		{
			mapping.put(p.getFirst(), p.getSecond());
		}
		return mapVanillaBiomes(mapping, fallback);
	}
	
	public static Function<Registry<Biome>, Climate.ParameterList<Holder<Biome>>> mapVanillaBiomes(Map<ResourceKey<Biome>, ResourceKey<Biome>> mapping, ResourceKey<Biome> fallback)
	{
		return mapVanillaBiomes(mapping, fallback, MultiNoiseBiomeSource.Preset.OVERWORLD);
	}
	
	public static Function<Registry<Biome>, Climate.ParameterList<Holder<Biome>>> mapVanillaBiomes(Map<ResourceKey<Biome>, ResourceKey<Biome>> mappingKeys, ResourceKey<Biome> fallbackKey, MultiNoiseBiomeSource.Preset base)
	{
		return (registry) -> {
			Climate.ParameterList<Holder<Biome>> baseBiomes = getParameterSource(base).apply(registry);
			List<Pair<Climate.ParameterPoint, Holder<Biome>>> climateList = getValues(baseBiomes);
			
			ArrayList<Pair<Climate.ParameterPoint, Holder<Biome>>> list = new ArrayList<>(climateList.size());
			
			Holder<Biome> fallback = registry.getOrCreateHolder(fallbackKey);
			HashMap<Holder<Biome>, Holder<Biome>> mapping = new HashMap<>(mappingKeys.size());
			mappingKeys.forEach((k,v) -> mapping.put(registry.getOrCreateHolder(k), registry.getOrCreateHolder(v)));
			
			climateList.stream().map(p -> p.mapSecond(b -> mapping.getOrDefault(b, fallback))).forEach(list::add);
			
			return new Climate.ParameterList<>(	list);
		};
	}
	
	private static Function<Registry<Biome>, Climate.ParameterList<Holder<Biome>>> getParameterSource(MultiNoiseBiomeSource.Preset base)
	{
		return base.parameterSource;
	}
	
	private static <T> List<Pair<Climate.ParameterPoint, T>> getValues(Climate.ParameterList<T> list)
	{
		return list.values();
	}
}
