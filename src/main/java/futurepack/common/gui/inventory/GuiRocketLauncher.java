package futurepack.common.gui.inventory;

import futurepack.api.Constants;
import futurepack.common.block.modification.TileEntityRocketLauncher;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Player;

public class GuiRocketLauncher extends GuiLaserEditor
{

	public GuiRocketLauncher(Player pl, TileEntityRocketLauncher tile)
	{
		super(pl, tile);
		res = new ResourceLocation(Constants.MOD_ID, "textures/gui/turret_gui_munition.png");
		imageHeight = 204;
	}

}
