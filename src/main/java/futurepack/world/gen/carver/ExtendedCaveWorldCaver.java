package futurepack.world.gen.carver;

import javax.annotation.Nullable;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.levelgen.Aquifer;
import net.minecraft.world.level.levelgen.DensityFunction;
import net.minecraft.world.level.levelgen.carver.CarverConfiguration;
import net.minecraft.world.level.levelgen.carver.CarvingContext;
import net.minecraft.world.level.levelgen.carver.CaveCarverConfiguration;
import net.minecraft.world.level.levelgen.carver.CaveWorldCarver;
import net.minecraft.world.level.material.FluidState;

public class ExtendedCaveWorldCaver extends CaveWorldCarver 
{

	public static CarveConfig DEFAULT = new CarveConfig(11, LAVA);
	
	public ExtendedCaveWorldCaver() 
	{
		super(CaveCarverConfiguration.CODEC);
	}
	
	public static class CarveConfig
	{
		public final int FLUID_HEIGHT;
		public final FluidState FLUID;
		
		public CarveConfig(int fLUID_HEIGHT, FluidState fLUID)
		{
			super();
			FLUID_HEIGHT = fLUID_HEIGHT;
			FLUID = fLUID;
		}
	}
	
	public CarveConfig getConfig()
	{
		return ExtendedCaveWorldCaver.DEFAULT;
	}

	@Override
	@Nullable
	public BlockState getCarveState(CarvingContext pContext, CaveCarverConfiguration pConfig, BlockPos pPos, Aquifer pAquifer) 
	{
		if (pPos.getY() <= getConfig().FLUID_HEIGHT) 
		{
			return getConfig().FLUID.createLegacyBlock();
		}
		else
		{
			BlockState blockstate = pAquifer.computeSubstance(new DensityFunction.SinglePointContext(pPos.getX(), pPos.getY(), pPos.getZ()), 0.0D);
			if (blockstate == null) 
			{
				return pConfig.debugSettings.isDebugMode() ? pConfig.debugSettings.getBarrierState() : null;
	         }
			else
			{
				return pConfig.debugSettings.isDebugMode() ? getDebugState(pConfig, blockstate) : blockstate;
			}
		}
	}

	public static BlockState getDebugState(CarverConfiguration pConfig, BlockState pState) {
	      if (pState.is(Blocks.AIR)) {
	         return pConfig.debugSettings.getAirState();
	      } else if (pState.is(Blocks.WATER)) {
	         BlockState blockstate = pConfig.debugSettings.getWaterState();
	         return blockstate.hasProperty(BlockStateProperties.WATERLOGGED) ? blockstate.setValue(BlockStateProperties.WATERLOGGED, Boolean.valueOf(true)) : blockstate;
	      } else {
	         return pState.is(Blocks.LAVA) ? pConfig.debugSettings.getLavaState() : pState;
	      }
	   }
}
