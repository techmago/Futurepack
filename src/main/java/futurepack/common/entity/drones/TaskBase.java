package futurepack.common.entity.drones;

public abstract class TaskBase 
{
	/**
	 * The assigned worker to handle this Task
	 */
	IWorker worker;
	
	/**
	 * The controller for this task 
	 */
	DroneControllerBase controller;
	
	/**
	 * Returns if the task is able to be done by an worker
	 * Important: worker is null
	 * @return
	 */
	abstract boolean isReady();
	
	/**
	 * Execute an step/tick in this task
	 * @return return true if tasks is finished
	 */
	abstract boolean step();
	
	/**
	 * Returns if the task should be deleted after finished or be checked later again
	 * @return
	 */
	abstract boolean isReapeatable();
		
	@Override
	public void finalize() 
	{
		controller.setWorkerIdle(worker);
	}
}
