package futurepack.common.dim.structures;

import java.util.ArrayList;
import java.util.Random;

import futurepack.common.block.misc.MiscBlocks;
import futurepack.common.block.misc.TileEntityDungeonCheckpoint;
import net.minecraft.ChatFormatting;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.storage.loot.LootTables;

public class StructureCheckpoint extends StructureBase
{
	private BlockPos[] teleporter;
	
	public StructureCheckpoint(StructureBase base)
	{
		super(base);
		BlockState[][][] blocks = getBlocks();
		
		ArrayList<BlockPos> teleporter = new ArrayList<BlockPos>();
		
		for(int x=0;x<blocks.length;x++)
		{
			for(int y=0;y<blocks[x].length;y++)
			{
				for(int z=0;z<blocks[x][y].length;z++)
				{
					if(blocks[x][y][z]!=null)
					{
						if(blocks[x][y][z].getBlock() == MiscBlocks.dungeon_checkpoint)
						{
							teleporter.add(new BlockPos(x,y,z));
						}
					}
				}
			}
		}
		this.teleporter = teleporter.toArray(new BlockPos[teleporter.size()]);
	}

	@Override
	public void addChestContentBase(ServerLevelAccessor w, BlockPos start, Random rand, CompoundTag extraData, LootTables manager)
	{
		super.addChestContentBase(w, start, rand, extraData, manager);
		
		int tecLvl = extraData.getInt("tecLevel");
		String dungeonName = extraData.getString("name");
		int floor = extraData.getInt("level");
		
		ChatFormatting[] light = {ChatFormatting.BLACK, ChatFormatting.DARK_AQUA, ChatFormatting.DARK_BLUE, ChatFormatting.GOLD, ChatFormatting.DARK_GREEN, ChatFormatting.DARK_PURPLE, ChatFormatting.DARK_RED};
		dungeonName = light[rand.nextInt(light.length)].toString() + dungeonName + ChatFormatting.RESET.toString();
		
		String teleporterName = String.format("Floor:%s %s L:%s", floor, dungeonName, tecLvl);
		
		for(BlockPos pos : teleporter)
		{
			BlockEntity e = w.getBlockEntity(start.offset(pos));
			if(e!=null)
			{
				TileEntityDungeonCheckpoint base = (TileEntityDungeonCheckpoint) e;
				base.setName(teleporterName);
				
			}
		}
	}

}
