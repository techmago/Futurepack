package futurepack.common.network;

import java.util.ArrayList;

import futurepack.api.PacketBase;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.eventbus.api.Event;

public class EventWirelessFunk
{
	public final int frequenz;
	public final int range;
	public final BlockPos source;
	public final PacketBase objPassed;
	public final int networkDepth;
	
	private final ArrayList<BlockEntity> allredyrecived = new ArrayList<BlockEntity>();
	
	public EventWirelessFunk(BlockPos src, int frequenz, int range, PacketBase pkt, int networkDepth)
	{
		this.source = src;
		this.frequenz = frequenz;
		this.range = range;
		this.objPassed = pkt;
		this.networkDepth = networkDepth;
	}
	
	
	public boolean canRecive(BlockEntity t)
	{
		if(t.getBlockPos().equals(source))
		{
			return false;
		}
		if(allredyrecived.contains(t))
		{
			return false;
		}
		allredyrecived.add(t);
		return true;
	}
}
