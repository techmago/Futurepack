package futurepack.api.interfaces.tilentity;

import net.minecraft.world.Nameable;

/**
 * Currently used by waypoints
 */
public interface ITileRenameable extends Nameable
{
	public void setName(String s);
}
