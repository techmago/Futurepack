package futurepack.common.block.inventory;

import java.util.ArrayList;
import java.util.UUID;

import javax.annotation.Nullable;

import futurepack.api.FacingUtil;
import futurepack.common.FPTileEntitys;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.WorldlyContainer;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.RecipeHolder;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Recipe;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;

public class TileEntityTechtable extends TileEntityInventoryBase implements WorldlyContainer, RecipeHolder
{
	//	0...8	: Crafting
	//		9	: Output
	//10...27	: Storage

	UUID owner;
	public Recipe last = null;

	private final int[] allowed;
	
	public TileEntityTechtable(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.TECHTABLE, pos, state);
		allowed = new int[18];
		for(int i=0;i<18;i++)
			allowed[i] = i+10;
	}
	
	
	private boolean canCraft(Player pl)
	{
		return HelperResearch.isUseable(pl, items.get(9));
	}
	
	@Override
	protected int getInventorySize()
	{
		return 28;
	}

	
	@Override
	public boolean canPlaceItem(int var1, ItemStack var2)
	{
		return var1 > 9;
	}

	@Override
	public int[] getSlotsForFace(Direction side)
	{
		return allowed;
	}

	@Override
	public boolean canPlaceItemThroughFace(int index, ItemStack itemStackIn, Direction direction)
	{
		return index > 9;
	}

	@Override
	public boolean canTakeItemThroughFace(int index, ItemStack stack, Direction direction)
	{
		return index > 9;
	}
	
	@Nullable
	public NeighbourContainer[] getSurroundingInventories() //called when opening the container...
	{
		ArrayList<NeighbourContainer> list =  new ArrayList<NeighbourContainer>(6);
		for(Direction face : FacingUtil.VALUES)
		{
			BlockPos neighbor = worldPosition.relative(face);
			BlockEntity tile = level.getBlockEntity(neighbor);
			if(tile!=null)
			{
				LazyOptional<IItemHandler> handler = tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, face.getOpposite());
				handler.ifPresent(c -> 
				{
					if(c instanceof IItemHandlerModifiable)
					{
						CheckedWrapper ch = new CheckedWrapper((IItemHandlerModifiable) c);
						if(ch.getSlots() > 0)
						{
							list.add( new NeighbourContainer(getItem(tile), face, ch));
						}
					}
				});
			}
		}
		
		if(list.isEmpty())
		{
			return null;
		}
		else
		{
			return list.toArray(new NeighbourContainer[list.size()]);
		}
	}
	
	public static ItemStack getItem(BlockEntity tile)
	{
		BlockState state = tile.getLevel().getBlockState(tile.getBlockPos());
		ItemLike item = state.getBlock();
		if(item == null)
		{
			item = state.getBlock();
		}
		return new ItemStack(item, 1);
	}
	
	public static class NeighbourContainer
	{
		public ItemStack item;
		public Direction side;
		public IItemHandlerModifiable inventory;
		
		public NeighbourContainer(ItemStack item, Direction side, IItemHandlerModifiable inventory)
		{
			super();
			this.item = item;
			this.side = side;
			this.inventory = inventory;
		}
	}
	
	private static class CheckedWrapper implements IItemHandlerModifiable
	{
		private static final ItemStack WIP = new ItemStack(Blocks.BEDROCK);
		
		public IItemHandlerModifiable inventory;
		private Integer[] slotMap;
		
		private CheckedWrapper(IItemHandlerModifiable inventory)
		{
			this.inventory = inventory;
			ArrayList<Integer> slots = new ArrayList<Integer>(inventory.getSlots());
			for(int i=0;i<inventory.getSlots();i++)
			{
				ItemStack cash = inventory.getStackInSlot(i);
				inventory.setStackInSlot(i, ItemStack.EMPTY);
				if(inventory.insertItem(i, WIP, true).isEmpty()) //check if random item gets accepted
				{
					slots.add(i);
				}
				inventory.setStackInSlot(i, cash);
			}
			slotMap = slots.toArray(new Integer[slots.size()]);
		}

		@Override
		public int getSlots()
		{
			return slotMap.length;
		}

		@Override
		public ItemStack getStackInSlot(int slot)
		{
			return inventory.getStackInSlot(getSlot(slot));
		}

		@Override
		public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
		{
			return inventory.insertItem(getSlot(slot), stack, simulate);
		}

		@Override
		public ItemStack extractItem(int slot, int amount, boolean simulate)
		{
			return inventory.extractItem(getSlot(slot), amount, simulate);
		}

		@Override
		public int getSlotLimit(int slot)
		{
			return inventory.getSlotLimit(getSlot(slot));
		}

		@Override
		public void setStackInSlot(int slot, ItemStack stack)
		{
			inventory.setStackInSlot(getSlot(slot), stack);
		}
		
		private int getSlot(int slot)
		{
			return slotMap.length > slot ? slotMap[slot] : slot;
		}

		@Override
		public boolean isItemValid(int slot, ItemStack stack) 
		{
			return inventory.isItemValid(getSlot(slot), stack);
		}
	}

	@Override
	public void setRecipeUsed(Recipe recipe)
	{
		last = recipe;
	}

	@Override
	public Recipe getRecipeUsed()
	{
		return last;
	}
	
	@Override
	public String getGUITitle() {
		return "gui.futurepack.techtable.title";
	}
	
	@Override
	public ItemStack removeItem(int slot, int amount) 
	{
		if(slot == 9) //the output slot
		{
			amount = 64; //take all from slot - so no spliting
		}
		return super.removeItem(slot, amount);
	}
}
