package futurepack.common.spaceships.moving;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.LongSupplier;
import java.util.stream.Collectors;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.ticks.LevelTicks;
import net.minecraft.world.ticks.ScheduledTick;
import net.minecraft.world.ticks.TickPriority;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IForgeRegistryEntry;

public class MovingSheduledTicks<T extends IForgeRegistryEntry<T>> implements INBTSerializable<CompoundTag>
{
	private final IForgeRegistry<T> registry;
	private List<ScheduledTick<T>> movedBlocks;
	public LongSupplier gameTimeSupplier;
	
	private MovingSheduledTicks(IForgeRegistry<T> registry, LongSupplier gameTimeSupplier, ArrayList<ScheduledTick<T>> movedBlocks)
	{
		this(registry, gameTimeSupplier);
		this.movedBlocks = movedBlocks;
	}
	
	private MovingSheduledTicks(IForgeRegistry<T> registry, LongSupplier gameTimeSupplier)
	{
		this.registry = registry;
		this.gameTimeSupplier = gameTimeSupplier;
	}
	
	public static <T> ScheduledTick<T> moveTicks(ScheduledTick<T> entry, Vec3i direction)
	{
		return new ScheduledTick<T>(entry.type(), entry.pos().offset(direction), entry.triggerTick(), entry.priority(), entry.subTickOrder());
	}
	
	public static <T extends IForgeRegistryEntry<T>> MovingSheduledTicks<T> load(IForgeRegistry<T> reg, CompoundTag tag, LongSupplier gameTimeSupplier)
	{
		MovingSheduledTicks<T> ticks = new MovingSheduledTicks<T>(reg, gameTimeSupplier);
		ticks.deserializeNBT(tag);
		return ticks;
	}
	
	
	public static <T> ListTag saveTickList(Function<T, ResourceLocation> p_219502_0_, Iterable<ScheduledTick<T>> p_219502_1_, long gameTime) 
	{
		ListTag listnbt = new ListTag();
		
		for(ScheduledTick<T> nextticklistentry : p_219502_1_) 
		{
			CompoundTag compoundnbt = new CompoundTag();
			compoundnbt.putString("i", p_219502_0_.apply(nextticklistentry.type()).toString());
			compoundnbt.putInt("x", nextticklistentry.pos().getX());
			compoundnbt.putInt("y", nextticklistentry.pos().getY());
			compoundnbt.putInt("z", nextticklistentry.pos().getZ());
			compoundnbt.putInt("t", (int)(nextticklistentry.triggerTick() - gameTime));
			compoundnbt.putInt("p", nextticklistentry.priority().getValue());
			compoundnbt.putLong("st", nextticklistentry.subTickOrder());
			listnbt.add(compoundnbt);
		}

		return listnbt;
	}
	
	public static <T> List<ScheduledTick<T>> loadTickList(Function<ResourceLocation, T> p_219502_0_, ListTag listnbt, long gameTime) 
	{
		return listnbt.stream().map(n -> (CompoundTag)n).map(nbt -> {
			BlockPos pos = new BlockPos(nbt.getInt("x"), nbt.getInt("y"), nbt.getInt("z"));
			ScheduledTick<T> e = new ScheduledTick<>(p_219502_0_.apply(new ResourceLocation(nbt.getString("i"))), pos, nbt.getInt("t") + gameTime, TickPriority.byValue(nbt.getInt("p")), nbt.getLong("st"));
			return e;
		}).collect(Collectors.toList());
	}
	

	@Override
	public CompoundTag serializeNBT() 
	{
		CompoundTag nbt = new CompoundTag();
		
		nbt.put("ticks", saveTickList(registry::getKey, movedBlocks, gameTimeSupplier.getAsLong()));
		
		return new CompoundTag();
	}

	@Override
	public void deserializeNBT(CompoundTag nbt) 
	{
		this.movedBlocks = loadTickList(registry::getValue, nbt.getList("ticks", nbt.getId()), gameTimeSupplier.getAsLong());
	}

	public void sheduleTicks(LevelTicks<T> w) 
	{
		movedBlocks.forEach(w::schedule);
	}

	public static MovingSheduledTicks<Block> createBlock(ServerLevel w, SimpleCollision collision, Vec3i direction) 
	{
		return createBase(ForgeRegistries.BLOCKS, w.getBlockTicks(), collision, direction, w::getGameTime);
	}
	
	public static MovingSheduledTicks<Fluid> createFluid(ServerLevel w, SimpleCollision collision, Vec3i direction) 
	{
		return createBase(ForgeRegistries.FLUIDS, w.getFluidTicks(), collision, direction, w::getGameTime);
	}
	
	public static <T extends IForgeRegistryEntry<T>> MovingSheduledTicks<T> createBase(IForgeRegistry<T> registry, LevelTicks<T> list, SimpleCollision collision, Vec3i direction, LongSupplier gameTimeSupplier) 
	{
		AABB box =  collision.getSize();
		BoundingBox bb = new BoundingBox((int)box.minX, (int)box.minY, (int)box.minZ, (int)box.maxX+1, (int)box.maxY+1, (int)box.maxZ+1);//+1 becuase id does a block.x < box.maxX check and nto a a <= check 
		
		ArrayList<ScheduledTick<T>> movedBlocks = new ArrayList<>(bb.getLength().distManhattan(Vec3i.ZERO));
		Set<BlockPos> movedBlocksSet = new HashSet<>(collision.getUnchangedPositions());
		movedBlocksSet.addAll(collision.getOldPositions());
		
		list.forContainersInArea(bb, (chunkPos, container) -> 
		{
			container.removeIf(entry -> {
				if(movedBlocksSet.contains(entry.pos()))
				{
					movedBlocks.add(moveTicks(entry, direction));
					return true;
				}
				return false;
			});
		});
		
		movedBlocks.trimToSize();

		return new MovingSheduledTicks<>(registry, gameTimeSupplier, movedBlocks);
	}

}
