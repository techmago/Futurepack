package futurepack.common.item.tools.compositearmor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.AttributeModifier.Operation;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public class ItemCompositeArmor extends ArmorItem
{
	protected final int slotCount;

	public ItemCompositeArmor(ArmorMaterial materialIn, EquipmentSlot equipmentSlotIn, int slotCount, Item.Properties props)
	{
		super(materialIn,equipmentSlotIn, props);
		this.slotCount = slotCount;
	}
	
	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type)
	{
		if(slot== EquipmentSlot.CHEST||slot== EquipmentSlot.FEET || slot== EquipmentSlot.HEAD)
			return "futurepack:textures/models/armor/compount_layer_1.png";
		else if(slot== EquipmentSlot.LEGS)
			return "futurepack:textures/models/armor/compount_layer_2.png";
		else
			return super.getArmorTexture(stack, entity, slot, type);
	}
	
	@Override
	public void onArmorTick(ItemStack it, Level world, Player player)
	{
		if(it.hasTag() && !isBroken(it))
		{		
			CompositeArmorInventory arm = new CompositeArmorInventory(player);
			if(arm.getPart(slot)!=null)
				arm.getPart(slot).onArmorTick(world, player, arm);
		}
		
		super.onArmorTick(it, world, player);
	}
	
	@Override
	public <T extends LivingEntity> int damageItem(ItemStack stack, int amount, T entity, Consumer<T> onBroken) 
	{
		if(stack.getDamageValue() + amount >= stack.getMaxDamage())
		{
			stack.getOrCreateTag().putBoolean("broken", true);
			stack.setDamageValue(stack.getMaxDamage() -1);
			return 0;
		}
		
		return super.damageItem(stack, amount, entity, onBroken);
	}
	
	public boolean isBroken(ItemStack armor)
	{
		if(armor.getOrCreateTag().getBoolean("broken"))
		{
			if(armor.getDamageValue()+1 >= armor.getMaxDamage())
			{
				return true;
			}
			else
			{
				armor.getOrCreateTag().putBoolean("broken", false);
				return false;
			}
		}
		return false;
	}
	
	@Override
	public Multimap<Attribute, AttributeModifier> getAttributeModifiers(EquipmentSlot equipmentSlot, ItemStack stack) 
	{
		if(isBroken(stack))
		{
			return HashMultimap.create();
		}
		
		Multimap<Attribute, AttributeModifier> mmap = HashMultimap.create();
		Multimap<Attribute, AttributeModifier> base = super.getAttributeModifiers(equipmentSlot, stack);
		
		if(equipmentSlot == this.slot)
		{
			CompositeArmorPart part = CompositeArmorPart.getInventory(stack);
			if(part!=null)
			{
				mmap.putAll(part.getAttributeModifiers());
			}
		}
		
		if(mmap.isEmpty())
			return base;
		
		mmap.putAll(base);
		
		Attribute[] attributes = mmap.keySet().toArray(new Attribute[base.keySet().size()]);
		for(Attribute attr : attributes)
		{
			AttributeModifier mod;
			if(base.get(attr).isEmpty())
			{
				mod = mmap.get(attr).iterator().next();
			}
			else
			{
				mod = base.get(attr).iterator().next();
			}
			
			UUID armorUuid = mod.getId();
			double[] amount = new double[] {0};
			ArrayList<AttributeModifier> doubled = new ArrayList<>();
			
			mmap.get(attr).stream().filter(m -> m.getOperation() == Operation.ADDITION).forEach(m -> {
				amount[0] += m.getAmount();
				doubled.add(m);
			});
			
			doubled.forEach(m -> {
				mmap.remove(attr, m);
			});
			
			mmap.put(attr, new AttributeModifier(armorUuid, mod.getName(), amount[0], Operation.ADDITION));
			
		}
		
		return mmap;
	}
	
	@Override
	public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) 
	{
		tooltip.add(new TranslatableComponent("futurepack.item.is_composite_armor"));
		if(isBroken(stack))
		{
			tooltip.add(new TranslatableComponent("futurepack.item.broken").setStyle(Style.EMPTY.withColor(ChatFormatting.RED)));
		}
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
	}
	
	public CompositeArmorPart createArmorPartInstance(ItemStack armor)
	{
		return new CompositeArmorPart(armor, this.getSlot(), this.slotCount);	
	}
}
