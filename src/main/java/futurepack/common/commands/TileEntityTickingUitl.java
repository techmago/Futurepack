package futurepack.common.commands;

import java.lang.reflect.Field;
import java.util.List;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.arguments.coordinates.Coordinates;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.TickingBlockEntity;
import net.minecraft.world.level.levelgen.structure.BoundingBox;

public class TileEntityTickingUitl
{
	public static int stopServerTicks(CommandContext<CommandSourceStack> context) throws CommandSyntaxException
	{
		return stopServerTicks(context.getSource().getLevel(), context.getArgument("from", Coordinates.class).getBlockPos(context.getSource()), context.getArgument("to", Coordinates.class).getBlockPos(context.getSource()));
	}

	public static int stopServerTicks(ServerLevel w, BlockPos start, BlockPos end) throws CommandSyntaxException
	{
		Field f;
		try
		{
			f = Level.class.getDeclaredField("blockEntityTickers");
			f.setAccessible(true);
			List<TickingBlockEntity> list = (List<TickingBlockEntity>) f.get(w);

			final BoundingBox box = BoundingBox.fromCorners(start, end);

			System.out.println("befor remove: " + list.size());
			list.removeIf(t -> box.isInside(t.getPos()));
			System.out.println("after remove: " + list.size());

		} catch (Exception e)
		{
			e.printStackTrace();
			return -1;
		}
		return Command.SINGLE_SUCCESS;
	}
}
