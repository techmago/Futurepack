//package futurepack.common.entity.ai;
//
//import java.util.EnumSet;
//
//import futurepack.common.entity.ai.hivemind.HVTask;
//import futurepack.common.entity.ai.hivemind.HiveMind;
//import futurepack.common.entity.ai.hivemind.HiveMindList;
//import futurepack.common.entity.living.EntityDungeonSpider;
//import net.minecraft.core.BlockPos;
//import net.minecraft.core.Direction;
//import net.minecraft.core.particles.ParticleTypes;
//import net.minecraft.server.level.ServerLevel;
//import net.minecraft.world.entity.ai.goal.Goal;
//
//public class AIHiveMind extends Goal 
//{
//	protected EntityDungeonSpider spider;
//	
//	protected HVTask currentTask;
//	
//	public AIHiveMind(EntityDungeonSpider spider) 
//	{
//		this.spider = spider;
//		setFlags(EnumSet.of(Goal.Flag.JUMP, Goal.Flag.MOVE, Goal.Flag.LOOK));
//	}
//
//	@Override
//	public boolean canUse() 
//	{
//		boolean b = spider.getRestrictCenter() != BlockPos.ZERO;
//		return b;
//	}
//
//	
//	@Override
//	public void start() 
//	{
//		spider.setAttachedDirection(Direction.DOWN);
//		tick();
//	}
//	
//	@Override
//	public void tick() 
//	{
//		if(spider.getCommandSenderWorld() instanceof ServerLevel)
//		{
//			if(currentTask == null || currentTask.isFinished(spider.getCommandSenderWorld()))
//			{
//				HiveMindList hvlist = HiveMindList.getHiveMindsIn((ServerLevel) spider.getCommandSenderWorld());
//				HiveMind mind = hvlist.getHiveMindFor(spider);
//				
//				currentTask = mind.getTaskFor(spider);
//			}
//			if(currentTask!=null && !currentTask.isFinished(spider.getCommandSenderWorld()))
//			{
//				if(!spider.isAttachedToWallOrCeiling())
//					spider.setAttachedDirection(Direction.DOWN);
//				
//				BlockPos target = currentTask.getSpiderPosition();
//				
//				((ServerLevel)spider.getCommandSenderWorld()).sendParticles(ParticleTypes.AMBIENT_ENTITY_EFFECT, target.getX()+0.5, target.getY()+0.5, target.getZ()+0.5, 0, 0, 0, 0, 0);
//				
//				if(spider.getNavi().getTarget() == null)
//				{
//					spider.getNavi().addTarget(target);
//				}
//				else
//				{
//					if(target.equals(spider.blockPosition()))
//					{
//						if(currentTask.execute(spider))
//						{
//							currentTask = null;
//						}
//					}
//				}
//			}
//		}
//	}
//	
//	@Override
//	public void stop() 
//	{
//		super.stop();
//		spider.detachFromWall();
//	}
//}
