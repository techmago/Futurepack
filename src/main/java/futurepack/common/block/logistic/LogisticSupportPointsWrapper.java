package futurepack.common.block.logistic;

import futurepack.api.EnumLogisticType;
import futurepack.api.capabilities.ILogisticInterface;
import futurepack.api.capabilities.ISupportStorage;

public class LogisticSupportPointsWrapper implements ISupportStorage
{
	public final ILogisticInterface log;
	public final ISupportStorage base;
	
	public LogisticSupportPointsWrapper(ILogisticInterface log, ISupportStorage base)
	{
		super();
		this.log = log;
		this.base = base;
	}
	
	@Override
	public int get()
	{
		return base.get();
	}

	@Override
	public int getMax()
	{
		return base.getMax();
	}

	@Override
	public int use(int used)
	{
		return base.use(used);
	}

	@Override
	public int add(int added)
	{
		return base.add(added);
	}

	@Override
	public EnumEnergyMode getType()
	{
		return base.getType();
	}

	@Override
	public void support()
	{
		base.support();
	}

	@Override
	public boolean canAcceptFrom(ISupportStorage other)
	{
		return base.canAcceptFrom(other) && log.getMode(EnumLogisticType.SUPPORT).canInsert();
	}

	@Override
	public boolean canTransferTo(ISupportStorage other) 
	{
		return base.canTransferTo(other) && log.getMode(EnumLogisticType.SUPPORT).canExtract();
	}

}
