package futurepack.generating.connected;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ConnectedGenerator {
	
	private static GsonBuilder gsonB = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting();
	
	private HashMap<String, String[]> blocks = new HashMap<>();
	
	private String[] PROPS = new String[] {"down", "up", "north", "east", "south", "west"};
	
	private File resOut;
	
	public ConnectedGenerator(File resOut) {
		this.resOut = resOut;
	}
	
	private static String modelToJson(ModelConnected model) {
		Gson gson = gsonB.create();
		return gson.toJson(model);
	}
	
	public static String blockstateToJson(GenBlockState state) {
		Gson gson = gsonB.create();
		return gson.toJson(state);
	}
	
	public void addBlock(String name, List<String> staticProps) {
		System.out.println("Added " + name);
		blocks.put(name, staticProps.toArray(new String[] {}));
	}
	
	public void addBlock(String name) {
		this.addBlock(name, new ArrayList<>());
	}
	
	private String intToFixedBinary(int number, int length) {
		String binaryNumber = Integer.toBinaryString(number);
		
		while(binaryNumber.length() < length) {
			binaryNumber = "0" + binaryNumber;
		}
		
		return binaryNumber;
	}
	
	public static String toOutputPath(File sourceBase, String fileName, String relPath) {
		StringBuilder b = new StringBuilder(sourceBase.getAbsolutePath());
		
		String[] packParts = relPath.split("/");
		
		for(String s : packParts) {
			b.append(File.separator + s);
		}
		
		b.append(File.separator + fileName);
		return b.toString();
	}
	
	public void generateFiles() {
		
		Iterator<String> it = blocks.keySet().iterator();
				
		while(it.hasNext()) {
			String block = it.next();
			
			System.out.println("Generating Block " + block);
			System.out.println("Using custom properties: " + Arrays.toString(blocks.get(block)));
			
			Map<String, ModelConnected> models = new HashMap<>();
			
			models.put("single", getBasicModel(block));
			
			ModelConnected one = getBasicModel(block);

			one.addTexture("north", block + "/one");
			one.addTexture("east", block + "/one");
			one.addTexture("south", block + "/one");
			one.addTexture("west", block + "/one");
			
			models.put("one", one);
			
			ModelConnected opposite = getBasicModel(block);

			opposite.addTexture("north", block + "/opposite");
			opposite.addTexture("east", block + "/opposite");
			opposite.addTexture("south", block + "/opposite");
			opposite.addTexture("west", block + "/opposite");
			
			models.put("opposite", opposite);
			
			ModelConnected corner = getBasicModel(block);

			corner.addTexture("east", block + "/corner");
			corner.addTexture("west", block + "/corner");
			corner.addTexture("south", block + "/one");
			corner.addTexture("down", block + "/one");
			corner.elements.get(0).faces.get("down").rotation = 180;
			corner.elements.get(0).faces.get("west").rotation = 270;
			
			models.put("corner_alt", corner);
			
			ModelConnected corner_alt = getBasicModel(block);

			corner_alt.addTexture("up", block + "/corner");
			corner_alt.addTexture("down", block + "/corner");
			corner_alt.addTexture("south", block + "/one");
			corner_alt.addTexture("west", block + "/one");
			corner_alt.elements.get(0).faces.get("down").rotation = 90;
			corner_alt.elements.get(0).faces.get("south").rotation = 90;
			corner_alt.elements.get(0).faces.get("west").rotation = 270;
			
			models.put("corner_alt", corner_alt);
			
			ModelConnected three = getBasicModel(block);

			three.addTexture("east", block + "/three");
			three.addTexture("west", block + "/three");
			three.addTexture("down", block + "/opposite");
			
			models.put("t", three);
			
			ModelConnected three_alt = getBasicModel(block);

			three_alt.addTexture("up", block + "/three");
			three_alt.addTexture("down", block + "/three");
			three_alt.addTexture("south", block + "/opposite");
			three_alt.elements.get(0).faces.get("south").rotation = 90;
			three_alt.elements.get(0).faces.get("down").rotation = 180;
			
			models.put("t_alt", three_alt);
			
			ModelConnected corner_three = getBasicModel(block);

			corner_three.addTexture("down", block + "/corner");
			corner_three.addTexture("south", block + "/corner");
			corner_three.addTexture("west", block + "/corner");
			
			corner_three.elements.get(0).faces.get("down").rotation = 90;
			corner_three.elements.get(0).faces.get("west").rotation = 270;
			
			models.put("corner_three", corner_three);
			
			ModelConnected all = getBasicModel(block);
			all.addTexture("up", block + "/all");
			all.addTexture("down", block + "/all");
			all.addTexture("north", block + "/all");
			all.addTexture("east", block + "/all");
			all.addTexture("south", block + "/all");
			all.addTexture("west", block + "/all");
			
			models.put("all", all);
			
			ModelConnected all_three = getBasicModel(block);
			all_three.addTexture("up", block + "/three");
			all_three.addTexture("down", block + "/three");
			all_three.addTexture("north", block + "/three");
			all_three.addTexture("east", block + "/three");
			all_three.addTexture("south", block + "/three");
			all_three.addTexture("west", block + "/three");
			
			all_three.elements.get(0).faces.get("down").rotation = 90;
			
			all_three.elements.get(0).faces.get("north").rotation = 90;
			all_three.elements.get(0).faces.get("east").rotation = 270;
			
			models.put("all_three", all_three);
			
			
			for(String s : models.keySet()) {
				
				String jsonModel = modelToJson(models.get(s));
				
				try {
					Files.write(Paths.get(toOutputPath(this.resOut, block + "_" + s + ".json", "assets/futurepack/models/block/" + block)), jsonModel.getBytes());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			String itemJson = "{\r\n" + 
					"  \"parent\": \"futurepack:block/" + block + "/" + block + "_single\"\r\n" + 
					"}";
			
			try {
				Files.write(Paths.get(toOutputPath(this.resOut, block + ".json", "assets/futurepack/models/item/")), itemJson.getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			GenBlockState state = new GenBlockState();
			
			for(int i = 0; i < Math.pow(2, PROPS.length); i++) {
				String binaryNumber = intToFixedBinary(i, PROPS.length);
				
				StringBuilder variant = new StringBuilder();
				
				int neighbourCount = 0;
				
				for(int j = 0; j < PROPS.length; j++) {
					if(j != 0) {
						variant.append(",");
					}
					
					variant.append(PROPS[j]);
					variant.append("=");
					variant.append((binaryNumber.charAt(j) == '1' ? "true": "false"));		
					
					if(binaryNumber.charAt(j) == '1') {
						neighbourCount++;
					}
				}
				
				boolean down = binaryNumber.charAt(0) == '1';
				boolean up = binaryNumber.charAt(1) == '1';
				boolean north = binaryNumber.charAt(2) == '1';
				boolean east = binaryNumber.charAt(3) == '1';
				boolean south = binaryNumber.charAt(4) == '1';
				boolean west = binaryNumber.charAt(5) == '1';
				
				GenBlockStateModel stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_single");				
				
				if(neighbourCount == 1) {
					stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_one");
					if(down) {
						stateModel.x = 180;
					}
					if(north) {
						stateModel.x = 90;
					}
					if(east) {
						stateModel.x = 90;
						stateModel.y = 90;
					}
					if(south) {
						stateModel.x = 270;
					}
					if(west) {
						stateModel.x = 90;
						stateModel.y = 270;
					}
				}
				
				if(neighbourCount == 2) {
					if(down && up) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_opposite");
					}
					if(north && south) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_opposite");
						stateModel.x = 90;
					}
					if(east && west) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_opposite");
						stateModel.x = 90;
						stateModel.y = 90;
					}
					
					if(up && north) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_corner");
					}
					
					if(up && east) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_corner");
						stateModel.y = 90;
					}
					
					if(up && south) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_corner");
						stateModel.y = 180;
					}
					
					if(up && west) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_corner");
						stateModel.y = 270;
					}
					
					if(down && north) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_corner");
						stateModel.x = 180;
						stateModel.y = 180;
					}
					
					if(down && east) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_corner");
						stateModel.y = 270;
						stateModel.x = 180;
					}
					
					if(down && south) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_corner");
						stateModel.x = 180;
					}
					
					if(down && west) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_corner");
						stateModel.y = 90;
						stateModel.x = 180;
					}
					
					if(north && east) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_corner_alt");
					}
					
					if(east && south) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_corner_alt");
						stateModel.y = 90;
					}
					
					if(south && west) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_corner_alt");
						stateModel.y = 180;
					}
					
					if(west && north) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_corner_alt");
						stateModel.y = 270;
					}
				}
				
				if(neighbourCount == 3) {
					if(up && north && south) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_t");
					}
					
					if(up && east && west) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_t");
						stateModel.y = 90;
					}
					
					if(down && north && south) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_t");
						stateModel.x = 180;
					}
					
					if(down && east && west) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_t");
						stateModel.x = 180;
						stateModel.y = 90;
					}
					
					if(north && east && west) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_t_alt");
					}
					
					if(north && east && south) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_t_alt");
						stateModel.y = 90;
					}
					
					if(east && south && west) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_t_alt");
						stateModel.y = 180;
					}
					
					if(north && south && west) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_t_alt");
						stateModel.y = 270;
					}
					
					if(up && down && north) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_t");
						stateModel.x = 90;
					}
					
					if(up && down && east) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_t");
						stateModel.x = 90;
						stateModel.y = 90;
					}
					
					if(up && down && south) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_t");
						stateModel.x = 90;
						stateModel.y = 180;
					}
					
					if(up && down && west) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_t");
						stateModel.x = 90;
						stateModel.y = 270;
					}
					
					if(up && north && east) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_corner_three");
					}
					
					if(up && east && south) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_corner_three");
						stateModel.y = 90;
					}
					
					if(up && south && west) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_corner_three");
						stateModel.y = 180;
					}
					
					if(up && west && north) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_corner_three");
						stateModel.y = 270;
					}
					
					if(down && north && east) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_corner_three");
						stateModel.x = 180;
						stateModel.y = 270;
					}
					
					if(down && east && south) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_corner_three");
						stateModel.x = 180;
					}
					
					if(down && south && west) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_corner_three");
						stateModel.x = 180;
						stateModel.y = 90;
					}
					
					if(down && west && north) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_corner_three");
						stateModel.x = 180;
						stateModel.y = 180;
					}
				}
				
				if(neighbourCount == 4) {
					if((up && down && north && south) || (up && down && east && west) || (north && east && south && west)) {
						stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_all");
					}
					else {
						
						if(up && north && east && south) {
							stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_all_three");
						}
						
						if(up && east && south && west) {
							stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_all_three");
							stateModel.y = 90;
						}
						
						if(up && south && west && north) {
							stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_all_three");
							stateModel.y = 180;
						}
						
						if(up && west && north && east) {
							stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_all_three");
							stateModel.y = 270;
						}
						
						
						if(down && north && east && south) {
							stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_all_three");
							stateModel.x = 180;
						}
						
						if(down && east && south && west) {
							stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_all_three");
							stateModel.x = 180;
							stateModel.y = 90;
						}
						
						if(down && south && west && north) {
							stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_all_three");
							stateModel.x = 180;
							stateModel.y = 180;
						}
						
						if(down && west && north && east) {
							stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_all_three");
							stateModel.x = 180;
							stateModel.y = 270;
						}
						
						if(up && down && north && east) {
							stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_all_three");
							stateModel.y = 180;
						}
						
						if(up && down && east && south) {
							stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_all_three");
							stateModel.y = 270;
						}
						
						if(up && down && south && west) {
							stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_all_three");
						}
						
						if(up && down && west && north) {
							stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_all_three");
							stateModel.y = 90;
						}
					}
					
				}
				
				if(neighbourCount == 5) {
					stateModel = new GenBlockStateModel("futurepack:block/quantanium/" + block + "_all");
				}
				
				
				
				String variantBase = variant.toString();
				
				String[] staticProps = blocks.get(block);
				if(staticProps.length > 0) {
					for(int j = 0; j < Math.pow(2, staticProps.length); j++) {
						String staticNum = intToFixedBinary(j, staticProps.length);
						
						StringBuilder subBuilder = new StringBuilder(variantBase);
						
						
						for(int k = 0; k < staticProps.length; k++) {
							subBuilder.append(",");
							subBuilder.append(staticProps[k]);
							subBuilder.append("=");
							subBuilder.append((staticNum.charAt(k) == '1' ? "true" : "false"));
			            }
						
						System.out.println("Generating Variant: " + subBuilder.toString());
						
						state.addVariant(subBuilder.toString(), stateModel);
					}
				}
				else {
					state.addVariant(variantBase, stateModel);
				}
			}
			
			try {
				Files.write(Paths.get(toOutputPath(this.resOut, block + ".json", "assets/futurepack/blockstates")), blockstateToJson(state).getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static ConnectedGenerator create(File resOut) {
		return new ConnectedGenerator(resOut);
	}
	
	private static ModelConnected getBasicModel(String block) {
		ModelConnected model = new ModelConnected();
		
		ModelElement element = new ModelElement(new int[] {0,0,0}, new int[] {16,16,16});
		
		element.addFace("up", new ModelFace("up"));
		element.addFace("down", new ModelFace("down"));
		element.addFace("north", new ModelFace("north"));
		element.addFace("east", new ModelFace("east"));
		element.addFace("south", new ModelFace("south"));
		element.addFace("west", new ModelFace("west"));
		
		model.addElement(element);
		
		model.addTexture("particle", block + "/" + "single");
		model.addTexture("up", block + "/" + "single");
		model.addTexture("down", block + "/" + "single");
		model.addTexture("north", block + "/" + "single");
		model.addTexture("east", block + "/" + "single");
		model.addTexture("south", block + "/" + "single");
		model.addTexture("west", block + "/" + "single");
		
		return model;
	}
}
