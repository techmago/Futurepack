package futurepack.depend.api.interfaces;

import futurepack.common.gui.SlotScrollable;

public interface IContainerScrollable
{
	public default boolean isEnabled(SlotScrollable s)
	{
		int slot = s.getSlotIndex();
		int row = slot/getRowWidth() - getScollIndex();
		if(row >= 0 && row < getRowCount())
		{
			s.y = row * 18 + s.startY;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public int getScollIndex();
	
	public int getRowWidth();
	
	public int getRowCount();
}
