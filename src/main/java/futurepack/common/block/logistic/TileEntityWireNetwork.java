package futurepack.common.block.logistic;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import futurepack.common.FPTileEntitys;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityWireNetwork extends TileEntityWireBase implements ITileNetwork
{

	public TileEntityWireNetwork(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.WIRE_NETWORK, pos, state);
	}

	@Override
	public int getMaxEnergy() 
	{
		return 500;
	}

	@Override
	public boolean isNetworkAble()
	{
		return true;
	}
	@Override
	public boolean isWire()
	{
		return true;
	}
	
	@Override
	public void onFunkPacket(PacketBase pkt) {}
}
