package futurepack.common.item.tools;

import futurepack.common.ManagerGleiter;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterials;
import net.minecraft.world.item.DyeableLeatherItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ItemGleiterControler extends ArmorItem implements DyeableLeatherItem
{
	public ItemGleiterControler(Item.Properties props) 
	{
		super(ArmorMaterials.LEATHER, EquipmentSlot.CHEST, props);
//		
//		super(ArmorMaterial.LEATHER, 0, EntityEquipmentSlot.CHEST);
//		setCreativeTab(FPMain.tab_items);
	}
	
	@Override
	public InteractionResultHolder<ItemStack> use(Level w, Player pl, InteractionHand hand)
	{
//		ItemStack it = pl.getHeldItem(hand);
		if(!w.isClientSide)
		{
			boolean open = ManagerGleiter.isGleiterOpen(pl);
			ManagerGleiter.setGleiterOpen(pl, !open);		
		}
		return super.use(w, pl, hand);
	}
	
	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type)
	{
		if(slot== EquipmentSlot.CHEST)
		{
			if(type!=null && type.equalsIgnoreCase("overlay"))
			{
				return "futurepack:textures/models/armor/paragleiter_over.png";
			}
			else
			{	
				return "futurepack:textures/models/armor/paragleiter.png";
			}
		}
		return super.getArmorTexture(stack, entity, slot, type);
	}
	
	@Override
	public int getColor(ItemStack stack)
    {
		CompoundTag nbttagcompound = stack.getTagElement("display");
		return nbttagcompound != null && nbttagcompound.contains("color", 99) ? nbttagcompound.getInt("color") : 0xaa2111;
    }
}
