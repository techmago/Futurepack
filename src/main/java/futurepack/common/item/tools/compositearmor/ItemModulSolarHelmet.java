package futurepack.common.item.tools.compositearmor;

import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ItemModulSolarHelmet extends ItemModulNeonContainer
{
	public ItemModulSolarHelmet(Item.Properties props) 
	{
		super(EquipmentSlot.HEAD, 750, props);
	}

	@Override
	public void onArmorTick(Level world, Player player, ItemStack it, CompositeArmorInventory inv)
	{	
		pushEnergy(inv, it);
		
		if(world.isDay() && world.canSeeSky(player.blockPosition()))
		{
			if(getNeon(it) < getMaxNeon(it))
			{
				addNeon(it, 1);
			}
		}
	}
	
	@Override
	public boolean isEnergyProvider()
	{
		return true;
	}
	
}
