package futurepack.common.block.plants;

import futurepack.api.Constants;
import futurepack.common.FuturepackMain;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.material.Material;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class PlantBlocks 
{
	public static final Block erse = HelperItems.setRegistryName(new BlockErse(Block.Properties.of(Material.PLANT).noCollission().randomTicks().strength(0F).sound(SoundType.GRASS)), Constants.MOD_ID, "erse");
	public static final Block glowmelo = HelperItems.setRegistryName(new BlockGlowmelo(Block.Properties.of(Material.PLANT).randomTicks().lightLevel(state -> 14).sound(SoundType.WOOL).strength(0.5F).noOcclusion()), Constants.MOD_ID, "glowmelo");
	public static final Block topinambur = HelperItems.setRegistryName(new BlockTopinambur(Block.Properties.of(Material.PLANT).noCollission().randomTicks().strength(0F).sound(SoundType.GRASS)), Constants.MOD_ID, "topinambur");
	public static final Block mendel_berry = HelperItems.setRegistryName(new BlockMendelBerry(Block.Properties.of(Material.PLANT).noCollission().randomTicks().strength(0F).sound(SoundType.GRASS)), Constants.MOD_ID, "mendel_berry");
	public static final Block oxades = HelperItems.setRegistryName(new BlockOxades(Block.Properties.of(Material.PLANT).noCollission().randomTicks().strength(0F).sound(SoundType.GRASS)), Constants.MOD_ID, "oxades");
	
	//should be terrain but are to complex because of light thing
	public static final Block leaves_tyros = HelperItems.setRegistryName(new BlockTyrosLeaves(Block.Properties.of(Material.LEAVES).strength(0.2F).randomTicks().sound(SoundType.GRASS).noOcclusion().isValidSpawn(Blocks::ocelotOrParrot).isSuffocating(Blocks::never).isViewBlocking(Blocks::never)), Constants.MOD_ID, "leaves_tyros");
	public static final Block sapling_tyros = HelperItems.setRegistryName(new BlockDirtSapling(new TyrosTree(), Block.Properties.of(Material.PLANT).noCollission().randomTicks().strength(0F).sound(SoundType.GRASS)), Constants.MOD_ID, "sapling_tyros");
	public static final Block sapling_mushroom = HelperItems.setRegistryName(new BlockSandSapling(new MenelausMushroom(), Block.Properties.of(Material.PLANT).noCollission().randomTicks().strength(0F).sound(SoundType.GRASS)), Constants.MOD_ID, "sapling_mushroom");
	public static final Block sapling_palirie = HelperItems.setRegistryName(new BlockDirtSapling(new PalirieTree(), Block.Properties.of(Material.PLANT).noCollission().randomTicks().strength(0F).sound(SoundType.GRASS)), Constants.MOD_ID, "sapling_palirie");
	
	public static void registerBlocks(RegistryEvent.Register<Block> event)
	{
		IForgeRegistry<Block> r = event.getRegistry();
		r.registerAll(erse, glowmelo, topinambur, mendel_berry, oxades, leaves_tyros, sapling_tyros, sapling_mushroom, sapling_palirie);
	}
	
	public static void registerItems(RegistryEvent.Register<Item> event)
	{
		IForgeRegistry<Item> r = event.getRegistry();
		r.registerAll(item(leaves_tyros), sapling(sapling_tyros), sapling(sapling_mushroom), sapling(sapling_palirie));
	}	
	
	private static Item item(Block bl)
	{
		return new BlockItem(bl, new Item.Properties().stacksTo(64).tab(FuturepackMain.tab_resources)).setRegistryName(HelperItems.getRegistryName(bl));
	}
	
	private static Item sapling(Block bl)
	{
		return new ItemBurnableBlock(bl, new Item.Properties().stacksTo(64).tab(FuturepackMain.tab_resources), 100).setRegistryName(HelperItems.getRegistryName(bl));
	}
}
