package futurepack.client.render.block;

import futurepack.api.Constants;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.resources.ResourceLocation;

public class ModelNeonEngine2 extends SegmentedBlockModel
{
	// This layer location should be baked with EntityRendererProvider.Context in
	// the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation(Constants.MOD_ID, "neon_engine"), "main");
	private final ModelPart root;
	private final ModelPart Eisenkern1;
	private final ModelPart Eisenkern2;
	private final ModelPart Eisenkern3;
	private final ModelPart Eisenkern4;
	
	public ModelNeonEngine2(ModelPart root) 
	{
		super(RenderType::entitySolid);
		
		this.root = root.getChild("root");
		Eisenkern1 = this.root.getChild("Eisenkern1");
		Eisenkern2 = this.root.getChild("Eisenkern2");
		Eisenkern3 = this.root.getChild("Eisenkern3");
		Eisenkern4 = this.root.getChild("Eisenkern4");
	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition root = partdefinition.addOrReplaceChild("root", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Energieleiter = root.addOrReplaceChild("Energieleiter", CubeListBuilder.create().texOffs(0, 40).addBox(-4.0F, -4.0F, -8.0F, 8.0F, 8.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition HalterH = root.addOrReplaceChild("HalterH", CubeListBuilder.create().texOffs(0, 21).addBox(-8.0F, -8.0F, 4.0F, 16.0F, 16.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Spulle1 = root.addOrReplaceChild("Spulle1", CubeListBuilder.create().texOffs(0, 0).addBox(-2.0F, -7.0F, -0.1F, 4.0F, 4.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.7854F));

		PartDefinition Spulle2 = root.addOrReplaceChild("Spulle2", CubeListBuilder.create().texOffs(0, 0).addBox(-2.0F, -7.0F, -0.1F, 4.0F, 4.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -0.7854F));

		PartDefinition Spulle3 = root.addOrReplaceChild("Spulle3", CubeListBuilder.create().texOffs(0, 0).addBox(-2.0F, -7.0F, -0.1F, 4.0F, 4.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -2.3562F));

		PartDefinition Spulle4 = root.addOrReplaceChild("Spulle4", CubeListBuilder.create().texOffs(0, 0).addBox(-2.0F, -7.0F, -0.1F, 4.0F, 4.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 2.3562F));

		PartDefinition HalterV = root.addOrReplaceChild("HalterV", CubeListBuilder.create().texOffs(0, 21).addBox(-8.0F, -8.0F, -3.0F, 16.0F, 16.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Eisenkern1 = root.addOrReplaceChild("Eisenkern1", CubeListBuilder.create().texOffs(17, 0).addBox(-1.0F, -10.0F, 1.0F, 2.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.7854F));

		PartDefinition Eisenkern2 = root.addOrReplaceChild("Eisenkern2", CubeListBuilder.create().texOffs(17, 0).addBox(-1.0F, -10.0F, 1.0F, 2.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 2.3562F));

		PartDefinition Eisenkern3 = root.addOrReplaceChild("Eisenkern3", CubeListBuilder.create().texOffs(17, 0).addBox(-1.0F, -10.0F, 1.0F, 2.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -2.3562F));

		PartDefinition Eisenkern4 = root.addOrReplaceChild("Eisenkern4", CubeListBuilder.create().texOffs(17, 0).addBox(-1.0F, -10.0F, 1.0F, 2.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -0.7854F));

		PartDefinition Fixirung = root.addOrReplaceChild("Fixirung", CubeListBuilder.create().texOffs(26, 0).addBox(-5.0F, -5.0F, -4.0F, 10.0F, 10.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 64, 64);
	}

	// fields
//	ModelPart Energieleiter;
//	ModelPart HalterH;
//	ModelPart Spulle1;
//	ModelPart Spulle2;
//	ModelPart Spulle3;
//	ModelPart Spulle4;
//	ModelPart HalterV;
//	ModelPart Fixirung;

//	Cube box1, box2, box3, box4;
/*
	public ModelNeonEngine2()
	{
		super(RenderType::entitySolid);

		texWidth = 64;
		texHeight = 64;

		Energieleiter = new ModelPart(this, 0, 40);
		Energieleiter.addBox(-4F, -4F, -8F, 8, 8, 16);
		Energieleiter.setPos(0F, 0F, 0F);
		Energieleiter.setTexSize(64, 64);
		Energieleiter.mirror = true;
		setRotation(Energieleiter, 0F, 0F, 0F);
		HalterH = new ModelPart(this, 0, 21);
		HalterH.addBox(-8F, -8F, 4F, 16, 16, 3);
		HalterH.setPos(0F, 0F, 0F);
		HalterH.setTexSize(64, 64);
		HalterH.mirror = true;
		setRotation(HalterH, 0F, 0F, 0F);
		Spulle1 = new ModelPart(this, 0, 0);
		Spulle1.addBox(-2F, -7F, -0.1F, 4, 4, 8);
		Spulle1.setPos(0F, 0F, 0F);
		Spulle1.setTexSize(64, 64);
		Spulle1.mirror = true;
		setRotation(Spulle1, 0F, 0F, 0.7853982F);
		Spulle2 = new ModelPart(this, 0, 0);
		Spulle2.addBox(-2F, -7F, -0.1F, 4, 4, 8);
		Spulle2.setPos(0F, 0F, 0F);
		Spulle2.setTexSize(64, 64);
		Spulle2.mirror = true;
		setRotation(Spulle2, 0F, 0F, -0.7853982F);
		Spulle3 = new ModelPart(this, 0, 0);
		Spulle3.addBox(-2F, -7F, -0.1F, 4, 4, 8);
		Spulle3.setPos(0F, 0F, 0F);
		Spulle3.setTexSize(64, 64);
		Spulle3.mirror = true;
		setRotation(Spulle3, 0F, 0F, -2.356194F);
		Spulle4 = new ModelPart(this, 0, 0);
		Spulle4.addBox(-2F, -7F, -0.1F, 4, 4, 8);
		Spulle4.setPos(0F, 0F, 0F);
		Spulle4.setTexSize(64, 64);
		Spulle4.mirror = true;
		setRotation(Spulle4, 0F, 0F, 2.356194F);
		HalterV = new ModelPart(this, 0, 21);
		HalterV.addBox(-8F, -8F, -3F, 16, 16, 3);
		HalterV.setPos(0F, 0F, 0F);
		HalterV.setTexSize(64, 64);
		HalterV.mirror = true;
		setRotation(HalterV, 0F, 0F, 0F);
		Eisenkern1 = new ModelPart(this, 17, 0);
		Eisenkern1.addBox(-1F, -10F, 1F, 2, 4, 2);
		Eisenkern1.setPos(0F, 0F, 0F);
		Eisenkern1.setTexSize(64, 64);
		Eisenkern1.mirror = true;
		setRotation(Eisenkern1, 0F, 0F, 0.7853982F);
		Eisenkern2 = new ModelPart(this, 17, 0);
		Eisenkern2.addBox(-1F, -10F, 1F, 2, 4, 2);
		Eisenkern2.setPos(0F, 0F, 0F);
		Eisenkern2.setTexSize(64, 64);
		Eisenkern2.mirror = true;
		setRotation(Eisenkern2, 0F, 0F, 2.356194F);
		Eisenkern3 = new ModelPart(this, 17, 0);
		Eisenkern3.addBox(-1F, -10F, 1F, 2, 4, 2);
		Eisenkern3.setPos(0F, 0F, 0F);
		Eisenkern3.setTexSize(64, 64);
		Eisenkern3.mirror = true;
		setRotation(Eisenkern3, 0F, 0F, -2.356194F);
		Eisenkern4 = new ModelPart(this, 17, 0);
		Eisenkern4.addBox(-1F, -10F, 1F, 2, 4, 2);
		Eisenkern4.setPos(0F, 0F, 0F);
		Eisenkern4.setTexSize(64, 64);
		Eisenkern4.mirror = true;
		setRotation(Eisenkern4, 0F, 0F, -0.7853982F);
		Fixirung = new ModelPart(this, 26, 0);
		Fixirung.addBox(-5F, -5F, -4F, 10, 10, 1);
		Fixirung.setPos(0F, 0F, 0F);
		Fixirung.setTexSize(64, 64);
		Fixirung.mirror = true;
		setRotation(Fixirung, 0F, 0F, 0F);

	}
*/
	@Override
	public ModelPart[] getParts()
	{
		return new ModelPart[] {root};
	}

	public void setOut(double d)
	{
		float[] offset = new float[] { 0 + 0.1F, 0.5F, 1F + 0.2F, 1.5F - 0.25F };
		float[][] axes = new float[][] { { -1F, 1F }, { -1, -1 }, { 1, -1 }, { 1, 1 } };
		ModelPart[] pistons = new ModelPart[] { Eisenkern1, Eisenkern2, Eisenkern3, Eisenkern4 };

		for (int i = 0; i < 4; i++)
		{
			float f = 1 + (float) Math.sin(2 * Math.PI * d + Math.PI * offset[i]);
			pistons[i].x = f * axes[i][0];
			pistons[i].y = f * axes[i][1];
		}
	}
}
