package futurepack.common.block.misc;

import java.util.Random;

import futurepack.api.FacingUtil;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.IntegerProperty;

public class BlockWirelessRedstoneTransmitter extends Block
{
	public static final IntegerProperty POWER = BlockStateProperties.POWER;
	public final boolean INVERTED;
	
	public BlockWirelessRedstoneTransmitter(Block.Properties props, boolean inverted)
	{
		super(props);
//		super(Material.IRON);
		INVERTED = inverted;
//		setCreativeTab(FPMain.tab_deco);
		
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void onPlace(BlockState state, Level w, BlockPos pos, BlockState oldState, boolean isMoving)
	{
		super.onPlace(state, w, pos, oldState, isMoving);
		if(oldState.getBlock()!=this)
		{
			w.scheduleTick(pos, this, 1);
		}
	}
	
	@Override
	public void neighborChanged(BlockState state, Level w, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving)
	{
		w.scheduleTick(pos, this, 1);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void onRemove(BlockState state, Level w, BlockPos pos, BlockState newState, boolean isMoving)
	{
		if(newState.getBlock()!=this)
		{
			BlockWirelessRedstoneReceiver.updateReceiversAround(w, pos, state.getValue(POWER), 0);
		}
		super.onRemove(state, w, pos, newState, isMoving);
	}
	
	@Override
	public void tick(BlockState state, ServerLevel w, BlockPos pos, Random random)
	{
		updateState(w, pos, state, state.getValue(POWER));
	}
	
	private void updateState(Level w, BlockPos pos, BlockState state, int oldPower)
	{
		int newPower = 0;
		for(Direction face : FacingUtil.VALUES)
		{
			int r = w.getSignal(pos.relative(face), face);		
			newPower = Math.max(newPower, r);
		}
		if(INVERTED)
			newPower = 15-newPower;
			
		if(oldPower!=newPower)
		{
			BlockWirelessRedstoneReceiver.updateReceiversAround(w, pos, oldPower, newPower);
			
			state = state.setValue(POWER, newPower);
			w.setBlock(pos, state, 3);
		}
	}
	
//	private int getOldPower(World w, BlockPos pos)
//	{
//		return w.getBlockState(pos).getValue(POWER);
//	}
//	

	
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(POWER);
	}
}
