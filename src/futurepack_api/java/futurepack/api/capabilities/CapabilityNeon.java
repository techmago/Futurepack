package futurepack.api.capabilities;

import net.minecraft.nbt.CompoundTag;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.util.INBTSerializable;


public class CapabilityNeon extends EnergyStorageBase implements INeonEnergyStorage, INBTSerializable<CompoundTag>
{	
	public static final Capability<INeonEnergyStorage> cap_NEON = CapabilityManager.get(new CapabilityToken<>(){});
	
//	public static class Storage implements IStorage<INeonEnergyStorage>
//	{
//
//		@Override
//		public Tag writeNBT(Capability<INeonEnergyStorage> capability, INeonEnergyStorage instance, Direction side)
//		{
//			return IntTag.valueOf(instance.get());
//		}
//
//		@Override
//		public void readNBT(Capability<INeonEnergyStorage> capability, INeonEnergyStorage instance, Direction side, Tag nbt)
//		{
//			if(nbt instanceof IntTag)
//			{
//				int power = ((IntTag)nbt).getAsInt();
//				
//				if(power > instance.get())
//				{
//					instance.add(power - instance.get());
//				}
//				else if(power < instance.get())
//				{
//					instance.use(instance.get() - power);
//				}
//			}
//		}
//
//	}

	public CapabilityNeon()
	{
		this(100, EnumEnergyMode.USE);
	}
	
	public CapabilityNeon(int maxpower, EnumEnergyMode mode)
	{
		super(maxpower, mode);
	}


	@Override
	public boolean canTransferTo(INeonEnergyStorage other)
	{
		return getType().getPriority() < other.getType().getPriority();
	}

	@Override
	public boolean canAcceptFrom(INeonEnergyStorage other)
	{
		return get() <= getMax();
	}

	@Override
	public CompoundTag serializeNBT()
	{
		CompoundTag nbt = new CompoundTag();
		nbt.putInt("p", get());
		return nbt;
	}

	@Override
	public void deserializeNBT(CompoundTag nbt)
	{
		this.energy = nbt.getInt("p");
	}
}
