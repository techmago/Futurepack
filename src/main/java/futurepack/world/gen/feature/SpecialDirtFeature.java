package futurepack.world.gen.feature;

import java.util.Random;

import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.Heightmap.Types;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.level.levelgen.feature.configurations.BlockStateConfiguration;
import net.minecraft.world.level.material.Material;
/**
 * BlockStateFeatureConfig because I use it as "1 blockstate" config
 */
public class SpecialDirtFeature extends Feature<BlockStateConfiguration> 
{

	public SpecialDirtFeature(Codec<BlockStateConfiguration> configFactoryIn) 
	{
		super(configFactoryIn);
	}

	@Override
	public boolean place(FeaturePlaceContext<BlockStateConfiguration> pContext) 
	{
		return place(pContext.level(), pContext.chunkGenerator(), pContext.random(), pContext.origin(), pContext.config());
	}
	
	public boolean place(WorldGenLevel w, ChunkGenerator p_241855_2_, Random rand, BlockPos pos, BlockStateConfiguration config) 
	{
		BlockPos.MutableBlockPos mut = new BlockPos.MutableBlockPos();
		
		for(int x=-8;x<8;x+=2)
		{
			for(int z=-8;z<8;z+=2)
			{
				mut.set(pos).move(x,0,z);
				BlockPos h = w.getHeightmapPos(Types.WORLD_SURFACE_WG, mut).below();
				if(w.isWaterAt(h))
				{
					genMenelausDirt(w, h, rand, config.state);
					return true;
				}
			}
		}
		return false;
	}
	
	
	public void genMenelausDirt(LevelAccessor w, BlockPos pos, Random r, BlockState fillerblock)
	{
		int radius = 10 + r.nextInt(5);
	
		for(int y=-radius/2;y<1;y++)
		{
			for(int x=-radius;x<radius;x++)
			{
				for(int z=-radius;z<radius;z++)
				{
					if(z*z + x*x - y*y < radius)
					{
						BlockPos xyz = pos.offset(x,y,z);
						BlockState state = w.getBlockState(xyz);
						if(state.getMaterial() != Material.WATER)
						{
							setBlock(w, xyz, fillerblock);
						}
					}
					
				}
			}
		}
	}


	
}
