package futurepack.common.block.misc;

import futurepack.api.interfaces.IBlockClientOnlyTickingEntity;
import futurepack.common.FPConfig;
import futurepack.common.FPSounds;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockHoldingTile;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.Mth;
import net.minecraft.util.StringRepresentable;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.level.material.PushReaction;

public class BlockAirlockDoor extends BlockHoldingTile implements IBlockClientOnlyTickingEntity<TileEntityAirlockDoor>
{
	public static final BooleanProperty EXTENDED = BlockStateProperties.EXTENDED;
	public static final EnumProperty<EnumAirlockDirection> DIRECTION = EnumProperty.create("direction", EnumAirlockDirection.class);
	
	private BlockState hangardoors;
	
	protected BlockAirlockDoor(Block.Properties props) 
	{
		super(props);
//		super(p_i45386_1_,15);
//		setCreativeTab(FPMain.tab_maschiens);
		hangardoors = Blocks.BARRIER.defaultBlockState();
		
	}
	
	@Override
	public int getLightBlock(BlockState state, BlockGetter worldIn, BlockPos pos) 
	{
		return 1;
	}
		
	@Override
	public RenderShape getRenderShape(BlockState state)
    {
        return RenderShape.INVISIBLE;
    }
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(EXTENDED, DIRECTION);
	}
	
	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		Direction face = context.getNearestLookingDirection().getOpposite();
		int r = Mth.floor(context.getRotation() * 4.0F / 360.0F + 0.5D) & 3;
		
		return super.getStateForPlacement(context).setValue(DIRECTION, EnumAirlockDirection.fromFacingAndRotation(face, r)).setValue(EXTENDED, true);
	}

	@Deprecated
	public void onPlace(BlockState state, Level worldIn, BlockPos pos, BlockState oldState, boolean isMoving) {
		super.onPlace(state, worldIn, pos, oldState, isMoving);
		
		this.neighborChanged(state, worldIn, pos, this, pos, isMoving);
	}
	
	public void setActivate(BlockState state, Level w, BlockPos pos, boolean b)
	{
		boolean active = state.getValue(EXTENDED);
		if(active != b)
		{
			if(!w.isClientSide)
			{
				((ServerLevel)w).playSound(null, pos.getX(), pos.getY(), pos.getZ(), FPSounds.AIRLOCK, SoundSource.BLOCKS, (float) (0.5F * FPConfig.CLIENT.volume_spacedoor.get()), 1F + 0.3F*w.random.nextFloat());
			}
			w.setBlock(pos, state.setValue(EXTENDED, b), 2);
		}
		
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void neighborChanged(BlockState state, Level w, BlockPos pos, Block p_149695_5_, BlockPos nBlock, boolean isMoving)
	{
		if(w.isClientSide)
			return;
		
		boolean b = w.getBestNeighborSignal(pos)<=0;
		
		Direction dir = state.getValue(DIRECTION).getFacing();
		if(b)
		{
			BlockPos xyz1 = pos.relative(dir);
			BlockPos xyz2 = xyz1.relative(dir);
			
//			Block b1 = w.getBlock(x1,y1,z1);
//			int meta1 = w.getBlockMetadata(x1, y1, z1);
//			Block b2 = w.getBlock(x2,y2,z2);
//			int meta2 = w.getBlockMetadata(x2, y2, z2);
			BlockState st1 = w.getBlockState(xyz1);
			BlockState st2 = w.getBlockState(xyz2);
			
			if(st1 == hangardoors && st2 == hangardoors)
				return;
			
			PushReaction rec1 = st1.getPistonPushReaction();
			if(rec1 == PushReaction.NORMAL && st1.getDestroySpeed(w, xyz1)< 0)
				rec1 = PushReaction.BLOCK;
				
			PushReaction rec2 = st2.getPistonPushReaction();
			if(rec2 == PushReaction.NORMAL && st2.getDestroySpeed(w, xyz2)< 0)
				rec2 = PushReaction.BLOCK;
			
			if(rec1== PushReaction.DESTROY || rec1== PushReaction.NORMAL || w.isEmptyBlock(xyz1) || st1==hangardoors)
			{
				if(st1!=hangardoors)
				{
					w.destroyBlock(xyz1, true);
					w.setBlock(xyz1, hangardoors, 2);
				}
				if(rec2== PushReaction.DESTROY || rec2== PushReaction.NORMAL || w.isEmptyBlock(xyz2) || st2==hangardoors)
				{
					if(st2!=hangardoors)
					{
						w.destroyBlock(xyz2, true);
						w.setBlock(xyz2, hangardoors, 2);
					}
					setActivate(state, w, pos, true);
				}			
			}
			
		}
		else
		{
			BlockPos xyz1 = pos.relative(dir);
			BlockPos xyz2 = xyz1.relative(dir);
			BlockState st1 = w.getBlockState(xyz1);
			BlockState st2 = w.getBlockState(xyz2);
			
			if(st1 == hangardoors)
			{
				w.removeBlock(xyz1, false);
			}
			
			if(st2 == hangardoors)
			{
				w.removeBlock(xyz2, false);
			}
			
			setActivate(state, w, pos, false);
		}
		super.neighborChanged(state, w, pos, p_149695_5_, nBlock, isMoving);
	}
	
	
	@Override
	public void onRemove(BlockState state, Level w, BlockPos pos, BlockState newState, boolean isMoving)
	{
		if(newState.getBlock()!=this)
		{
			Direction dir = state.getValue(DIRECTION).getFacing();
			
			BlockPos xyz1 = pos.relative(dir);
			BlockPos xyz2 = xyz1.relative(dir);
			BlockState st1 = w.getBlockState(xyz1);
			BlockState st2 = w.getBlockState(xyz2);
			
			if(st1 == hangardoors)
			{
				w.removeBlock(xyz1, false);
			}
			
			if(st2  == hangardoors)
			{
				w.removeBlock(xyz2, false);
			}
		}
		super.onRemove(state, w, pos, newState, isMoving);
	}
	
	@Override
	public BlockState rotate(BlockState state, Rotation rot)
	{
		return state.setValue(DIRECTION, state.getValue(DIRECTION).rotate(rot));
	}
	
	@Override
	public BlockState mirror(BlockState state, Mirror mir)
	{
		return state.setValue(DIRECTION, state.getValue(DIRECTION).mirror(mir));
	}
	
	public static enum EnumAirlockDirection implements StringRepresentable
	{
		DOWN_NS,
		DOWN_WE,
		UP_NS,
		UP_WE,
		NORTH,
		EAST,
		SOUTH,
		WEST;

		@Override
		public String getSerializedName() 
		{
			return this.name().toLowerCase();
		}
		
		public EnumAirlockDirection mirror(Mirror m)
		{
			if (m == Mirror.FRONT_BACK)
			{
				if(this == WEST)
					return EnumAirlockDirection.EAST;
				else if(this == EAST)
					return EnumAirlockDirection.WEST;
			}
			else if(m == Mirror.LEFT_RIGHT)
			{
				if ( this == NORTH)
					return SOUTH;
				else if(this == SOUTH)
					return NORTH;
			}
			
			return this;
		}
		
		public EnumAirlockDirection rotate(Rotation r)
		{
			switch (r)
			{
			case CLOCKWISE_90:
				return rotateY();
			case CLOCKWISE_180:
				return getOpposite();
			case COUNTERCLOCKWISE_90:
				return rotateYCCW();
			default:
				return this;
			}
		}
		
		public EnumAirlockDirection getOpposite()
		{
			switch (this)
			{
			case DOWN_NS:
				return UP_NS;
			case DOWN_WE:
				return UP_WE;
			case UP_NS:
				return DOWN_NS;
			case UP_WE:
				return DOWN_WE;
			case NORTH:
				return EnumAirlockDirection.SOUTH;
			case SOUTH:
				return EnumAirlockDirection.NORTH;
			case EAST:
				return EnumAirlockDirection.WEST;
			case WEST:
				return EnumAirlockDirection.EAST;
				
			default:
				return null;
			}
		}
		
		public EnumAirlockDirection rotateY()
		{
			switch (this)
			{
			case DOWN_NS:
				return DOWN_WE;
			case DOWN_WE:
				return DOWN_NS;
			case UP_NS:
				return UP_WE;
			case UP_WE:
				return UP_NS;
			case NORTH:
				return EnumAirlockDirection.EAST;
			case EAST:
				return EnumAirlockDirection.SOUTH;
			case SOUTH:
				return EnumAirlockDirection.WEST;
			case WEST:
				return EnumAirlockDirection.NORTH;
				
			default:
				return null;
			}
		}
		
		public EnumAirlockDirection rotateYCCW()
		{
			switch (this)
			{
			case DOWN_NS:
				return DOWN_WE;
			case DOWN_WE:
				return DOWN_NS;
			case UP_NS:
				return UP_WE;
			case UP_WE:
				return UP_NS;
			case NORTH:
				return EnumAirlockDirection.WEST;
			case EAST:
				return EnumAirlockDirection.NORTH;
			case SOUTH:
				return EnumAirlockDirection.EAST;
			case WEST:
				return EnumAirlockDirection.SOUTH;
				
			default:
				return null;
			}
		}
		
		public Direction getFacing()
		{
			switch (this)
			{
			case DOWN_NS:
				return Direction.DOWN;
			case DOWN_WE:
				return Direction.DOWN;
			case UP_NS:
				return Direction.UP;
			case UP_WE:
				return Direction.UP;
			case NORTH:
				return Direction.NORTH;
			case SOUTH:
				return Direction.SOUTH;
			case EAST:
				return Direction.EAST;
			case WEST:
				return Direction.WEST;
				
			default:
				return null;
			}
		}
		
		public static EnumAirlockDirection fromFacingAndRotation(Direction face, int rot)
		{
			switch (face)
			{
			case NORTH:
				return NORTH;
			case SOUTH:
				return SOUTH;
			case EAST:
				return EAST;
			case WEST:
				return WEST;
			case UP:
					return (rot == 1||rot==3) ? UP_NS : UP_WE;
			case DOWN:
				return (rot == 1|rot==3) ? DOWN_NS : DOWN_WE;
			default:
				return null;
			}
		}
	}

	@Override
	public BlockEntityType<TileEntityAirlockDoor> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.AIRLOCK_DDOR;
	}

	
}
