package futurepack.common.entity.living;

import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;

import futurepack.client.particle.ParticleFireflyMoving;
import futurepack.client.particle.ParticleFireflyStill;
import futurepack.common.FPEntitys;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.TamableAnimal;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.AttributeModifier.Operation;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.ai.goal.target.NonTameRandomTargetGoal;
import net.minecraft.world.entity.animal.Wolf;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.monster.Spider;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.scores.Team;

public class EntityJawaul extends Wolf 
{
	private static final EntityDataAccessor<Boolean> ALPHA = SynchedEntityData.defineId(EntityJawaul.class, EntityDataSerializers.BOOLEAN);

	private static final AttributeModifier ALPHA_HEALTH_BOOST     = new AttributeModifier(UUID.fromString("67576589-7686-5683-ADFF-33434456FEA6"), "Health boost", 10.0D, Operation.ADDITION);
	private static final AttributeModifier ALPHA_ATTACK_BOOST     = new AttributeModifier(UUID.fromString("67576589-7686-5683-ADFF-33434456FEA7"), "Attack boost", 2.0D, Operation.MULTIPLY_TOTAL);
	
	private float dominance = -1F;
	
	public EntityJawaul(EntityType<? extends EntityJawaul> type, Level worldIn) 
	{
		super(type, worldIn);
	}

	@Override
	protected void defineSynchedData() 
	{
	      super.defineSynchedData();
	      this.entityData.define(ALPHA, false);
	}
	
	@Override
	protected void registerGoals() 
	{
		super.registerGoals();
		
		this.targetSelector.addGoal(4, new NonTameRandomTargetGoal<>(this, Monster.class, false, new Predicate<LivingEntity>()
		{
			@Override
			public boolean test(LivingEntity t) 
			{
				if(t.getType() == FPEntitys.CRAWLER)
				{
					return ((EntityCrawler)t).isBaby() || getPackSize() >= 3;
				}
				return false;
			}
		}));
		this.targetSelector.addGoal(5, new NearestAttackableTargetGoal<>(this, EntityJawaul.class, 10, true, false, new Predicate<LivingEntity>() 
		{
			@Override
			public boolean test(LivingEntity t) 
			{
				EntityJawaul other = (EntityJawaul) t;
				
				if(other.isTame())
					return false;
				
				if(isAlpha())
				{
					if(!other.hasPackLeader() || other.isAlpha())//alphas battle each other
						return true;
				}
				else if(!hasPackLeader())
				{
					if(!other.hasPackLeader() && !other.isAlpha())//form pack by fight
						return true;
				}
				return false;
			}
		}));
	}
	
	private void setPackLeader(EntityJawaul alpha)
	{
		if(this.getOwner() instanceof Player || this.isTame())
		{
			throw new IllegalStateException("Tamed Jawaul dont have a wild leader, they have a player!");
		}
		
		this.setAlpha(false);
		alpha.setTarget(null);
		
		stopBeingAngry();
		
		if(alpha.isAlpha())
		{
			this.setOwnerUUID(alpha.getUUID());
		}
	}
	
	@Override
	public void setTame(boolean tamed) 
	{
		super.setTame(tamed);
	}
	
	@Override
	public void setOwnerUUID(UUID p_184754_1_) 
	{
		super.setOwnerUUID(p_184754_1_);
	}
	
	@Override
	public void setTarget(LivingEntity target) 
	{
		if(isAlpha())
		{
			double range = 20;
			AABB bb = new AABB(getX()-range, getY()-range, getZ()-range, getX()+range, getY()+range, getZ()+range);
			List<EntityJawaul> subordinates = this.level.getEntitiesOfClass(EntityJawaul.class, bb, new Predicate<EntityJawaul>() 
			{
				@Override
				public boolean test(EntityJawaul t) 
				{
					return t.isAlive() && t.isOwnedBy(EntityJawaul.this);
				}
			});
			for(EntityJawaul jawual : subordinates)
			{
				jawual.setTarget(target);
			}
		}
		super.setTarget(target);
	}
	
	public void addToPack(EntityJawaul coyote)
	{
		if(isAlpha())
		{
			packSize = -1;
			coyote.setPackLeader(this);
		}
		else if(hasPackLeader())
		{
			((EntityJawaul)getOwner()).addToPack(coyote);
		}
		else
		{
			setAlpha(true);
			packSize = -1;
			coyote.setPackLeader(this);
		}
	}
	
	public boolean isAlpha()
	{
		return this.entityData.get(ALPHA);
	}
	
	public void setAlpha(boolean isPackLeader)
	{
		this.entityData.set(ALPHA, isPackLeader);
		if(isPackLeader)
		{
			this.setOwnerUUID(null);
			if(!this.getAttribute(Attributes.ATTACK_DAMAGE).hasModifier(ALPHA_ATTACK_BOOST))
				this.getAttribute(Attributes.ATTACK_DAMAGE).addTransientModifier(ALPHA_ATTACK_BOOST);
			if(!this.getAttribute(Attributes.MAX_HEALTH).hasModifier(ALPHA_HEALTH_BOOST))
				this.getAttribute(Attributes.MAX_HEALTH).addTransientModifier(ALPHA_HEALTH_BOOST);
		}
		else
		{
			this.getAttribute(Attributes.ATTACK_DAMAGE).removeModifier(ALPHA_ATTACK_BOOST);
			this.getAttribute(Attributes.MAX_HEALTH).removeModifier(ALPHA_HEALTH_BOOST);
		}
	}
	
	@Override
	public void addAdditionalSaveData(CompoundTag compound) 
	{
		super.addAdditionalSaveData(compound);
		compound.putBoolean("Alpha", this.isAlpha());
		compound.putFloat("dominance", getDominance());
	}
	
	@Override
	public void readAdditionalSaveData(CompoundTag compound) 
	{
		super.readAdditionalSaveData(compound);
		this.setAlpha(compound.getBoolean("Alpha"));
		if(compound.contains("dominance"))
			this.dominance = compound.getFloat("dominance");
	}
	
	@Override
	protected SoundEvent getAmbientSound() 
	{
		if(hasSurrendered() && !hasPackLeader())
		{
			return SoundEvents.WOLF_WHINE;
		}
		return super.getAmbientSound();
	}
	
	public float getDominance()
	{
		if(dominance < 0)
		{
			dominance = this.random.nextFloat();
		}

		return dominance;
	}
	
	private int packSize = -1;
	
	public int getPackSize()
	{
		if(this.isAlpha())
		{
			if(packSize > 0)
				return packSize;
			else
			{
				double range = 20;
				AABB bb = new AABB(getX()-range, getY()-range, getZ()-range, getX()+range, getY()+range, getZ()+range);
				List<EntityJawaul> subordinates = this.level.getEntitiesOfClass(EntityJawaul.class, bb, new Predicate<EntityJawaul>() 
				{
					@Override
					public boolean test(EntityJawaul t) 
					{
						return t.isAlive() && t.isOwnedBy(EntityJawaul.this);
					}
				});
				return packSize = 1+subordinates.size();
			}
		}
		else
		{
			LivingEntity owner = this.getOwner();
			if(owner!= null && owner instanceof EntityJawaul)
			{
				if(((EntityJawaul)owner).isAlpha())
				{
					return ((EntityJawaul)owner).getPackSize();
				}
				else
				{
					this.setOwnerUUID(null);
					return 1;
				}
			}
			else
			{
				return 2;
			}
		}
	}
	
	@Override
	public LivingEntity getOwner() 
	{
		UUID id = getOwnerUUID();
		if(id!=null && this.level instanceof ServerLevel)
		{
			LivingEntity liv = (LivingEntity) ((ServerLevel)level).getEntity(id);
			if(liv instanceof EntityJawaul && !liv.isAlive())
			{
				setOwnerUUID(null);
				return null;
			}
			else if(liv == this)
			{
				setOwnerUUID(null);
				return null;
			}
			else if(liv == null)
			{
				return super.getOwner();
			}
			return liv;
		}
		return super.getOwner();
	}
	
	public boolean hasPackLeader()
	{
		if(getOwnerUUID() != null && getOwner() instanceof EntityJawaul && ((EntityJawaul)getOwner()).isAlpha())
		{
			if(this.isTame())
			{
				this.setTame(false);
				this.setOrderedToSit(false);
			}
			return true;
		}
		return false;
	}
	
	@Override
	public boolean canAttack(LivingEntity target) 
	{
		if(target instanceof TamableAnimal)
		{
			TamableAnimal tame = (TamableAnimal) target; 
			if(this.isAlpha())
			{
				if(this.getUUID().equals(tame.getOwnerUUID()))
				{
					return false;
				}
			}
			else if(hasPackLeader() && this.getOwnerUUID().equals(tame.getOwnerUUID()))
			{
				return false;
			}
			if(tame instanceof EntityJawaul)
			{
				if(((EntityJawaul)tame).hasSurrendered())
				{
					return false;
				}
			}
		}
		return super.canAttack(target);
	}
	
	private int alpha_timer = 0;
	
	@Override
	public void aiStep() 
	{
		super.aiStep();
		if(!level.isClientSide)
		{
			//LivingEntity liv = getOwner();
			//String woner = liv==null? "null" : liv.getClass().getSimpleName();
			//String s = "Alpaha: " + isAlpha() + " Tamed:" + isTamed() + " Owner:" + woner + " Packsize:" + getPackSize() + " OnwerId:" + getOwnerId();
			//this.setCustomName(new StringTextComponent(s));
			//this.setCustomNameVisible(true);
			
			if(isAlpha())
			{
				if(--alpha_timer < 0)
				{
					if(getPackSize() <= 1)
					{
						setAlpha(false);
						return;
					}
					else
					{
						alpha_timer = 20 * 60;
					}
				}
				
				double range = 2;
				AABB bb = new AABB(getX()-range, getY()-range, getZ()-range, getX()+range, getY()+range, getZ()+range);
				List<EntityJawaul> surrendered = this.level.getEntitiesOfClass(EntityJawaul.class, bb, new Predicate<EntityJawaul>() 
				{
					@Override
					public boolean test(EntityJawaul t) 
					{
						return t.isAlive() && t.hasSurrendered() && !t.hasPackLeader() && t!=EntityJawaul.this;
					}
				});
				if(!surrendered.isEmpty())
				{
					this.addToPack(surrendered.get(0));
					
					((ServerLevel)level).playSound(null, getX(), getY(), getZ(), SoundEvents.WOLF_HOWL, SoundSource.NEUTRAL, 0.3F, 0.8F + random.nextFloat()*0.2F);
					
				}
			}
			else
			{
				if(!hasPackLeader() && !hasSurrendered())
				{
					double range = 2;
					AABB bb = new AABB(getX()-range, getY()-range, getZ()-range, getX()+range, getY()+range, getZ()+range);
					List<EntityJawaul> surrendered = this.level.getEntitiesOfClass(EntityJawaul.class, bb, new Predicate<EntityJawaul>() 
					{
						@Override
						public boolean test(EntityJawaul t) 
						{
							return t.isAlive() && t.hasSurrendered();
						}
					});
					if(!surrendered.isEmpty())
					{
						this.setAlpha(true);
						alpha_timer = 20 * 60;
						((ServerLevel)level).playSound(null, getX(), getY(), getZ(), SoundEvents.WOLF_GROWL, SoundSource.NEUTRAL, 0.9F, 0.8F + random.nextFloat()*0.2F);
					}
				}
				else if(hasPackLeader())
				{
					if(random.nextFloat() < 0.01)
					{
						this.addEffect(new MobEffectInstance(MobEffects.REGENERATION, 20));
					}
				}
				
//				TODO: strength boost for pack with size
			
//				TODO: pickup small creatures
			}
		}
		else
		{
			if(isAlpha())
			{
				addFireFlies();
			}
		}
		
	}
	
	/**
	 * alpha is Zinogre
	 */
	private void addFireFlies()
	{
		if(this.random.nextInt(10) == 0)
		{
			if(level.isClientSide)
			{
				Runnable r = new Runnable()
				{
					@Override
					public void run() 
					{
						float x = (float) (getX() + random.nextFloat() - 0.5F);
						float y = (float) (getY() + getEyeHeight() + random.nextFloat() - 0.5F);
						float z = (float) (getZ() + random.nextFloat() - 0.5F);
						
						ParticleFireflyStill fly = new ParticleFireflyMoving((ClientLevel) level,x,y,z, random.nextFloat()-0.5, random.nextFloat()-0.5, random.nextFloat()-0.5);
						fly.initRandomColor(0x9900ff, 0x00FFff);
						fly.setLifetime(30);
						Minecraft.getInstance().particleEngine.add(fly); 
						
						fly = new ParticleFireflyStill((ClientLevel) level, x, y, z);
						fly.initRandomColor(0x9900ff, 0x00FFff);
						fly.setLifetime(40);
						Minecraft.getInstance().particleEngine.add(fly);
					}
				};
				r.run();
			}
		}
	}
	
	@Override
	public Team getTeam() 
	{
		if(hasPackLeader())
		{
			return getOwner().getTeam();
		}
		return super.getTeam();
	}
	
	@Override
	public boolean isAlliedTo(Entity entityIn) 
	{	
		if (this.isTame()) 
		{
			LivingEntity owner = this.getOwner();
			if (entityIn == owner) 
			{
				return true;
			}

			if (owner != null) 
			{
				return owner.isAlliedTo(entityIn);
			}
		}
		return super.isAlliedTo(entityIn);
	}
	
	@Override
	public void die(DamageSource cause) 
	{
		if(isAlpha())
		{
			Entity arrowShooter = cause.getEntity();
			if(arrowShooter != null && arrowShooter instanceof EntityJawaul)
			{
				switchLeader((EntityJawaul)arrowShooter);
			}
			else
			{
				//find new leader among followers
				double range = 20;
				AABB bb = new AABB(getX()-range, getY()-range, getZ()-range, getX()+range, getY()+range, getZ()+range);
				List<EntityJawaul> subordinates = this.level.getEntitiesOfClass(EntityJawaul.class, bb, new Predicate<EntityJawaul>() 
				{
					@Override
					public boolean test(EntityJawaul t) 
					{
						return t.isAlive() && t.isOwnedBy(EntityJawaul.this);
					}
				});
				float dominance = 0F;
				EntityJawaul newDom = null;
				for(EntityJawaul sub : subordinates)
				{
					if(sub.getDominance() > dominance)
					{
						newDom = sub;
						dominance = sub.getDominance();
					}
				}
				if(newDom!=null)
				{
					newDom.setOwnerUUID(null);
					newDom.setAlpha(true);
					
					for(EntityJawaul sub : subordinates)
					{
						if(sub!=newDom)
							sub.switchLeader(newDom);
					}
				}
			}
		}
		super.die(cause);
	}
	
	public void switchLeader(EntityJawaul newAlpha)
	{
		if(isAlpha())
		{
			double range = 20;
			AABB bb = new AABB(getX()-range, getY()-range, getZ()-range, getX()+range, getY()+range, getZ()+range);
			List<EntityJawaul> subordinates = this.level.getEntitiesOfClass(EntityJawaul.class, bb, new Predicate<EntityJawaul>() 
			{
				@Override
				public boolean test(EntityJawaul t) 
				{
					return t.isAlive() && t.isOwnedBy(EntityJawaul.this);
				}
			});
			for(EntityJawaul subCoyote : subordinates)
			{
				subCoyote.switchLeader(newAlpha);
			}
		}
		
		if(this.isAlive())
		{
			newAlpha.addToPack(this);
		}
	}
	
	public boolean hasSurrendered()
	{
		if(this.isTame())
			return false;
		
		float nearDeath = 1F - (getHealth() / getMaxHealth());
		return nearDeath >= getDominance();
	}
	
	@Override
	public boolean hurt(DamageSource source, float amount) 
	{
		if(source.getEntity() instanceof EntityJawaul)
		{
			if(getHealth() < amount && hasSurrendered())
			{
				if(isAlpha())
				{
					this.setAlpha(false);
				}
				return false;
			}
		}
		else if(source.getEntity() instanceof Spider)
		{
			amount *= 0.5F; // damage reduction from spiders
		}
		return super.hurt(source, amount);
	}
	
	@Override
	public void killed(ServerLevel world, LivingEntity entityLivingIn) 
	{
		super.killed(world, entityLivingIn);
		
		this.addEffect(new MobEffectInstance(MobEffects.REGENERATION, 20*10, 5));
		this.addEffect(new MobEffectInstance(MobEffects.DAMAGE_RESISTANCE, 20*5));
		
		if(!isTame())
		{
			EntityJawaul boss = this.isAlpha() ? this : (EntityJawaul) this.getOwner();
			if(boss!=null)
			{
				double range = 7;
				AABB bb = new AABB(getX()-range, getY()-range, getZ()-range, getX()+range, getY()+range, getZ()+range);
				List<EntityJawaul> mates = this.level.getEntitiesOfClass(EntityJawaul.class, bb, new Predicate<EntityJawaul>() 
				{
					@Override
					public boolean test(EntityJawaul t) 
					{
						return t.isAlive() && t.isOwnedBy(boss);
					}
				});
				if(boss != this)
				{
					boss.addEffect(new MobEffectInstance(MobEffects.REGENERATION, 20*10, 2));
				}
				for(EntityJawaul mate : mates)
				{
					mate.addEffect(new MobEffectInstance(MobEffects.REGENERATION, 20*10, 1));
				}
			}
		}
	}
	
	@Override
	public boolean canBeControlledByRider() 
	{
		return this.isTame();
	}
}
