package futurepack.common;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import futurepack.api.Constants;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class FPSounds
{
	public static final Sounds GRENADE_SHOT = new Sounds(new ResourceLocation(Constants.MOD_ID,"grenadeshot"));
	public static final Sounds LOAD = new Sounds(new ResourceLocation(Constants.MOD_ID, "loade"));
	public static final Sounds ELEKTROFELD = new Sounds(new ResourceLocation(Constants.MOD_ID, "elektrofeld"));
	public static final Sounds GENERATOR = new Sounds(new ResourceLocation(Constants.MOD_ID, "generator"));
	public static final Sounds JUMP = new Sounds(new ResourceLocation(Constants.MOD_ID, "spacejump"));
	public static final Sounds OPTIBENCH = new Sounds(new ResourceLocation(Constants.MOD_ID, "optibench"));
	public static final Sounds AIRLOCK = new Sounds(new ResourceLocation(Constants.MOD_ID, "airlock"));
	public static final Sounds PART_PRESS = new Sounds(new ResourceLocation(Constants.MOD_ID, "part_press"));
	public static final Sounds CORELASER = new Sounds(new ResourceLocation(Constants.MOD_ID, "corelaser"));
	public static final Sounds SHIELD_HIT = new Sounds(new ResourceLocation(Constants.MOD_ID, "shield_hit"));
	public static final Sounds DEFIBRILATOR = new Sounds(new ResourceLocation(Constants.MOD_ID, "defibrilator"));

	public static final Sounds SOUNDTRACK = new Sounds(new ResourceLocation(Constants.MOD_ID, "record.futurepack"));
	public static final Sounds MENELAUS = new Sounds(new ResourceLocation(Constants.MOD_ID, "record.menelaus"));
	public static final Sounds TYROS = new Sounds(new ResourceLocation(Constants.MOD_ID, "record.tyros"));
	public static final Sounds ENTROS = new Sounds(new ResourceLocation(Constants.MOD_ID, "record.entros"));
	public static final Sounds UNKNOWN = new Sounds(new ResourceLocation(Constants.MOD_ID, "record.unknown"));
	public static final Sounds ENVIA = new Sounds(new ResourceLocation(Constants.MOD_ID, "record.envia"));
	public static final Sounds LYRARA = new Sounds(new ResourceLocation(Constants.MOD_ID, "record.lyrara"));


	public static void register(RegistryEvent.Register<SoundEvent> event)
	{
		Field[] fields = FPSounds.class.getFields();

		for(Field f : fields)
		{
			if(Modifier.isStatic(f.getModifiers()))
			{
				try
				{
					Object o = f.get(null);

					if(o instanceof Sounds)
					{
						Sounds se = (Sounds) o;
						se.register(event.getRegistry());
					}
				}
				catch (IllegalArgumentException e)
				{
					e.printStackTrace();
				}
				catch (IllegalAccessException e)
				{
					e.printStackTrace();
				}

			}
		}
	}


	private static class Sounds extends SoundEvent
	{
		private final ResourceLocation registerName;

		public Sounds(ResourceLocation name)
		{
			super(name);
			this.registerName = name;
		}

		public void register(IForgeRegistry<SoundEvent> reg)
		{
			if(HelperItems.getRegistryName(this)!=registerName)
				this.setRegistryName(registerName);
			reg.register(this);
		}

	}

//	public static void playSound()
//	{
//		SoundHandler handler = Minecraft.getInstance().getSoundHandler();
//		handler.
//	}
}
