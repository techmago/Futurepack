package futurepack.common.gui.escanner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.ItemPredicateBase;
import futurepack.common.recipes.industrialfurnace.FPIndustrialNeonFurnaceManager;
import futurepack.common.recipes.industrialfurnace.IndNeonRecipe;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperJSON;
import net.minecraft.world.item.ItemStack;

public class ComponentIndustrialNeonFurnace extends ComponentIndsutrialfurnaceBase
{
	private List<Integer> support;
	
	public ComponentIndustrialNeonFurnace(JsonObject obj)
	{
		super(null, null);
		if(obj.has("load"))
		{
			List<IndNeonRecipe> list = null;
			if(obj.get("load").isJsonPrimitive())
			{
				String s = obj.get("load").getAsString();
				list = Arrays.asList(FPIndustrialNeonFurnaceManager.instance.getMatchingRecipes(s));
			}
			else if(obj.get("load").isJsonArray())
			{
				list = new ArrayList<IndNeonRecipe>();
				for(JsonElement elm : obj.getAsJsonArray("load"))
				{
					String s = elm.getAsString();
					list.addAll(Arrays.asList(FPIndustrialNeonFurnaceManager.instance.getMatchingRecipes(s)));
				}
			}
			if(list!=null)
				bakeItems(list);
		}
		else if(obj.has("slots") && obj.get("slots").isJsonObject())
		{
			JsonObject jo = obj.getAsJsonObject("slots");
			in = new List[3];
			out = new List[3];
			for(int i=0;i<3;i++)
			{
				in[i] = HelperJSON.getItemFromJSON(jo.get("in" + (i+1)), true);
				out[i] = HelperJSON.getItemFromJSON(jo.get("out" + (i+1)), true);
				HelperJSON.setupRendering(in[i]);
				HelperJSON.setupRendering(out[i]);
			}
			support = Arrays.asList(obj.get("support").getAsInt());
			
			
			//debug: -- id search
			boolean print = false;
			for(IndNeonRecipe r : FPIndustrialNeonFurnaceManager.instance.recipes)
			{
				ItemStack o = r.getOutput();
				for(int i=0;i<out.length;i++)
				{
					for(ItemStack is : out[i])
					{
						if(o.sameItem(is))
						{
							System.out.println("\t" + r + " [" + is + "]");
							print = true;
						}
					}
				}
			}
			if(!print)
				System.out.println("\tUnknown");
		}
		
	}

	private void bakeItems(List<IndNeonRecipe> list)
	{
		support = new ArrayList<Integer>(list.size());
		List<ItemStack>[][] inputs = new List[3][list.size()];
		List<ItemStack>[] outputs = new List[list.size()];
		
		for(int i=0;i<list.size();i++)
		{
			IndNeonRecipe r =  list.get(i);
			outputs[i] = new ArrayList<>();
			outputs[i].add(r.getOutput());
			ItemPredicateBase[] input = r.getInputs();
			int minSize = 1;
			for(int j=0;j<3;j++)
			{
				if(j<input.length)
				{
					inputs[j][i] = input[j].collectAcceptedItems(new ArrayList<ItemStack>());
					minSize = Math.max(minSize, inputs[j][i].size());
				}
				else
				{
					inputs[j][i] = new ArrayList<>();
					inputs[j][i].add(ItemStack.EMPTY);
				}
			}
			
			HelperComponent.fillListToSameSize(new List[]{outputs[i], inputs[0][i], inputs[1][i], inputs[2][i]}, minSize);
			support.add(r.getSupport());
		}
		
		in = new List[3];
		out = new List[3];
		
		out[0] = new ArrayList<ItemStack>();
		for(List<ItemStack> l : outputs)
			out[0].addAll(l);
		HelperJSON.setupRendering(out[0]);
		out[1] = Collections.emptyList();
		out[2] = Collections.emptyList();
		
		for(int i=0;i<3;i++)
		{
			in[i] = new ArrayList<>();
			for(List<ItemStack> l : inputs[i])
				in[i].addAll(l);
			HelperJSON.setupRendering(in[i]);
		}
	}
	
	@Override
	public int getHeight()
	{	
		//return 82;
		return super.getHeight();
	}
	
	@Override
	public void render(PoseStack matrixStack, int x, int y, int blitOffset, int mouseX, int mouseY, GuiScannerBase gui)
	{
		super.render(matrixStack, x, y, blitOffset, mouseX, mouseY, gui);
		
		HelperComponent.renderIndNeonFurn(matrixStack, x+5, y+23+16, blitOffset);
		HelperComponent.renderIndNeonFurn(matrixStack, x+23, y+23+16, blitOffset);
		HelperComponent.renderIndNeonFurn(matrixStack, x+41, y+23+16, blitOffset);
		
		HelperComponent.renderSupport(matrixStack, x+5, y+3, blitOffset); //y+23+18+16
		gui.getMinecraft().font.draw(matrixStack, HelperComponent.getStack(support)+"SP", x+5+18,y+3+5, 0x3a3a3a);//y+23+18+16+5
	}
}
