package futurepack.common.item.tools.compositearmor;

import java.util.List;

import javax.annotation.Nullable;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public abstract class ItemModulArmorBase extends Item
{
	public final EquipmentSlot position;
	
	public ItemModulArmorBase(EquipmentSlot slot, Item.Properties props)
	{
		super(props.stacksTo(1));
		this.position = slot;
//		if(slot.getSlotType()!=Type.ARMOR)
//			throw new IllegalStateException("can not add an ArmorModul to a non armor position");	
	}
	
	public abstract void onArmorTick(Level world, Player player, ItemStack it, CompositeArmorInventory inv);

	public boolean isSlotFitting(ItemStack stack, EquipmentSlot type, @Nullable CompositeArmorPart armorPart)
	{
		return type == position;
	}
	
	@OnlyIn(Dist.CLIENT)
	@Override
	public void appendHoverText(ItemStack stack, Level w, List<Component> tooltip, TooltipFlag advanced)
	{
		tooltip.add(new TranslatableComponent("tooltip.composite.can_be_installed"));
		for(int i = EquipmentSlot.values().length - 1; i > 0; i--)
		{
			EquipmentSlot slot = EquipmentSlot.values()[i];
			if(isSlotFitting(stack, slot, null))
				tooltip.add(new TranslatableComponent("tooltip.composite.part." + slot.getName()));
		}
		super.appendHoverText(stack, w, tooltip, advanced);
	}
}
