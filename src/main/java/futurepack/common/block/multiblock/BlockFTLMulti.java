package futurepack.common.block.multiblock;

import com.google.common.base.Predicate;

import futurepack.common.item.tools.ToolItems;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Vec3i;
import net.minecraft.util.StringRepresentable;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.phys.BlockHitResult;

public class BlockFTLMulti extends Block implements Predicate<BlockState>
{
	public static EnumProperty<Direction.Axis> direction = EnumProperty.create("direction", Direction.Axis.class, Direction.Axis.X, Direction.Axis.Z);
	public static BooleanProperty left = BooleanProperty.create("left");
	public static EnumProperty<EnumPart> part = EnumProperty.create("part", EnumPart.class);
	public static BooleanProperty top = BooleanProperty.create("top");
	
	private final Predicate<BlockState> update;
	
	public BlockFTLMulti(Block.Properties props)
	{
		super(props);
//		super(Material.IRON);
//		setCreativeTab(FPMain.tab_deco);
		registerDefaultState(defaultBlockState().setValue(direction, Direction.Axis.X).setValue(left, false).setValue(part, EnumPart.OFF).setValue(top, false));
		update = new UpdatePredicate();
	}
	
	@Override
	public RenderShape getRenderShape(BlockState state)
    {
        return RenderShape.MODEL;
    }


	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		builder.add(direction, left, part, top);
		super.createBlockStateDefinition(builder);
	}
	
//	@Override
//	public IBlockState getStateFromMeta(int meta)
//	{
//		IBlockState state =  getDefaultState();
//		state = state.with(part, EnumPart.values()[meta & 3]);
//		state = state.with(top, (meta & 4) != 0);
//		state = state.with(direction, (meta&8)!=0 ? EnumFacing.Axis.X : EnumFacing.Axis.Z);
//		
//		return state;
//	}
//	
//	@Override
//	public int getMetaFromState(IBlockState state)
//	{
//		EnumPart part = state.get(BlockFTLMulti.part);
//		if(part==EnumPart.OFF)
//		{
//			return 0;
//		}
//		else
//		{
//			int meta = part.ordinal();
//			meta |= state.get(top)? 4 : 0;// 1>>2 = 4
//			meta |= state.get(direction)==Axis.X ? 8 : 0;
//			return meta;
//		}
//	}
	
//	@Override
//	public IBlockState getActualState(IBlockState state, IWorldReader w, BlockPos pos)
//	{
//		if(state.get(direction)==Axis.X)
//		{
//			IBlockState st = w.getBlockState(pos.add(0,0,-1));
//			boolean left = st.getBlock()==this;
//			state = state.with(BlockFTLMulti.left, left);
//		}
//		else
//		{
//			IBlockState st = w.getBlockState(pos.add(-1,0,0));
//			boolean left = st.getBlock()==this;
//			state = state.with(BlockFTLMulti.left, left);
//		}
//		return state;
//	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void onPlace(BlockState state, Level w, BlockPos pos, BlockState oldState, boolean isMoving)
	{
//		if(oldState.getBlock() != this)
//		{
//			BlockPos start = pos;
//			while(apply(w.getBlockState(start.add(0,-1,0))))
//			{
//				start = start.add(0,-1,0);
//			}
//			while(apply(w.getBlockState(start.add(-1,0,0))))
//			{
//				start = start.add(-1,0,0);
//			}
//			while(apply(w.getBlockState(start.add(0,0,-1))))
//			{
//				start = start.add(0,0,-1);
//			}
//			
//			activateMultiBlock(w, start);
//		}
		
		super.onPlace(state, w, pos, oldState, isMoving);
	}

	private void activateMultiBlock(Level w, BlockPos start) 
	{
		while(apply(w.getBlockState(start.offset(0,-1,0))))
		{
			start = start.offset(0,-1,0);
		}
		while(apply(w.getBlockState(start.offset(-1,0,0))))
		{
			start = start.offset(-1,0,0);
		}
		while(apply(w.getBlockState(start.offset(0,0,-1))))
		{
			start = start.offset(0,0,-1);
		}
		
		if(checkExistance(w, start, new Vec3i(2, 2, 3), this))
		{
			for(int x=0;x<2;x++)
			{
				for(int y=0;y<2;y++)
				{
					for(int z=0;z<3;z++)
					{
						onBuildingFinished(w, start, x, y, z, false);
					}
				}
			}
		}
		else if(checkExistance(w, start, new Vec3i(3, 2, 2), this))
		{
			for(int x=0;x<3;x++)
			{
				for(int y=0;y<2;y++)
				{
					for(int z=0;z<2;z++)
					{
						onBuildingFinished(w, start, x, y, z, true);
					}
				}
			}
		}
	}
	
	
	
	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player player, InteractionHand hand, BlockHitResult hit)
	{
		if(player.getItemInHand(hand).getItem() == ToolItems.scrench)
		{
			if(state.getValue(part) == EnumPart.OFF)
			{
				activateMultiBlock(w, pos);
				return InteractionResult.SUCCESS;
			}
		}
		
		return super.use(state, w, pos, player, hand, hit);
	}
	
	@Override
	public void onRemove(BlockState state, Level worldIn, BlockPos pos, BlockState newState, boolean isMoving) 
	{
		if(state.getValue(part) != EnumPart.OFF)
		{
			BlockPos start = pos;
			if(state.getValue(top))
				start = start.below();
			
			int d;
			switch(state.getValue(part))
			{
			case START:
				d=0;
				break;
			case MIDDLE:
				d=1;
				break;
			case END:
				d=2;
				break;
			default:
				d=0;
				break;
			}
			switch (state.getValue(direction))
			{
			case X:
				start = start.offset(-d, 0, 0);
				break;
			case Z:
				start = start.offset(0, 0, -d);
			default:
				break;
			}
			if(state.getValue(left))
			{
				switch (state.getValue(direction))
				{
				case X:
					start = start.offset(0, 0, -1);
					break;
				case Z:
					start = start.offset(-1, 0, 0);
				default:
					break;
				}
			}
			
			for(BlockPos p : BlockPos.betweenClosed(start, start.offset(2, 1, 2)))
			{
				resetBlock(worldIn, p);
			}
		}
		
		super.onRemove(state, worldIn, pos, newState, isMoving);
	}
	
	
	
	/**
	 * 
	 * @param w the World
	 * @param start the start position of the multiblock
	 * @param x the x postion from the start
	 * @param y the y postion from the start
	 * @param z the z postion from the start
	 * @param xz true: x is the longer side; false: z is the longer side
	 */
	public void onBuildingFinished(Level w, BlockPos start, int x, int y, int z, boolean xz)
	{
		BlockPos pos = start.offset(x,y,z);
		BlockState state = w.getBlockState(pos);
		if(xz)
		{
			state = state.setValue(direction, Direction.Axis.X);
			EnumPart epart;
			switch (x)
			{
			case 0:
				epart=EnumPart.START;
				break;
			case 1:
				epart=EnumPart.MIDDLE;
				break;
			case 2:
				epart=EnumPart.END;
				break;
			default:
				epart=EnumPart.OFF;
			}
			state = state.setValue(part, epart);
			state = state.setValue(top, y==1);
			
			BlockState st = w.getBlockState(pos.offset(0,0,-1));
			boolean left = st.getBlock()==this;
			state = state.setValue(BlockFTLMulti.left, left);
		}
		else
		{
			state = state.setValue(direction, Direction.Axis.Z);
			EnumPart epart;
			switch (z)
			{
			case 0:
				epart=EnumPart.START;
				break;
			case 1:
				epart=EnumPart.MIDDLE;
				break;
			case 2:
				epart=EnumPart.END;
				break;
			default:
				epart=EnumPart.OFF;			
			}
			state = state.setValue(part, epart);
			state = state.setValue(top, y==1);
			
			BlockState st = w.getBlockState(pos.offset(-1,0,0));
			boolean left = st.getBlock()==this;
			state = state.setValue(BlockFTLMulti.left, left);
		}	
		w.setBlock(pos, state, 2);
	}
	
	private void resetBlock(Level w, BlockPos pos)
	{
		if(w.getBlockState(pos).getBlock() == this)
		{
			w.setBlock(pos, defaultBlockState(), 2);
		}
	}
	
	
	@Override
	public boolean apply(BlockState state)
	{
		return state.getBlock() == this && state.getValue(part) == EnumPart.OFF;
	}
	
	public static enum EnumPart implements StringRepresentable
	{
		OFF, START, MIDDLE, END;

		@Override
		public String getSerializedName()
		{
			return this.name().toLowerCase();
		}
		
	}
	/**
	 * 
	 * @param w The World
	 * @param start The Start position to check th building (must be at the x/y/z min args)
	 * @param xyz the x/y/z dimension of the building
	 * @param pred checks if the building blocks are right
	 */
	public static boolean checkExistance(Level w, BlockPos start, Vec3i xyz, Predicate<BlockState> pred)
	{
		for(int x=0;x<xyz.getX();x++)
		{
			for(int y=0;y<xyz.getY();y++)
			{
				for(int z=0;z<xyz.getZ();z++)
				{
					BlockPos pos = start.offset(x,y,z);
					if(!pred.apply(w.getBlockState(pos)))
					{
						return false;
					}
				}
			}
		}
		return true;
	}
	
	private class UpdatePredicate implements Predicate<BlockState>
	{

		@Override
		public boolean apply(BlockState input)
		{
			return input.getBlock() == BlockFTLMulti.this;
		}
		
	}
}
