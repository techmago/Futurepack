package futurepack.generating.fillableModel;

public class FillableBlock {
	public String changingState;
	public int minLevel = 0;
	public int maxLevel;
	public String rotationState = "facing";
	public String texturePath;
	public String baseModel;
	
	public FillableBlock(String changingState, int maxLevel, String baseModel, String texturePath) {
		this.changingState = changingState;
		this.maxLevel = maxLevel;
		this.baseModel = baseModel;
		this.texturePath = texturePath;
	}
	
	public FillableBlock(String changingState, int maxLevel, String baseModel, String texturePath, String rotationState) {
		this(changingState, maxLevel, baseModel, texturePath);
		this.rotationState = rotationState;
	}
}
