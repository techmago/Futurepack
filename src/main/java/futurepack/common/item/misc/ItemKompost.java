package futurepack.common.item.misc;

import java.util.Random;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.BlockParticleOption;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.BoneMealItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.BonemealableBlock;
import net.minecraft.world.level.block.GrassBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;

public class ItemKompost extends Item
{

	public ItemKompost(Item.Properties props)
	{
		super(props);
	}

	@Override
	public InteractionResult useOn(UseOnContext context)
	{
		if(applyCompost(context.getItemInHand(), context.getLevel(), context.getClickedPos(), context.getPlayer()))
		{
			return InteractionResult.SUCCESS;
		}
		return super.useOn(context);
	}
	
	public static boolean applyCompost(ItemStack it, Level w, BlockPos pos, Player pl)
	{
		BlockState IBlockState = w.getBlockState(pos);
		
		//on dirt/grass			
		Block b = w.getBlockState(pos).getBlock();
		BlockState b1 = w.getBlockState(pos.offset(0, 1, 0));
		if((b instanceof GrassBlock || b ==  Blocks.DIRT) && (w.isEmptyBlock(pos.offset(0, 1, 0)) || !b1.isSolidRender(w, pos.offset(0, 1, 0))))
		{
			w.playLocalSound(pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, SoundEvents.GRASS_STEP, SoundSource.NEUTRAL, 10.0F, 1.0F, true);
			
			if(w.isClientSide)
			{
				spawnCompostParticleTypes(w, w.random, pos.offset(0, 1, 0), 24, 1.0F, 0.1F);
			}
			
			w.setBlockAndUpdate(pos, Blocks.PODZOL.defaultBlockState());
			if(w.random.nextInt(4) == 0 && b1.getMaterial() == Material.AIR)
				w.setBlockAndUpdate(pos.offset(0, 1, 0), Blocks.TALL_GRASS.defaultBlockState());
			
	        if (!pl.isCreative())
	        {
	        	it.shrink(1);
	        }
	        
	        return true;
		}
		
		//auf Pflanzen
		if (IBlockState.getBlock() instanceof BonemealableBlock)
		{
			if(((BonemealableBlock)IBlockState.getBlock()).isValidBonemealTarget(w, pos, IBlockState, w.isClientSide))
			{
				
				w.playLocalSound(pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, SoundEvents.GRASS_STEP, SoundSource.NEUTRAL, 10.0F, 1.0F, true);

				if(w.isClientSide)
					spawnCompostParticleTypes(w, w.random, pos, 16, 0.8F, 0.1F);
				
				if(w.random.nextInt(2) == 0)
				{
					
	                if (BoneMealItem.applyBonemeal(it, w, pos, pl))
	                {
	                    if (!w.isClientSide)
	                    {
	                    	w.levelEvent(2005, pos, 0);
	                    } 
	                }
				}
                else if(!pl.isCreative())
    	        {
                	it.shrink(1);
    	        }
				
				return true;
			}	
		}
		
		return false;
	}
		
	private static void spawnCompostParticleTypes(Level w, Random rand, BlockPos pos, int amount, float spread, float speed) 
	{
		
		for (int i = 0; i < amount; ++i) 
		{	
        	float x = rand.nextFloat() * spread - spread / 2.0F;
        	float z = rand.nextFloat() * spread - spread / 2.0F;
        	
        	if(rand.nextBoolean())
        	{
	            w.addParticle(
	            	new BlockParticleOption(ParticleTypes.BLOCK, Blocks.OAK_LEAVES.defaultBlockState()), 
	            	pos.getX() + 0.5F + x,
	            	pos.getY() + rand.nextFloat() * 0.4F + 0.8F , 
	            	pos.getZ() + 0.5F + z, 
	            	rand.nextFloat() * speed * (x < 0.0F ? -1.0F : 1.0F), 
	            	rand.nextFloat() * -0.3, 
	            	rand.nextFloat() * speed * (z < 0.0F ? -1.0F : 1.0F)
	            );
        	}
        	else
        	{
        		w.addParticle(
        			new BlockParticleOption(ParticleTypes.BLOCK, Blocks.SPRUCE_LOG.defaultBlockState()), 
	                pos.getX() + 0.5F + x,
	                pos.getY() + rand.nextFloat() * 0.4F + 0.8F , 
	                pos.getZ() + 0.5F + z, 
	                rand.nextFloat() * speed * (x < 0.0F ? -1.0F : 1.0F), 
	                rand.nextFloat() * -0.3, 
	                rand.nextFloat() * speed * (z < 0.0F ? -1.0F : 1.0F)
	            );
        	}
        }
	}
}
