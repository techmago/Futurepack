package futurepack.common.potions;

import futurepack.common.FuturepackMain;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.effect.MobEffectCategory;
import net.minecraft.world.entity.LivingEntity;

public class PotionParalyze extends MobEffect
{

	protected PotionParalyze()
	{
		super(MobEffectCategory.NEUTRAL, 0xe4ff04);
		//this.registerPotionAttributeModifier(Attributes.movementSpeed, "7107DE5E-7CE8-4030-940E-514C1F160890", -0.15000000596046448D, 2);
	}

	@Override
	public void applyEffectTick(LivingEntity liv, int lvl)
	{
		super.applyEffectTick(liv, lvl);
		double y = liv.getDeltaMovement().y;
		if(y>0)
			y=0;
		if(liv.isInWaterOrRain())
			y-=0.01;
		liv.setDeltaMovement(0, y, 0);
		
		liv.swinging=false;
		if(liv.level.random.nextInt(256-lvl)==0)
		{
			liv.hurt(FuturepackMain.NEON_DAMAGE, 1);
		}
	}
}
