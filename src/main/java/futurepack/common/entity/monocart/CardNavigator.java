package futurepack.common.entity.monocart;

import java.util.LinkedList;

import com.mojang.math.Vector3f;

import futurepack.api.ParentCoords;
import futurepack.common.block.logistic.monorail.BlockMonorailBasic;
import futurepack.common.block.logistic.monorail.BlockMonorailBasic.EnumMonorailStates;
import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.DustParticleOptions;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;

public class CardNavigator
{
	private final EntityMonocartBase cart;
	private LinkedList<Vec3> path; 
	
	public CardNavigator(EntityMonocartBase par1)
	{
		cart = par1;
	}
	
	/**
	 * Clears the current path and calculate the new one.
	 * @param target
	 */
	public void initPath(ParentCoords target)
	{
		path = new LinkedList<Vec3>();
		while(target!=null)
		{
			BlockState state = cart.level.getBlockState(target);
			if(state.getBlock() instanceof BlockMonorailBasic)
			{
				EnumMonorailStates es = state.getValue(BlockMonorailBasic.getPropertie((BlockMonorailBasic) state.getBlock()));
				if(es.isYLoocked() || path.isEmpty())
				{
					path.addFirst(convert(target));
				}
				else
				{
					Vec3 vec1=new Vec3(target.getX(), target.getY(), target.getZ());
					Vec3 vec2=new Vec3(target.getX(), target.getY(), target.getZ());
					switch(es)
					{
					case ASCENDING_EAST:
						vec1 = vec1.add(0, 0.5, 0.5);
						vec2 = vec2.add(1, 1.5, 0.5);						
						break;
					case ASCENDING_NORTH:
						vec1 = vec1.add(0.5, 0.5, 1);
						vec2 = vec2.add(0.5, 1.5, 0);						
						break;
					case ASCENDING_SOUTH:
						vec1 = vec1.add(0.5, 0.5, 0);
						vec2 = vec2.add(0.5, 1.5, 1);						
						break;
					case ASCENDING_WEST:
						vec1 = vec1.add(1, 0.5, 0.5);
						vec2 = vec2.add(0, 1.5, 0.5);						
						break;
					case UP_DOWN:
						vec1 = vec1.add(0.5, 0.25, 0.5);
						vec2 = vec2.add(0.5, 0.75, 0.5);	
						break;
					case NORTH_EAST_SOUTH_WEST_DOWN:
						vec1 = vec1.add(0.5, 0.5, 0.5);
						vec2 = vec2.add(0.5, 0, 0.5);
						break;
					case NORTH_EAST_SOUTH_WEST_UP:
						vec1 = vec1.add(0.5, 1, 0.5);
						vec2 = vec2.add(0.5, 0.5, 0.5);				
						break;
					default:break;
					}
					
					if(vec1.distanceToSqr(path.getFirst()) > vec2.distanceToSqr(path.getFirst()))
					{
						addChecked(vec2);
						addChecked(vec1);
					}
					else
					{
						addChecked(vec1);
						addChecked(vec2);
					}
				}
				
				if(cart.level.isClientSide)
				{
					
					cart.level.addParticle(new DustParticleOptions(new Vector3f(1, 0, 0), 1), target.getX()+0.5, target.getY()+0.5, target.getZ()+0.5, 0, 0, 0);
				}
			}
			
			target = target.getParent();
		}
	}
	
	private void addChecked(Vec3 vec)
	{
		Vec3 now = path.getFirst();
		if(!now.equals(vec))
		{
			if(cart.level.isClientSide)
			{
				cart.level.addParticle(ParticleTypes.MYCELIUM, vec.x, vec.y, vec.z, 0, 0, 0);
				cart.level.addParticle(ParticleTypes.MYCELIUM, vec.x, vec.y, vec.z, 0, 0, 0);
				cart.level.addParticle(ParticleTypes.MYCELIUM, vec.x, vec.y, vec.z, 0, 0, 0);
				cart.level.addParticle(ParticleTypes.MYCELIUM, vec.x, vec.y, vec.z, 0, 0, 0);
				cart.level.addParticle(ParticleTypes.MYCELIUM, vec.x, vec.y, vec.z, 0, 0, 0);
				cart.level.addParticle(ParticleTypes.MYCELIUM, vec.x, vec.y, vec.z, 0, 0, 0);
			}
			path.addFirst(vec);
		}
	}
	
	protected Vec3 getPathEnd()
	{
		return this.path.getLast();
	}
	
	protected Vec3 getNext()
	{
		return path.getFirst();
	}
	
	public void doTasks()
	{
			
//		BlockPos pos = new BlockPos(cart);
		
		if(path!=null)
		{
			Vec3 next = getNext();
			
			if(!cart.canPass(next))
			{
				cart.setDeltaMovement(Vec3.ZERO);
				deletePath();
				
				
				if(cart.level instanceof ServerLevel)
				{
					((ServerLevel)cart.level).sendParticles(ParticleTypes.NOTE, next.x, next.y, next.z, 1, 0F,0F,0F, 0.1);
				}
				
				return;
			}
			
			Vec3 motion = getMotionToBlock(next);
			calcRotaion(motion);
			
			float f = cart.getSpeed();

			cart.setDeltaMovement(motion.scale(f));
			
			AABB bb = cart.getBoundingBox();
			bb = bb.inflate(Math.abs(cart.getDeltaMovement().x), Math.abs(cart.getDeltaMovement().y), Math.abs(cart.getDeltaMovement().z)).move(0, -0.2, 0);
			boolean flag1 = bb.contains(next);
			if(flag1)
			{
				path.removeFirst();
				if(path.isEmpty())
				{
					path = null;
					if(cart.level instanceof ServerLevel)
					{
						((ServerLevel)cart.level).sendParticles(ParticleTypes.HEART, next.x, next.y, next.z, 1, 0F,0F,0F, 0.1);
					}
				}
			}
		}
		else
		{
			
			cart.setDeltaMovement(0, cart.getDeltaMovement().y, 0);
		}
	}
	
	
	
	private Vec3 getMotionToBlock(Vec3 pos)
	{
		Vec3 entity = cart.position();
		return  pos.subtract(entity).normalize();
	}
	
	private void calcRotaion(Vec3 vec)
	{
			
		double pitch = -Math.asin(vec.y); //neigung
		double yaw = -Math.atan2(vec.x, vec.z); //drehwinkel
		cart.setXRot((float) Math.toDegrees(pitch));
		cart.setYRot((float) Math.toDegrees(yaw));
		
		int[] pitch1 = new int[]{45,0,-45};
		int[] yaw1 = new int[]{0, 90, 180, -90, -180};
		
		BlockPos pos = cart.blockPosition();
		if(cart.level.isEmptyBlock(pos))
		{
			pos = pos.below();
		}
		BlockState state = cart.level.getBlockState(pos);
				
		if(state.getBlock() instanceof BlockMonorailBasic)
		{
			EnumMonorailStates es = state.getValue(BlockMonorailBasic.getPropertie((BlockMonorailBasic) state.getBlock()));
			pitch1 = es.getPitch();
		}
		cart.setXRot(getNearest(Math.round(cart.getXRot()), pitch1));
	}
	
	private int getNearest(int t1, int... ints)
	{
		if(ints.length==0)
			return t1;
			
		int[] t2 = new int[ints.length];
		for(int i=0;i<ints.length;i++)
		{
			t2[i] = Math.abs(ints[i] - t1);
		}
		
		int pos = 0;
		for(int i=0;i<t2.length;i++)
		{
			if(t2[i] < t2[pos])
			{
				pos = i;
			}
		}
		
		return ints[pos];
	}
	

	public boolean pathless()
	{
		return path==null;
	}

	public void deletePath()
	{
		path = null;
	}
	
	private static Vec3 convert(BlockPos pos)
	{
		return pos==null ? null : new Vec3(pos.getX()+0.5, pos.getY()+0.4, pos.getZ()+0.5);
	}
}
