package futurepack.common.gui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import it.unimi.dsi.fastutil.ints.Int2ObjectArrayMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import net.minecraft.ChatFormatting;

public class FormatedTextEditor 
{
	private ArrayList<String> lines;
	private Int2ObjectMap<String> formatedLines;
	
	private BiFunction<Integer, FormatedTextEditor, String> formatter; 
	
	public FormatedTextEditor(BiFunction<Integer, FormatedTextEditor, String> formatter) 
	{
		lines = new ArrayList<String>();
		formatedLines = new Int2ObjectArrayMap<String>();
		this.formatter = formatter;
	}
	
	public int getLines()
	{
		return lines.size();
	}
	
	public String getRawLine(int line)
	{
		try
		{
		if(lines.isEmpty())
			return "";
		else
			return lines.get(line);
		}
		catch(IndexOutOfBoundsException e)
		{
			e.printStackTrace();
			return "@?#";
		}
	}
	
	public String getLineFormatted(int line)
	{
		return formatedLines.computeIfAbsent(line, i -> {
			return formatter.apply(i, FormatedTextEditor.this);
		});
	}
	
	public void insertChar(int line, int pos, char c)
	{
		String base = getRawLine(line);
		
		if(pos > base.length())
			throw new IndexOutOfBoundsException("length: " + base.length() + " but index: " + pos);
		
		StringBuilder build = new StringBuilder(base);
		build.insert(pos, c);
		
		
		
		String out = build.toString();
		String[] newLines = out.split("\n");
		if(newLines.length==0)
		{
			newLines = new String[] {""};
		}
		if(out.endsWith("\n"))
		{
			newLines = Arrays.copyOf(newLines, newLines.length+1);
			newLines[newLines.length-1] = "";
		}
		
		replaceLines(line, newLines);
	}
	
	public void insertString(int line, int pos, CharSequence str)
	{
		String base = getRawLine(line);

		StringBuilder build = new StringBuilder(base);
		build.insert(pos, str);
		String out = build.toString();
		String[] newLines = out.split("\n");
		replaceLines(line, newLines);
	}
	
	public void insertChars(int line, int pos, char[] str)
	{
		String base = getRawLine(line);

		StringBuilder build = new StringBuilder(base);
		build.insert(pos, str);
		String out = build.toString();
		String[] newLines = out.split("\n");
		replaceLines(line, newLines);
	}
	
	
	public void replaceLines(int originalLines, String...replacements)
	{
		if(!lines.isEmpty())
			lines.remove(originalLines);
		
		lines.addAll(originalLines, Arrays.asList(replacements));
		
		notifyFormattingChangeFrom(originalLines);
	}
	
	public void removeLines(int first_line, int start_pos, int last_line, int end_pos)
	{
		try
		{
			if(first_line == last_line)//remove something in line
			{
				String l = lines.get(first_line);
				
				String a = l.substring(0, start_pos);
				String b = l.substring(end_pos);
				
				lines.set(first_line, a+b);
			}
			else
			{
				String a = lines.get(first_line);
				String b = lines.get(last_line);
				
				String fixed1 = a.substring(0, start_pos);
				String fixed2 = b.substring(end_pos);
				lines.set(first_line, fixed1+fixed2);
				a=null;
				b=null;
				
				for(int line=last_line;line>first_line;line--)
				{
					lines.remove(line);
				}
			}
			
			notifyFormattingChangeFrom(first_line);
		}
		catch (IndexOutOfBoundsException e) 
		{
			e.printStackTrace();
		}
	}
	
	public void notifyFormattingChange(IntStream lines)
	{
		lines.forEach(formatedLines::remove);
	}
	
	public void notifyFormattingChangeFrom(int lineStart)
	{
		for(int i=lineStart;i<getLines();i++)
		{
			formatedLines.remove(i);
		}
	}
	
	public static class JavaScriptFormatter implements BiFunction<Integer, FormatedTextEditor, String>
	{
		public static final String[] keywords = ("break,case,catch,continue,debugger,default,delete,do,else,finally,for,function,if,in,instanceof,new,return,switch,this,throw,try,typeof,var,void,while,with,null,true,false,"
				+ "class,const,enum,export,extends,import,super,implements,interface,let,package,private,protected,public,static,yield,NaN,Infinity,undefined,int,byte,char,goto,long,final,float,short,double,native,"
				+ "throws,boolean,abstract,volatile,transient,synchronized").split(",");
		
		public static final String format_keywords = ChatFormatting.BOLD.toString() + ChatFormatting.LIGHT_PURPLE.toString();
		public static final String format_numbers = ChatFormatting.GREEN.toString();
		public static final String format_comments = ChatFormatting.ITALIC.toString() + ChatFormatting.GOLD;
		public static final String format_string = ChatFormatting.AQUA.toString();
		
		public static final String format_reset = ChatFormatting.RESET.toString(); 
		
		@Override
		public String apply(Integer  line, FormatedTextEditor u) 
		{
			String lineText = u.getRawLine(line).replaceAll("\t", "    ");
			try
			{
				lineText = applyBase(lineText);
				lineText = applyMultilineComments(lineText, line, u);
				return lineText;
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				return lineText;
			}
		}
		
		private List<TextArea> commentAreas = new ArrayList<TextArea>();
		
		private String applyMultilineComments(String s, int line, FormatedTextEditor u)
		{
			String start = "/*";
			String end = "*/";
			
			Iterator<TextArea> iter = commentAreas.iterator();
			while(iter.hasNext())
			{
				TextArea a = iter.next();
				if(a.beginLine >= line)
					iter.remove();
				else if(a.endLine >= line)
				{
					a.endLine = -1;
					a.endPos = -1;
				}
			}
			
			boolean inComment = isLineComment(line, 0);
			boolean invalidated = false;
			
			int index = 0;
			while(index < s.length())
			{
				if(inComment)
				{
					int end_pos = s.indexOf(end, index);
					index = end_pos + end.length();
					if(end_pos >= 0)
					{
						for(TextArea area : commentAreas)
						{
							if(area.isOpen() && area.isInArea(line, end_pos))
							{
								area.endLine = line;
								area.endPos = end_pos + end.length();
								inComment = false;
								invalidateLines(line+1, u);
								break;
							}
						}
					}
					else
					{
						return format_comments + removeFormating(s);
					}
				}
				else
				{
					int start_pos = s.indexOf(start, index);
					index = start_pos + start.length();
					if(start_pos >= 0)
					{
						if(!invalidated)
						{
							invalidateLines(line, u);
							invalidated = true;
						}
						
						int end_pos = s.indexOf(end, index);
						if(end_pos >= start_pos)
						{
							index = end_pos + end.length();
							commentAreas.add(new TextArea(line, line, start_pos, end_pos + end.length()));
						}
						else
						{
							//this comment does not end here
							commentAreas.add(new TextArea(line, -1, start_pos, -1));
							break;
						}
					}
					else
					{
						break;
					}
				}
			}
			int shift = 0;
			StringBuilder builder = new StringBuilder(s);
			try
			{
				for(TextArea areas : commentAreas)
				{
					if(areas.beginLine == line)
					{
						if(areas.endLine == line)
						{
							//only part is a comment
							String str = builder.substring(areas.beginPos+shift, areas.endPos+shift);
							int len1 = str.length();
							str = removeFormating(str);
							
							str = format_comments + str + format_reset;
							
							builder.replace(areas.beginPos+shift, areas.endPos+shift, str);
							shift = str.length() - len1;
						}
						else
						{
							//rest of line is a comment
							String str = builder.substring(areas.beginPos+shift);
							int len1 = str.length();
							str = removeFormating(str);
							
							str = format_comments + str + format_reset;
							
							builder.replace(areas.beginPos+shift, areas.beginPos+shift+len1, str);
							shift = str.length() - len1;
						}
					}
					else if(areas.endLine == line)
					{
						//start of line is comment
						String str = builder.substring(0, areas.endPos+shift);
						int len1 = str.length();
						str = removeFormating(str);
						
						str = format_comments + str + format_reset;
						
						builder.replace(0, areas.endPos+shift, str);
						shift = str.length() - len1;
					}
				}
			}
			catch(StringIndexOutOfBoundsException e)
			{
				commentAreas.clear();
			}
			return builder.toString();
			
		}
		
		private boolean isLineComment(int line, int pos)
		{
			return isPosInside(commentAreas, line, pos);
		}

		private static String applyBase(String base)
		{
			base = applyKeywords(base);
			base = applyNumbers(base);
			base = applyStrings(base);
			base = applyComments(base);
			
			return base;
		}
		
		
		private static String applyKeywords(String base)
		{
			base = " " + base + " ";
			for(String key : keywords)
			{
				base = base.replaceAll("([^A-Za-z0-9_\u00a7.]+)(" + key + ")([^A-Za-z0-9_\u00a7.]+)", "$1" + format_keywords + key + format_reset + "$3");
			}
			return base.substring(1, base.length()-1);
		}
		
		private static String applyNumbers(String base)
		{
			return base.replaceAll("([^A-Za-z0-9_\u00a7]{1})([\\d]+)", "$1" + format_numbers+"$2"+format_reset);
		}
		
		private static String applyComments(String base)
		{
			int pos = base.indexOf("//");
			if(pos>=0)
			{
				String start = base.substring(0, pos);
				String end = base.substring(pos);
				
				end = removeFormating(end);
				
				base = start + format_comments + end;
			}
			
			return base;
		}
		
		private static String removeFormating(String s)
		{
			return s.replaceAll("\u00a7[0-9a-z]", ""); //remove all formating
		}
		
		private static String applyStrings(String base)
		{
			char[] stringchars = new char[] {'\'', '\"'};
			List<TextArea> stringAreas = new ArrayList<TextArea>();
			
			for(char c : stringchars)
			{
				boolean inString = false;
				int start = 0;
				while(start < base.length())
				{
					int pos = base.indexOf(c, start);
					if(pos==-1)
						break;
					
					if(!isPosInside(stringAreas, 0, pos))
					{
						if(inString)
						{
							stringAreas.get(stringAreas.size()-1).endPos = pos +1;
							inString = false;
						}
						else
						{
							stringAreas.add(new TextArea(0, 0, pos, -1));
							inString = true;
						}
					}
					start = pos+1;
				}
				if(inString)
				{
					stringAreas.get(stringAreas.size()-1).endPos = base.length();
				}
			}
			
			//use list to add formating
			StringBuilder builder = new StringBuilder(base);
			int shift = 0;
			for(TextArea strings : stringAreas)
			{
				String str = builder.substring(strings.beginPos+shift, strings.endPos+shift);
				int len1 = str.length();
				str = removeFormating(str);
				
				str = format_string + str + format_reset;
				
				builder.replace(strings.beginPos+shift, strings.endPos+shift, str);
				shift = str.length() - len1;
				
			}		
			return builder.toString();
		}
		
		private static boolean isPosInside(List<TextArea> list, int line, int pos)
		{
			for(TextArea area : list)
			{
				if(area.isInArea(line, pos))
					return true;
			}
			return false;
		}
		
		private void invalidateLines(int start, FormatedTextEditor u)
		{
			u.notifyFormattingChangeFrom(start);
			commentAreas.removeIf(t -> (t.beginLine >= start));
		}
		
	}
	
	public static void main(String[] args) 
	{
		JavaScriptFormatter format = new JavaScriptFormatter();
		FormatedTextEditor editor = new FormatedTextEditor(format);
		
		String test = "var /*lol*/ geg7 = \"jk var lo\" + /*~*/ test + \" AHA double \"; //var test commant 56\nfunction d() {}\n/*\nTest this\n var\n\ng\n*/ var d = 45;";
		editor.insertString(0, 0, test);
		
		long s = System.currentTimeMillis();
		for(int i=0;i<editor.getLines();i++)
		{
			System.out.println(editor.getLineFormatted(i));
		}
		long s2 = System.currentTimeMillis();
		System.out.println(s2 -s);
	}

	public void clear() 
	{
		lines.clear();
		formatedLines.clear();
	}

	public void addAll(List<String> newLines) 
	{
		this.lines.addAll(newLines);
	}
	
	public String getFullText()
	{
		return lines.stream().collect(Collectors.joining("\n"));
	}
}
