package futurepack.client;

import java.util.ArrayList;
import java.util.function.Function;

import futurepack.client.render.RenderDebugSelectors;
import futurepack.client.render.RenderGleiter;
import futurepack.client.render.RenderRoomAnalyzer;
import futurepack.client.render.block.RenderLogistic;
import futurepack.common.FPConfig;
import futurepack.common.ManagerGleiter;
import futurepack.common.block.modification.machines.TileEntityInfusionGenerator;
import futurepack.common.entity.living.EntityAlphaJawaul;
import futurepack.common.gui.GuiFPWorldLoading;
import futurepack.common.item.tools.ToolItems;
import futurepack.common.player.FakePlayerSP;
import futurepack.common.research.CustomPlayerData;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessageRequestJawaulInventory;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.world.dimensions.atmosphere.AtmosphereManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.screens.BackupConfirmScreen;
import net.minecraft.client.gui.screens.GenericDirtMessageScreen;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.gui.screens.inventory.InventoryScreen;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraftforge.client.event.FOVModifierEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderHandEvent;
import net.minecraftforge.client.event.RenderLevelLastEvent;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.client.event.ScreenOpenEvent;
import net.minecraftforge.client.gui.ForgeIngameGui;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;


public class ClientEvents
{
	public static final ClientEvents INSTANCE = new ClientEvents();

	private ClientEvents()
	{

	}

	private static ArrayList<Function<RenderLevelLastEvent, MultiBufferSource>> renderers = new ArrayList<>();

	public static Function<RenderLevelLastEvent, MultiBufferSource> addDeferedRendering(Function<RenderLevelLastEvent, MultiBufferSource> callback)
	{
		renderers.add(callback);
		return callback;
	}

	private static void finishDrawing(MultiBufferSource buffer)
	{
		if(buffer instanceof MultiBufferSource.BufferSource)
		{
			((MultiBufferSource.BufferSource) buffer).endBatch();
		}
	}

	@SubscribeEvent
	public void onRenderWorld(RenderLevelLastEvent event)
	{
		renderers.stream().map(f -> f.apply(event)).forEach(ClientEvents::finishDrawing);
		renderers.clear();

		RenderDebugSelectors.render(event.getPartialTick(), event.getPoseStack());

		ItemStack rooma = ItemStack.EMPTY;
		@SuppressWarnings("resource")
		Player pl = Minecraft.getInstance().player;

		if(pl.getMainHandItem().getItem() == ToolItems.roomanalyzer)
		{
			rooma = pl.getMainHandItem();
		}
		else if(pl.getOffhandItem().getItem() == ToolItems.roomanalyzer)
		{
			rooma = pl.getOffhandItem();
		}
		if(!rooma.isEmpty())
		{
			pl.level.getProfiler().push("futurepack_air_overlay");
			RenderRoomAnalyzer.render(event.getPartialTick(), event.getPoseStack());
			pl.level.getProfiler().pop();
		}

		boolean mainH = !pl.getItemInHand(InteractionHand.MAIN_HAND).isEmpty() && pl.getItemInHand(InteractionHand.MAIN_HAND).getItem() == ToolItems.logisticEditor;
		boolean offH = !pl.getItemInHand(InteractionHand.OFF_HAND).isEmpty() && pl.getItemInHand(InteractionHand.OFF_HAND).getItem() == ToolItems.logisticEditor;

		if(mainH || offH)
		{
			RenderLogistic.onItemHeld(pl.getItemInHand(mainH ? InteractionHand.MAIN_HAND : InteractionHand.OFF_HAND), event.getPartialTick(), event.getPoseStack());
		}

	}

	@SubscribeEvent
	public void onItemPulling(FOVModifierEvent event)
	{
		ItemStack it = event.getEntity().getUseItem();
		if(!it.isEmpty())
		{
			int j = it.getUseDuration() - event.getEntity().getUseItemRemainingTicks();
			float f;
			if(it.getItem() == ToolItems.laser_bow)
			{
				f = j / ( 20F / (1 + EnchantmentHelper.getItemEnchantmentLevel(Enchantments.POWER_ARROWS, it)) );
			}
			else if(it.getItem() == ToolItems.grenade_launcher)
			{
				f = j /20F;
			}
			else
			{
				return;
			}

			f = (f * f + f * 2.0F) / 3.0F;
			if (f < 0.1D || event.getEntity().getUseItemRemainingTicks()==0)
			{
				return;
			}
			if (f > 1.0F)
			{
				f = 1.0F;
			}
			event.setNewfov(event.getFov()-(f*0.5F));
		}
	}

	@SubscribeEvent
	public static void registerModels(ModelRegistryEvent event)
	{
//		try
//		{
//			FPBlocks.setupPreRendering();
//		}
//		catch(NoSuchMethodError e){}
//		FPBlocks.setupRendering();
//		FPItems.setupRendering();

	}

	@SubscribeEvent
	public void airOverlayPre(RenderGameOverlayEvent.PreLayer event)
	{
		//FIXME: there is now a IIngameOverlay for this

		if(event.getOverlay() == ForgeIngameGui.AIR_LEVEL_ELEMENT)
		{
			Minecraft mc = Minecraft.getInstance();
			if(!AtmosphereManager.hasWorldOxygen(mc.level))
			{
				Entity e = mc.getCameraEntity();
				if(mc.player == e)
				{
					Minecraft.getInstance().setCameraEntity(new FakePlayerSP(mc.player));
				}
			}
		}
	}

	@SubscribeEvent
	public void airOverlayPost(RenderGameOverlayEvent.PostLayer event)
	{
		if(event.getOverlay() == ForgeIngameGui.AIR_LEVEL_ELEMENT)
		{
			Minecraft mc = Minecraft.getInstance();
			if(!AtmosphereManager.hasWorldOxygen(mc.level))
			{

				if(mc.getCameraEntity().getClass()==FakePlayerSP.class)
				{
					Minecraft.getInstance().setCameraEntity(mc.player);
				}
			}
			if(System.currentTimeMillis() - lastUpdate < 2000)
			{
				int guiX=0, guiY=0;
				guiX = Minecraft.getInstance().getWindow().getGuiScaledWidth()/2;
				guiY = Minecraft.getInstance().getWindow().getGuiScaledHeight()-59;
				int tank = Math.round(20 * tankFill);
				for(int i=0;i<10;i++)
				{
					int id = tank >= 2 ? 30 : tank >= 1 ? 31 : 32;
					HelperComponent.renderSymbol(event.getMatrixStack(), guiX + 83 -8 * (i), guiY, 0, id);//id 30-32
					tank -= 2;
				}

			}
		}
	}

	private static long lastUpdate = 0;
	private static float tankFill = 0F;

	public static void setAirTanks(float full)
	{
		lastUpdate = System.currentTimeMillis();
		tankFill = full;
	}

	@SubscribeEvent
	public void onRenderPlayerModel(RenderPlayerEvent.Post render)
	{
		if(ManagerGleiter.isGleiterOpen(render.getPlayer()))
		{
			RenderGleiter.onRender(render);
		}
	}

	@SuppressWarnings("resource")
	@SubscribeEvent
	public void onRenderPlayerFirstPerson(RenderHandEvent event)
	{
		if(event.getHand()== InteractionHand.MAIN_HAND)
		{
			if(ManagerGleiter.isGleiterOpen(Minecraft.getInstance().player))
			{
				RenderGleiter.onRender(event);
			}
		}
	}

	@SubscribeEvent
	public void onGuiOpen(ScreenOpenEvent event)
	{
		Screen gui = event.getScreen();
		if (gui instanceof BackupConfirmScreen && FPConfig.CLIENT.futurepackSkipBackupScreen.get())
		{
			Minecraft th = Minecraft.getInstance();
			//gui.init(th, th.getMainWindow().getScaledWidth(), th.getMainWindow().getScaledHeight());

			BackupConfirmScreen cbs = (BackupConfirmScreen)gui;

			if(cbs.getTitle() instanceof TranslatableComponent)
			{
				TranslatableComponent ttc = (TranslatableComponent) cbs.getTitle();
				if("selectWorld.backupQuestion.experimental".equals(ttc.getKey()))
				{
					Thread t = new Thread(() -> {
						try
						{
							Thread.sleep(247);
						} catch (InterruptedException e)
						{
							e.printStackTrace();
						}

						if(th.screen == cbs)
						{
							/**
							 * btn0 = Create Backup and load world
							 * btn1 = Just load world without backup
							 */
							int btnNumber = 1;

							if(FPConfig.CLIENT.futurepackCreateBackupOnSkip.get()) {
								btnNumber = 0;
							}

							Button IknowWhatIamDoing = (Button) cbs.children().get(btnNumber); //lets create ALOT of backups

							TranslatableComponent displayText = new TranslatableComponent("menu.loadingLevel");

							if(FPConfig.CLIENT.futurepackShowCustomLoadingScreen.get()) {
								Minecraft.getInstance().execute(() -> {
									Minecraft.getInstance().setScreen(new GuiFPWorldLoading(displayText));
								});

							}
							else {
								Minecraft.getInstance().execute(() -> {
									Minecraft.getInstance().setScreen(new GenericDirtMessageScreen(displayText));
								});
							}
							try {
								Thread.sleep(100);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}

							th.executeBlocking(IknowWhatIamDoing::onPress);
						}
					});
					t.start();
				}
			}
		}
		else if(gui instanceof InventoryScreen)
		{
			@SuppressWarnings("resource")
			LocalPlayer cp = Minecraft.getInstance().player;
			if(cp.isPassenger() && cp.getVehicle() instanceof EntityAlphaJawaul && ((EntityAlphaJawaul)cp.getVehicle()).isTame() && ((EntityAlphaJawaul)cp.getVehicle()).hasChest())
			{

				FPPacketHandler.CHANNEL_FUTUREPACK.sendToServer(new MessageRequestJawaulInventory());
				event.setCanceled(true);
			}
		}
	}

	@SubscribeEvent
	public void onItemTooltip(ItemTooltipEvent event)
	{
		CustomPlayerData data = null;
		if(event.getPlayer()==null)
			data = CustomPlayerData.createForLocalPlayer();
		else
			data = CustomPlayerData.getDataFromPlayer(event.getPlayer());

		if(data==null || data.hasResearch("energy.infusion"))
		{
			int fuel = TileEntityInfusionGenerator.getItemFuelBase(event.getItemStack());
			int factor = TileEntityInfusionGenerator.getCristalPower(event.getItemStack());
			if(fuel>0)
			{
				event.getToolTip().add(new TranslatableComponent("futurepack.infusion.fuel", fuel));
			}
			if(factor > 0)
			{
				event.getToolTip().add(new TranslatableComponent("futurepack.infusion.fuel.factor", factor));
			}

		}
	}
}
