package futurepack.world.dimensions;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.function.Consumer;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.core.HolderSet;
import net.minecraft.core.Registry;
import net.minecraft.server.level.WorldGenRegion;
import net.minecraft.world.level.LevelHeightAccessor;
import net.minecraft.world.level.NoiseColumn;
import net.minecraft.world.level.StructureFeatureManager;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.BiomeManager;
import net.minecraft.world.level.biome.BiomeSource;
import net.minecraft.world.level.biome.Climate;
import net.minecraft.world.level.biome.Climate.Sampler;
import net.minecraft.world.level.biome.FixedBiomeSource;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkAccess;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.GenerationStep.Carving;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.levelgen.Heightmap.Types;
import net.minecraft.world.level.levelgen.blending.Blender;
import net.minecraft.world.level.levelgen.structure.StructureSet;

public class ChunkGeneratorAsteroidBelt extends ChunkGenerator 
{
	public static final Codec<ChunkGeneratorAsteroidBelt> CODEC = RecordCodecBuilder.create((p_204551_) -> 
	{
		return commonCodec(p_204551_)
				.and(AsteroidBeltSettings.CODEC.fieldOf("settings").forGetter(ChunkGeneratorAsteroidBelt::settings))
				.apply(p_204551_, p_204551_.stable(ChunkGeneratorAsteroidBelt::new));
	});
		
	private AsteroidBeltSettings settings;
	
	
	public ChunkGeneratorAsteroidBelt(Registry<StructureSet> pStructureSets, Optional<HolderSet<StructureSet>> pStructureOverrides, BiomeSource pBiomeSource, BiomeSource pRuntimeBiomeSource, long pRingPlacementSeed, AsteroidBeltSettings settings) 
	{
		super(pStructureSets, pStructureOverrides, pBiomeSource, pRuntimeBiomeSource, pRingPlacementSeed);
		this.settings = settings;
	}
	
	public ChunkGeneratorAsteroidBelt(Registry<StructureSet> reg, AsteroidBeltSettings setting) 
	{
		this(reg, setting.structureOverrides(),
				new FixedBiomeSource(setting.getBiomeSupplier()),
				new FixedBiomeSource(setting.getBiomeSupplier()),
				0L, setting);
	}

	@Override
	protected Codec<ChunkGeneratorAsteroidBelt> codec() 
	{
		return CODEC;
	}

	public AsteroidBeltSettings settings() 
	{
	      return this.settings;
	   }
	
	@Override
	public ChunkGenerator withSeed(long seed) 
	{
		return new ChunkGeneratorAsteroidBelt(structureSets, structureOverrides,
				biomeSource,
				runtimeBiomeSource,
				seed, settings.withSeed(seed));
	}

	@Override
	protected Holder<Biome> adjustBiome(Holder<Biome> p_204385_) 
	{
		return settings.getBiomeSupplier();
	}
	
	@Override
	public void buildSurface(WorldGenRegion pLevel, StructureFeatureManager pStructureFeatureManager, ChunkAccess pChunk) 
	{
//		BlockState bedrock = Blocks.BEDROCK.getDefaultState();
//		BlockPos.Mutable mut = new BlockPos.Mutable();
//		for(int x=0;x<16;x++)
//		{
//			for(int z=0;z<16;z++)
//			{
//				mut.setPos(x, 0, z);
//				chunk.setBlockState(mut, bedrock, false);
//			}
//		}
	}
	
//set block in chunk
	@Override
	public CompletableFuture<ChunkAccess> fillFromNoise(Executor pExecutor, Blender pBlender, StructureFeatureManager pStructureFeatureManager, ChunkAccess chunk) 
	{
		BlockPos.MutableBlockPos mut = new BlockPos.MutableBlockPos();
		Heightmap heightmap = chunk.getOrCreateHeightmapUnprimed(Heightmap.Types.OCEAN_FLOOR_WG);
		Heightmap heightmap1 = chunk.getOrCreateHeightmapUnprimed(Heightmap.Types.WORLD_SURFACE_WG);
	    BlockState stone = Blocks.STONE.defaultBlockState();
		
		for(int x=0;x<16;x++)
		{
			for(int z=0;z<16;z++)
			{
				
				generatePillarAt(chunk.getPos().x*16 +x, chunk.getPos().z*16 +z, mut, p -> {
					chunk.setBlockState(p, stone, false);
					heightmap .update(p.getX() & 15, p.getY(), p.getZ() & 15, stone);
					heightmap1.update(p.getX() & 15, p.getY(), p.getZ() & 15, stone);
				}, chunk.getMinBuildHeight(), chunk.getHeight());
			}
		}
		
		return CompletableFuture.completedFuture(chunk);
	}

	private void generatePillarAt(int x, int z, BlockPos.MutableBlockPos mut, Consumer<BlockPos.MutableBlockPos> callback, int min_y, int total_height)
	{
		settings.getAsteroidLayers().forEach(l -> l.generatePillarAt(x, z, mut, callback, min_y, total_height));
	}
	
	
	
	@Override
	public int getBaseHeight(int x, int z, Types heightmapType, LevelHeightAccessor w) 
	{
		BlockState[] states = new BlockState[w.getHeight()];
		generatePillarAt(x, z, new BlockPos.MutableBlockPos(), p -> {
			states[p.getY()- w.getMinBuildHeight()] = Blocks.STONE.defaultBlockState();
		}, w.getMinBuildHeight(), w.getHeight());
		BlockState air = Blocks.AIR.defaultBlockState();
		for(int i=states.length-1;i>=0;i--)
		{
			if(heightmapType.isOpaque().test(states[i]==null?air:states[i]))
			{
				return i;
			}
		}
		
		return 0;
	}

	//basicly a horizontal block list ?
	@Override
	public NoiseColumn getBaseColumn(int x, int z, LevelHeightAccessor w)
	{
		BlockState[] states = new BlockState[w.getHeight()];
		generatePillarAt(x, z, new BlockPos.MutableBlockPos(), p -> {
			states[p.getY()-w.getMinBuildHeight()] = Blocks.STONE.defaultBlockState();
		}, w.getMinBuildHeight(), w.getHeight());
		BlockState air = Blocks.AIR.defaultBlockState();
		for(int i=0;i<states.length;i++)
		{
			if(states[i]==null)
			{
				states[i] = air;
			}
		}
		return new NoiseColumn(w.getMinBuildHeight(), states);
	}

	@Override
	public Sampler climateSampler() 
	{
		return Climate.empty();
	}

	@Override
	public void applyCarvers(WorldGenRegion pLevel, long pSeed, BiomeManager pBiomeManager, StructureFeatureManager pStructureFeatureManager, ChunkAccess pChunk, Carving pStep) 
	{
		
	}

	@Override
	public void spawnOriginalMobs(WorldGenRegion pLevel) 
	{
	}

	@Override
	public int getGenDepth() 
	{
		return 384;//from FLatLevelSource
	}

	@Override
	public int getSeaLevel() 
	{
		return -63;//from FLatLevelSource
	}

	@Override
	public int getMinY() 
	{
		return -128;//from FLatLevelSource
	}

	@Override
	public void addDebugScreenInfo(List<String> p_208054_, BlockPos p_208055_) 
	{
		
	}

}
