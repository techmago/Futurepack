package futurepack.client.render.entity;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import futurepack.api.Constants;
import futurepack.common.entity.living.EntityDungeonSpider;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;

public class ModelDungeonSpider extends EntityModel<EntityDungeonSpider>
{
	// This layer location should be baked with EntityRendererProvider.Context in
	// the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation(Constants.MOD_ID, "techno_drone"), "main");
	
	private final ModelPart droneGround;
	private final ModelPart nag;
	private final ModelPart leg1;
	private final ModelPart leg2;
	private final ModelPart leg3;
	private final ModelPart leg4;
	
	public ModelDungeonSpider(ModelPart root)
	{
		this.droneGround = root.getChild("droneGround");
		nag = droneGround.getChild("nag");
		leg1 = droneGround.getChild("leg1");
		leg2 = droneGround.getChild("leg2");
		leg3 = droneGround.getChild("leg3");
		leg4 = droneGround.getChild("leg4");
		
	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition droneGround = partdefinition.addOrReplaceChild("droneGround",
				CubeListBuilder.create().texOffs(0, 4).addBox(-2.0F, -3.0F, -2.0F, 4.0F, 2.0F, 4.0F, new CubeDeformation(0.1F)).texOffs(0, 10).addBox(-1.5F, -4.0F, 1.0F, 3.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition nag = droneGround.addOrReplaceChild("nag", CubeListBuilder.create().texOffs(0, 0).addBox(-1.5F, -1.0F, -1.0F, 3.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -3.0F, -2.0F));

		PartDefinition leg1 = droneGround.addOrReplaceChild("leg1", CubeListBuilder.create().texOffs(0, 14).addBox(-0.5F, 0.0F, 0.0F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -3.0F, -2.0F, 0.0F, 0.7854F, 0.0F));

		PartDefinition servo01 = leg1.addOrReplaceChild("servo01",
				CubeListBuilder.create().texOffs(0, 4).addBox(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.1F)).texOffs(0, 6).addBox(0.0F, 1.0F, 0.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.05F)),
				PartPose.offsetAndRotation(2.0F, 0.0F, 0.0F, 0.0F, 0.0F, -0.4363F));

		PartDefinition servo02 = servo01.addOrReplaceChild("servo02", CubeListBuilder.create().texOffs(12, 5).addBox(-1.0F, 0.0F, 0.0F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.0F, 2.0F, 0.0F, 0.0F, 0.0F, 0.3491F));

		PartDefinition leg2 = droneGround.addOrReplaceChild("leg2", CubeListBuilder.create().texOffs(8, 14).addBox(-0.5F, 0.0F, -1.0F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -3.0F, 2.0F, 0.0F, -0.7854F, 0.0F));

		PartDefinition servo2 = leg2.addOrReplaceChild("servo2",
				CubeListBuilder.create().texOffs(0, 4).addBox(0.0F, 0.0F, -1.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.1F)).texOffs(0, 6).addBox(0.0F, 1.0F, -1.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.05F)),
				PartPose.offsetAndRotation(2.0F, 0.0F, 0.0F, 0.0F, 0.0F, -0.4363F));

		PartDefinition servo3 = servo2.addOrReplaceChild("servo3", CubeListBuilder.create().texOffs(12, 5).addBox(-1.0F, 0.0F, -1.0F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.0F, 2.0F, 0.0F, 0.0F, 0.0F, 0.3491F));

		PartDefinition leg3 = droneGround.addOrReplaceChild("leg3", CubeListBuilder.create().texOffs(0, 14).addBox(-0.5F, 0.0F, 0.0F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -3.0F, 2.0F, 0.0F, -2.3562F, 0.0F));

		PartDefinition servo4 = leg3.addOrReplaceChild("servo4", CubeListBuilder.create().texOffs(0, 4).addBox(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.1F)).texOffs(0, 6).addBox(0.0F, 1.0F, 0.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.05F)),
				PartPose.offsetAndRotation(2.0F, 0.0F, 0.0F, 0.0F, 0.0F, -0.4363F));

		PartDefinition servo5 = servo4.addOrReplaceChild("servo5", CubeListBuilder.create().texOffs(12, 5).addBox(-1.0F, 0.0F, 0.0F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.0F, 2.0F, 0.0F, 0.0F, 0.0F, 0.3491F));

		PartDefinition leg4 = droneGround.addOrReplaceChild("leg4", CubeListBuilder.create().texOffs(8, 14).addBox(-0.5F, 0.0F, -1.0F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -3.0F, -2.0F, 0.0F, 2.3562F, 0.0F));

		PartDefinition servo6 = leg4.addOrReplaceChild("servo6",
				CubeListBuilder.create().texOffs(0, 4).addBox(0.0F, 0.0F, -1.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.1F)).texOffs(0, 6).addBox(0.0F, 1.0F, -1.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.05F)),
				PartPose.offsetAndRotation(2.0F, 0.0F, 0.0F, 0.0F, 0.0F, -0.4363F));

		PartDefinition servo7 = servo6.addOrReplaceChild("servo7", CubeListBuilder.create().texOffs(12, 5).addBox(-1.0F, 0.0F, -1.0F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.0F, 2.0F, 0.0F, 0.0F, 0.0F, 0.3491F));

		return LayerDefinition.create(meshdefinition, 16, 16);
	}
	

/*
	public ModelDungeonSpider()
	{
		super(RenderType::entitySolid);

		texWidth = 16;
		texHeight = 16;

		droneGround = new ModelPart(this);
		droneGround.setPos(0.0F, 24.0F, 0.0F);
		droneGround.texOffs(0, 4).addBox(-2.0F, -3.0F, -2.0F, 4.0F, 2.0F, 4.0F, 0.1F, false);
		droneGround.texOffs(0, 10).addBox(-1.5F, -4.0F, 1.0F, 3.0F, 2.0F, 2.0F, 0.0F, false);

		nag = new ModelPart(this);
		nag.setPos(0.0F, -3.0F, -2.0F);
		droneGround.addChild(nag);
		nag.texOffs(0, 0).addBox(-1.5F, -1.0F, -1.0F, 3.0F, 2.0F, 2.0F, 0.0F, false);

		leg1 = new ModelPart(this);
		leg1.setPos(2.0F, -3.0F, -2.0F);
		droneGround.addChild(leg1);
		setRotationAngle(leg1, 0.0F, 0.7854F, 0.0F);
		leg1.texOffs(0, 14).addBox(-0.5F, 0.0F, 0.0F, 3.0F, 1.0F, 1.0F, 0.0F, false);

		servo01 = new ModelPart(this);
		servo01.setPos(2.0F, 0.0F, 0.0F);
		leg1.addChild(servo01);
		setRotationAngle(servo01, 0.0F, 0.0F, -0.4363F);
		servo01.texOffs(0, 4).addBox(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F, 0.1F, false);
		servo01.texOffs(0, 6).addBox(0.0F, 1.0F, 0.0F, 1.0F, 1.0F, 1.0F, 0.05F, false);

		servo02 = new ModelPart(this);
		servo02.setPos(1.0F, 2.0F, 0.0F);
		servo01.addChild(servo02);
		setRotationAngle(servo02, 0.0F, 0.0F, 0.3491F);
		servo02.texOffs(12, 5).addBox(-1.0F, 0.0F, 0.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		leg2 = new ModelPart(this);
		leg2.setPos(2.0F, -3.0F, 2.0F);
		droneGround.addChild(leg2);
		setRotationAngle(leg2, 0.0F, -0.7854F, 0.0F);
		leg2.texOffs(8, 14).addBox(-0.5F, 0.0F, -1.0F, 3.0F, 1.0F, 1.0F, 0.0F, false);

		servo2 = new ModelPart(this);
		servo2.setPos(2.0F, 0.0F, 0.0F);
		leg2.addChild(servo2);
		setRotationAngle(servo2, 0.0F, 0.0F, -0.4363F);
		servo2.texOffs(0, 4).addBox(0.0F, 0.0F, -1.0F, 1.0F, 1.0F, 1.0F, 0.1F, false);
		servo2.texOffs(0, 6).addBox(0.0F, 1.0F, -1.0F, 1.0F, 1.0F, 1.0F, 0.05F, false);

		servo3 = new ModelPart(this);
		servo3.setPos(1.0F, 2.0F, 0.0F);
		servo2.addChild(servo3);
		setRotationAngle(servo3, 0.0F, 0.0F, 0.3491F);
		servo3.texOffs(12, 5).addBox(-1.0F, 0.0F, -1.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		leg3 = new ModelPart(this);
		leg3.setPos(-2.0F, -3.0F, 2.0F);
		droneGround.addChild(leg3);
		setRotationAngle(leg3, 0.0F, -2.3562F, 0.0F);
		leg3.texOffs(0, 14).addBox(-0.5F, 0.0F, 0.0F, 3.0F, 1.0F, 1.0F, 0.0F, false);

		servo4 = new ModelPart(this);
		servo4.setPos(2.0F, 0.0F, 0.0F);
		leg3.addChild(servo4);
		setRotationAngle(servo4, 0.0F, 0.0F, -0.4363F);
		servo4.texOffs(0, 4).addBox(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F, 0.1F, false);
		servo4.texOffs(0, 6).addBox(0.0F, 1.0F, 0.0F, 1.0F, 1.0F, 1.0F, 0.05F, false);

		servo5 = new ModelPart(this);
		servo5.setPos(1.0F, 2.0F, 0.0F);
		servo4.addChild(servo5);
		setRotationAngle(servo5, 0.0F, 0.0F, 0.3491F);
		servo5.texOffs(12, 5).addBox(-1.0F, 0.0F, 0.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		leg4 = new ModelPart(this);
		leg4.setPos(-2.0F, -3.0F, -2.0F);
		droneGround.addChild(leg4);
		setRotationAngle(leg4, 0.0F, 2.3562F, 0.0F);
		leg4.texOffs(8, 14).addBox(-0.5F, 0.0F, -1.0F, 3.0F, 1.0F, 1.0F, 0.0F, false);

		servo6 = new ModelPart(this);
		servo6.setPos(2.0F, 0.0F, 0.0F);
		leg4.addChild(servo6);
		setRotationAngle(servo6, 0.0F, 0.0F, -0.4363F);
		servo6.texOffs(0, 4).addBox(0.0F, 0.0F, -1.0F, 1.0F, 1.0F, 1.0F, 0.1F, false);
		servo6.texOffs(0, 6).addBox(0.0F, 1.0F, -1.0F, 1.0F, 1.0F, 1.0F, 0.05F, false);

		servo7 = new ModelPart(this);
		servo7.setPos(1.0F, 2.0F, 0.0F);
		servo6.addChild(servo7);
		setRotationAngle(servo7, 0.0F, 0.0F, 0.3491F);
		servo7.texOffs(12, 5).addBox(-1.0F, 0.0F, -1.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);
	}*/

	@Override
	public void setupAnim(EntityDungeonSpider entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		setRotationAngle(leg1, 0.0F, 0.7854F, 0.0F);
		setRotationAngle(leg2, 0.0F, -0.7854F, 0.0F);
		setRotationAngle(leg3, 0.0F, -2.3562F, 0.0F);
		setRotationAngle(leg4, 0.0F, 2.3562F, 0.0F);

		this.nag.yRot = netHeadYaw * ((float) Math.PI / 180F);
		this.nag.xRot = headPitch * ((float) Math.PI / 180F);

		float f3 = -(Mth.cos(limbSwing * 0.6662F * 2.0F + 0.0F) * 0.4F) * limbSwingAmount;
		float f4 = -(Mth.cos(limbSwing * 0.6662F * 2.0F + (float) Math.PI) * 0.4F) * limbSwingAmount;
		float f5 = -(Mth.cos(limbSwing * 0.6662F * 2.0F + ((float) Math.PI / 2F)) * 0.4F) * limbSwingAmount;
		float f6 = -(Mth.cos(limbSwing * 0.6662F * 2.0F + ((float) Math.PI * 1.5F)) * 0.4F) * limbSwingAmount;
		float f7 = Math.abs(Mth.sin(2 * limbSwing * 0.6662F + 0.0F) * 0.4F) * limbSwingAmount;
		float f8 = Math.abs(Mth.sin(2 * limbSwing * 0.6662F + (float) Math.PI) * 0.4F) * limbSwingAmount;
		float f9 = Math.abs(Mth.sin(2 * limbSwing * 0.6662F + ((float) Math.PI / 2F)) * 0.4F) * limbSwingAmount;
		this.leg1.yRot += f3;
		this.leg2.yRot += f4;
		this.leg3.yRot += f5;
		this.leg4.yRot += f6;
//		this.leg5.rotateAngleY += f5;
//		this.leg6.rotateAngleY += -f5;
//		this.leg3.rotateAngleY += f6;
//		this.leg4.rotateAngleY += -f6;
		this.leg1.zRot += -f7;
		this.leg2.zRot += -f8;
		this.leg3.zRot += f9;
		this.leg4.zRot += f9;
//		this.leg5.rotateAngleZ += f9;
//		this.leg6.rotateAngleZ += -f9;
//		this.leg3.rotateAngleZ += f10;
//		this.leg4.rotateAngleZ += -f10;
	}

	@Override
	public void renderToBuffer(PoseStack matrixStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha)
	{
		droneGround.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelPart modelRenderer, float x, float y, float z)
	{
		modelRenderer.xRot = x;
		modelRenderer.yRot = y;
		modelRenderer.zRot = z;
	}

}