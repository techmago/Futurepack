package futurepack.common.entity.ai;

import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.google.common.base.Predicates;

import futurepack.common.entity.living.EntityDungeonSpider;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.VoxelShape;

public class AIRandomWalkAtCeiling extends Goal 
{
	private int collidedTicks = 0;
	protected EntityDungeonSpider spider;
	
	public AIRandomWalkAtCeiling(EntityDungeonSpider spider) 
	{
		this.spider = spider;
		setFlags(EnumSet.of(Goal.Flag.JUMP, Goal.Flag.MOVE, Goal.Flag.LOOK));
	}

	@Override
	public boolean canUse() 
	{
		return spider.isOnGround() && (spider.getTarget() == null || spider.getCommandSenderWorld().random.nextInt(10) >= 9 );
	}
	
	@Override
	public boolean canContinueToUse() 
	{
		return spider.isAttachedToWallOrCeiling() && spider.getTarget() == null;
	}
	
	@Override
	public void start() 
	{
		collidedTicks = 0;
		if(!spider.isAttachedToWallOrCeiling())
		{
			spider.setAttachedDirection(Direction.DOWN);
			spider.setFacingDirection(spider.getDirection());
		}
		tick();
	}
	
	@Override
	public void tick() 
	{
		super.tick();
		
		Level w = spider.getCommandSenderWorld();
		
		AABB bb = spider.getBoundingBox();
		Direction attached = spider.getAttachedDirection();
		
		if(attached == null)
			return;
		
		bb = bb.move(BlockPos.ZERO.relative(attached));
		
		Stream<AABB> l = StreamSupport.stream(spider.level.getBlockCollisions(spider, bb).spliterator(), false).map(VoxelShape::toAabbs).flatMap(List::stream).filter(Predicates.notNull());
		Optional<AABB> hasGround = l.filter(bb::intersects).findAny();
		if(!hasGround.isPresent())
		{
			spider.detachFromWall();
			return;
		}
		
		int r = w.random.nextInt(500);
		if(r == 0)
		{
			turnLeft();
		}
		else if(r == 1)
		{
			turnRight();
		}
		
		if(spider.horizontalCollision || spider.verticalCollision)
		{
			collidedTicks++;
			if(collidedTicks > 10)
			{
				if(attached != Direction.UP)
				{
					BlockPos pos = spider.blockPosition().relative(spider.getFacingDirection());
					if(!w.isEmptyBlock(pos))
					{
						Direction facing = spider.getFacingDirection();
						
						spider.setAttachedDirection(facing);
						spider.setFacingDirection(attached.getOpposite());
					}
				}
				else
				{
					collidedTicks = 0;
					
					r = w.random.nextInt(10);
					if(r == 0)
					{
						turnLeft();
					}
					else if(r == 1)
					{
						turnRight();
					}	
				}
			}
		}
		else
		{
			collidedTicks = 0;
		}
		
		moveForward();
	}
	
	@Override
	public void stop() 
	{
		super.stop();
		spider.detachFromWall();
		spider.fallDistance = 0F;
	}
	
	private void turnLeft()
	{
		Direction attached = spider.getAttachedDirection();
		Direction facing = spider.getFacingDirection();
		
		facing = turnRightInternal(facing, attached.getOpposite());
		
		spider.setFacingDirection(facing);
	}
	
	private void turnRight()
	{
		Direction attached = spider.getAttachedDirection();
		Direction facing = spider.getFacingDirection();
		
		facing = turnRightInternal(facing, attached);
		
		spider.setFacingDirection(facing);
	}
	
	private static Direction turnRightInternal(Direction facing, Direction attached)
	{
		if(attached==Direction.DOWN)
		{
			return facing.getClockWise();
		}
		else if(attached == Direction.UP)
		{
			return facing.getCounterClockWise();
		}
		else if(attached == Direction.NORTH)
		{
			switch (facing) 
			{
			case UP:
				return Direction.EAST;
			case EAST:
				return Direction.DOWN;
			case DOWN:
				return Direction.WEST;
			case WEST:
				return Direction.UP;
			default:
		         throw new IllegalStateException("Unable to get Z-rotated facing of " + facing);
			}
		}
		else if(attached == Direction.SOUTH)
		{
			switch (facing) 
			{
			case UP:
				return Direction.WEST;
			case EAST:
				return Direction.UP;
			case DOWN:
				return Direction.EAST;
			case WEST:
				return Direction.DOWN;
			default:
		         throw new IllegalStateException("Unable to get CCN Z-rotated facing of " + facing);
			}
		}
		else if(attached == Direction.WEST)
		{
			switch (facing) 
			{
			case UP:
				return Direction.NORTH;
			case NORTH:
				return Direction.DOWN;
			case DOWN:
				return Direction.SOUTH;
			case SOUTH:
				return Direction.UP;
			default:
		         throw new IllegalStateException("Unable to get X-rotated facing of " + facing);
			}
		}
		else if(attached == Direction.EAST)
		{
			switch (facing) 
			{
			case UP:
				return Direction.SOUTH;
			case NORTH:
				return Direction.UP;
			case DOWN:
				return Direction.NORTH;
			case SOUTH:
				return Direction.DOWN;
			default:
		         throw new IllegalStateException("Unable to get CCN X-rotated facing of " + facing);
			}
		}
		
		return facing;
	}
	
	private void moveForward()
	{
		float speed = (float) spider.getAttribute(Attributes.MOVEMENT_SPEED).getValue() * 0.15F;
		Vec3 mov = Vec3.atLowerCornerOf(spider.getFacingDirection().getNormal()).add(Vec3.atLowerCornerOf(spider.getAttachedDirection().getNormal())).scale(speed);
		spider.setDeltaMovement(mov);
		
	}
}
