package futurepack.common.dim.structures.enemys;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import futurepack.common.FPLog;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.TagParser;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.SpawnEggItem;
import net.minecraft.world.level.Level;
import net.minecraftforge.registries.ForgeRegistries;

public class EnemyEntry
{
	private final ResourceLocation id;
	private final CompoundTag entityData;
	private final int count;
	
	public EnemyEntry(String id, CompoundTag entityData, int count)
	{
		super();
		this.id = new ResourceLocation(id);
		this.entityData = entityData;
		this.count = count;
	}
	
	public EnemyEntry(JsonObject obj) throws CommandSyntaxException
	{
		this(obj.get("id").getAsString(), obj.has("data") ? getData(obj.get("data")) : new CompoundTag(), obj.has("count") ? obj.get("count").getAsInt() : 1);
	}
	
	public List<Entity> getEntitys(Level w)
	{
		List<Entity> list = new ArrayList<Entity>(count);
		for(int i=0;i<count;i++)
		{
			list.add(create(w));
		}
		return list;
	}
	
	public int getEnemyCount()
	{
		return count;
	}
	
	private Entity create(Level w)
	{
		try
		{
			Entity e =  ForgeRegistries.ENTITIES.getValue(id).create(w);
			if(e==null)
			{
				FPLog.logger.error("Cant find Entity \"%s\" -> use fallback of pig to prevent NullPointer.", id);
				return EntityType.PIG.create(w);
			}
			e.load(entityData);
			return e;
		}
		catch(Exception ee)
		{
			FPLog.logger.fatal("Exception while loading Entity of %s entityData id [%s]", id, entityData.toString());
			throw ee;
		}
	}
	
	private static CompoundTag getData(JsonElement elm) throws CommandSyntaxException
	{
		String s = "";
		try
		{
			s = elm.getAsString();
		}
		catch(UnsupportedOperationException e)
		{
			s = elm.toString();
		}
		return TagParser.parseTag(s);
	}

	public int getDangerColor(float saturation)
	{
		EntityType type = ForgeRegistries.ENTITIES.getValue(id);
		if(type!=null)
		{
			SpawnEggItem egg = SpawnEggItem.byId(type);
			if(egg!=null)
			{
				int rgb = egg.getColor(0) & 0xFFFFFF;
				int end = 96+ (int) (159*saturation);
				end <<= 24;
				return end | rgb;
			}
		}
		return 0xFFFFFFFF;
	}
}
