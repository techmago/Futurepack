package futurepack.common.item;

import java.util.List;
import java.util.function.Function;

import futurepack.api.interfaces.IGranadleLauncherAmmo;
import futurepack.common.entity.throwable.EntityGrenadeBase;
import net.minecraft.core.BlockSource;
import net.minecraft.core.Direction;
import net.minecraft.core.Position;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public class ItemGreandeBase extends ItemDispensable implements IGranadleLauncherAmmo
{
	private final Function<Level, EntityGrenadeBase> constructor;
	
	public ItemGreandeBase(Properties properties, Function<Level, EntityGrenadeBase> constructor) 
	{
		super(properties);
		this.constructor = constructor;
	}

	@Override
	public ItemStack dispense(BlockSource src, ItemStack it, Position pos, Direction enumfacing)
	{
		ItemStack item = it.split(1);
		
		int size = 1;
		if(item.hasTag() && item.getTag().contains("size"))
		{
			size = item.getTag().getInt("size");
		} 
		
		EntityGrenadeBase grenade = constructor.apply(src.getLevel());
		grenade.setSize(size);

		grenade.setPos(src.x()+enumfacing.getStepX(), src.y()+enumfacing.getStepY(), src.z()+enumfacing.getStepZ());				
		
		double x, y, z;
		x = enumfacing.getStepX();
		y = enumfacing.getStepY();
		z = enumfacing.getStepZ();
		x +=( src.getLevel().random.nextFloat() - src.getLevel().random.nextFloat() )* 0.1;
		y +=( src.getLevel().random.nextFloat() - src.getLevel().random.nextFloat() )* 0.1;
		z +=( src.getLevel().random.nextFloat() - src.getLevel().random.nextFloat() )* 0.1;
		grenade.setDeltaMovement(x, y, z);
		
		src.getLevel().addFreshEntity(grenade);

		return it;
	}

	@Override
	public boolean isGranade(ItemStack it) 
	{
		return true;
	}
	
	@Override
	public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) 
	{
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
		tooltip.add(new TranslatableComponent("tooltip.futurepack.item.grenade.power", stack.getOrCreateTag().getInt("size")));
		
	}

	@Override
	public Entity createGrenade(Level w, ItemStack it, Player pl, float strength)
	{
		if(!it.isEmpty())
		{
			EntityGrenadeBase base = getGrande(w, it, pl);
			base.shootFromRotation(pl, pl.getXRot(), pl.getYRot(), 0.0F, 4.5F *strength, 1.0F);
			return base;
		}
		return null;
	}

	
	protected EntityGrenadeBase getGrande(Level w, ItemStack it, Player pl)
	{
		EntityGrenadeBase base = constructor.apply(w);
		base.setThrower(pl);
		
		int size = 1;
		if(it.hasTag() && it.getTag().contains("size"))
		{
			size = it.getTag().getInt("size");
		}
		base.setSize(size);
		return base;
	}
}
