package futurepack.common.gui.inventory;

import java.util.Arrays;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.common.block.modification.TileEntityModificationBase;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperGui;
import futurepack.depend.api.helper.HelperRendering;
import net.minecraft.client.resources.language.I18n;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.inventory.AbstractContainerMenu;

public abstract class GuiModificationBase<T extends TileEntityModificationBase> extends ActuallyUseableContainerScreen
{
	protected ResourceLocation res;
	int nx=7, ny=7;
	
	public GuiModificationBase(AbstractContainerMenu cont, String gui, Inventory inv)
	{
		super(cont, inv, "gui." + gui.replace(".png", ""));
		res = new ResourceLocation(Constants.MOD_ID, "textures/gui/" + gui);
		
	}

	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	public abstract T tile();
	
	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		RenderSystem.setShaderTexture(0, res);
		HelperRendering.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.blit(matrixStack, leftPos, topPos, 0, 0, this.imageWidth, this.imageHeight);
		
		HelperGui.renderNeon(matrixStack, leftPos+nx, topPos+ny, tile().energy, mouseX, mouseY);
		HelperComponent.renderSymbol(matrixStack, leftPos-20, topPos, getBlitOffset(), 29);
		if(!tile().getInventory().canWork())
		{	
			HelperComponent.renderSymbol(matrixStack, leftPos-20, topPos+18, getBlitOffset(), 23);
		}
		RenderSystem.setShaderTexture(0, res);
	}
	
	@Override
	protected void renderLabels(PoseStack matrixStack, int mouseX, int mouseY)
	{
		//Removed to remove default text (inventory and gui name) super.drawGuiContainerForegroundLayer(matrixStack, mouseX, mouseY);
		
		if(HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos , -20, 0, -20+18, 0+18))
		{
			HelperGui.drawHoveringTextFixedString(matrixStack, Arrays.asList(I18n.get("gui.futurepack.machine.scrench")), mouseX-leftPos +12, mouseY-topPos+4, -1, font);
		}
		
		if(!tile().getInventory().canWork())
		{
			if(HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos , -20, 18, -20+18, 18+18))
			{
				HelperGui.drawHoveringTextFixedString(matrixStack, Arrays.asList(I18n.get("gui.futurepack.machine.burned")), mouseX-leftPos +12, mouseY-topPos+4, -1, font);
			}
		}
		
		HelperGui.renderNeonTooltip(matrixStack, leftPos, topPos, nx, ny, tile().energy, mouseX, mouseY);
	}

}
