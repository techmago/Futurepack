package futurepack.client.render;

import com.mojang.blaze3d.vertex.BufferBuilder;
import com.mojang.math.Matrix4f;

import net.minecraft.client.multiplayer.ClientLevel;

//@ TODO: OnlyIn(Dist.CLIENT)
public class RenderSkyAsteroidBelt extends RenderSkyBase
{
	public RenderSkyAsteroidBelt()
	{
		super();
	}
	
	@Override
	protected void renderMoon(float size, int phase, BufferBuilder bufferbuilder, Matrix4f matrix4f1) 
	{
		//no moon in asteroid belt
	}
	
	@Override
	protected float getStarBrightness(float partialTicks, ClientLevel world, float skyBrightness) 
	{
		return 1F;
	}
}
