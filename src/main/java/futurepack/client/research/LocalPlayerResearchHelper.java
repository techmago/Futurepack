package futurepack.client.research;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

import com.google.common.io.Files;

import futurepack.api.event.ResearchFinishEvent;
import futurepack.common.FPLog;
import futurepack.common.research.CustomPlayerData;
import futurepack.common.research.Research;
import futurepack.common.research.ResearchManager;
import futurepack.common.sync.NetworkHandler;
import net.minecraft.client.Minecraft;
import net.minecraftforge.common.MinecraftForge;

public class LocalPlayerResearchHelper
{
	public static ArrayList<String> researches = null;//new ArrayList();
	private static LinkedList<WeakReference<Runnable>> able = new LinkedList<WeakReference<Runnable>>();
	
	private static ArrayList<String> notReaded;
	public static ArrayList<String> researching = new ArrayList<String>();
	
	static
	{
		notReaded = new ArrayList<String>();
		loadNotReadResearched();
	}
	
	public static void setupData(int[] local)
	{
		FPLog.logger.info("Get Research Data from Server");
		
		ArrayList<String> old = researches;		
		researches = new ArrayList<String>();
			
		for(int i : local)
		{
			String res = ResearchManager.getById(i).getName();
					
			if(old==null || !old.contains(res))
			{
				Research r = ResearchManager.getById(i);
				if(old!=null)
				{	
					MinecraftForge.EVENT_BUS.post(new ResearchFinishEvent.Client(Minecraft.getInstance().player, r));
					notReaded.add(res);
					Collections.sort(notReaded);
					saveNotReadResearches();
				}			
			}
			researches.add(res);
		}
		
		
		while(!able.isEmpty())
		{
			WeakReference<Runnable> run = able.removeFirst();
			if(run!=null && run.get()!=null)
			{
				run.get().run();
			}
		}			
	}
	
	public static void setupResearching(Integer[] local)
	{
		researching.clear();
		for(int i : local)
		{
			String res = ResearchManager.getById(i).getName();
			
			if(!researching.contains(res))
			{
				researching.add(res);
			}		
		}
	}
		
	public static void requestServerData(Runnable eventhandler)
	{
		able.addLast(new WeakReference<Runnable>(eventhandler));
		FPLog.logger.info("Send Research Request to Server");
		NetworkHandler.requestResearchDataFromServer();
	}
	
	public static CustomPlayerData getLocalPlayerData()
	{
		return CustomPlayerData.createForLocalPlayer();
	}

	public static void onResearchOpened(Research r) 
	{
		getNotReaded().remove(r.getName());
		saveNotReadResearches();
	}

	public static ArrayList<String> getNotReaded() 
	{
		return notReaded;
	}
	
	@SuppressWarnings("resource")
	public static File getLocalTempFile()
	{
		return new File(Minecraft.getInstance().gameDirectory, "futurepack_not_read.temp");
	}
	
	public static void saveNotReadResearches()
	{
		try
		{
			BufferedWriter write  = Files.newWriter(getLocalTempFile(), StandardCharsets.UTF_8);
			for(String s : notReaded)
			{
				write.append(s);
				write.newLine();
			}
			write.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static void loadNotReadResearched()
	{
		notReaded.clear();
		try
		{
			if(getLocalTempFile().exists())
			{
				notReaded.addAll(Files.readLines(getLocalTempFile(), StandardCharsets.UTF_8));
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
}
