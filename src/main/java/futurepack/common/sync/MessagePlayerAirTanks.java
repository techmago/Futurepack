package futurepack.common.sync;

import java.util.function.Supplier;

import futurepack.client.ClientEvents;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.network.NetworkEvent;

/** 
 * Send the modified Air to the Player (for Athmosphereless planets)
 */
public class MessagePlayerAirTanks
{
	private float playerAirTanks;
	
	
	public MessagePlayerAirTanks(float playerAir)
	{
		this.playerAirTanks = playerAir;
	}
	
	public static MessagePlayerAirTanks decode(FriendlyByteBuf buf) 
	{
		return new MessagePlayerAirTanks(buf.readVarInt() / 1024F);
	}
	
	public static void encode(MessagePlayerAirTanks msg, FriendlyByteBuf buf) 
	{
		buf.writeVarInt((int) (msg.playerAirTanks * 1024));
	}
	
	public static void consume(MessagePlayerAirTanks message, Supplier<NetworkEvent.Context> ctx) 
	{
		DistExecutor.runWhenOn(Dist.CLIENT, () ->  
											() -> 
		{
			ClientEvents.setAirTanks(message.playerAirTanks);
			ctx.get().setPacketHandled(true);
		}); 
	}
}
