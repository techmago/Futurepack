package futurepack.common.recipes.crafting;

import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.registries.IForgeRegistry;

public class FPSerializers
{
	public static final RecipeSerializer<?> RESEARCH_SHAPED = new ShapedRecipeWithResearch.Factory().setRegistryName(ShapedRecipeWithResearch.Factory.NAME);
	public static final RecipeSerializer<?> RESEARCH_SHAPELESS = new ShapelessRecipeWithResearch.Factory().setRegistryName(ShapelessRecipeWithResearch.Factory.NAME);
	public static final RecipeSerializer<?> MASCHIN_SHAPED = new RecipeMaschin.Factory().setRegistryName(RecipeMaschin.Factory.NAME);
	public static final RecipeSerializer<?> OWNER_SHAPED = new RecipeOwnerBased.Factory().setRegistryName(RecipeOwnerBased.Factory.NAME);
	public static final RecipeSerializer<?> DYING = new RecipeDyeing.Factory().setRegistryName(RecipeDyeing.Factory.NAME);
	public static final RecipeSerializer<?> AIRBRUSH = new AirbrushRecipe.Factory().setRegistryName(AirbrushRecipe.Factory.NAME);


	public static void init(Register<RecipeSerializer<?>> event)
	{
		//maguic
		IForgeRegistry<RecipeSerializer<?>> reg = event.getRegistry();
		reg.registerAll(RESEARCH_SHAPED, RESEARCH_SHAPELESS, MASCHIN_SHAPED, OWNER_SHAPED, DYING, AIRBRUSH);
	}
}
