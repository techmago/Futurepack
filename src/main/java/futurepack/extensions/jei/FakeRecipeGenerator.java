package futurepack.extensions.jei;

import java.util.ArrayList;
import java.util.Collections;

import futurepack.common.item.ComputerItems;
import futurepack.common.item.ResourceItems;
import futurepack.common.item.misc.MiscItems;
import futurepack.extensions.jei.partpress.PartPressJeiFakeRecipe;
import futurepack.extensions.jei.recycler.RecyclerJeiFakeRecipe;
import mezz.jei.api.registration.IRecipeRegistration;
import net.minecraft.client.resources.language.I18n;
import net.minecraft.world.item.ItemStack;

public class FakeRecipeGenerator 
{
	public static void registerRecyclerAnalyserRecipes(IRecipeRegistration registry)
	{
		String tip = I18n.get("jei.recycler.analyser.makesupport");
		
		ArrayList<ItemStack> t = new ArrayList<ItemStack>();
		
		t.add(new ItemStack(ComputerItems.ai_chip));
		t.add(new ItemStack(ComputerItems.damage_control_chip));
		t.add(new ItemStack(ComputerItems.industrie_chip));
		t.add(new ItemStack(ComputerItems.logic_chip));
		t.add(new ItemStack(ComputerItems.navigation_chip));
		t.add(new ItemStack(ComputerItems.network_chip));
		t.add(new ItemStack(ComputerItems.redstone_chip));
		t.add(new ItemStack(ComputerItems.support_chip));
		t.add(new ItemStack(ComputerItems.tactic_chip));
		t.add(new ItemStack(ComputerItems.transport_chip));
		t.add(new ItemStack(ComputerItems.ultimate_chip));
		t.add(new ItemStack(ComputerItems.toasted_chip));
		
		registry.addRecipes(FuturepackPlugin.RECYCLER, Collections.singletonList(new RecyclerJeiFakeRecipe(new ItemStack(MiscItems.analyzer), t, new ArrayList<ItemStack>(), tip)));
		
		t = new ArrayList<ItemStack>();
		
		t.add(new ItemStack(ComputerItems.a_ram));	
		t.add(new ItemStack(ComputerItems.dungeon_ram));	
		t.add(new ItemStack(ComputerItems.entronium_ram));	
		t.add(new ItemStack(ComputerItems.master_ram));	
		t.add(new ItemStack(ComputerItems.non_ram));	
		t.add(new ItemStack(ComputerItems.p_ram));	
		t.add(new ItemStack(ComputerItems.standart_ram));	
		t.add(new ItemStack(ComputerItems.tct_ram));
		t.add(new ItemStack(ComputerItems.torus_ram));	
		t.add(new ItemStack(ComputerItems.zombie_ram));	
		t.add(new ItemStack(ComputerItems.toasted_ram));	
		
		registry.addRecipes(FuturepackPlugin.RECYCLER, Collections.singletonList(new RecyclerJeiFakeRecipe(new ItemStack(MiscItems.analyzer), t, new ArrayList<ItemStack>(), tip)));
		
		t = new ArrayList<ItemStack>();
		
		t.add(new ItemStack(ComputerItems.a1_core));
		t.add(new ItemStack(ComputerItems.dungeon_core));
		t.add(new ItemStack(ComputerItems.entronium_core));
		t.add(new ItemStack(ComputerItems.master_core));
		t.add(new ItemStack(ComputerItems.non_core));
		t.add(new ItemStack(ComputerItems.p2_core));
		t.add(new ItemStack(ComputerItems.standart_core));
		t.add(new ItemStack(ComputerItems.tct_core));
		t.add(new ItemStack(ComputerItems.torus_core));
		t.add(new ItemStack(ComputerItems.zombie_core));
		t.add(new ItemStack(ComputerItems.toasted_core));
	
		registry.addRecipes(FuturepackPlugin.RECYCLER, Collections.singletonList(new RecyclerJeiFakeRecipe(new ItemStack(MiscItems.analyzer), t, new ArrayList<ItemStack>(), tip)));			
	}
	
	public static void registerPartPressRecipes(IRecipeRegistration registry)
	{
		registry.addRecipes(FuturepackPlugin.PARTPRESS, Collections.singletonList(new PartPressJeiFakeRecipe("forge:gems/diamond", new ItemStack(ResourceItems.parts_diamond,4))));
		registry.addRecipes(FuturepackPlugin.PARTPRESS, Collections.singletonList(new PartPressJeiFakeRecipe("forge:ingots/iron", new ItemStack(ResourceItems.parts_iron,4))));
		registry.addRecipes(FuturepackPlugin.PARTPRESS, Collections.singletonList(new PartPressJeiFakeRecipe("forge:ingots/neon", new ItemStack(ResourceItems.parts_neon,4))));
		registry.addRecipes(FuturepackPlugin.PARTPRESS, Collections.singletonList(new PartPressJeiFakeRecipe("forge:ingots/copper", new ItemStack(ResourceItems.parts_copper,4))));
		registry.addRecipes(FuturepackPlugin.PARTPRESS, Collections.singletonList(new PartPressJeiFakeRecipe("forge:gems/quartz", new ItemStack(ResourceItems.parts_quartz,4))));
		registry.addRecipes(FuturepackPlugin.PARTPRESS, Collections.singletonList(new PartPressJeiFakeRecipe("forge:ingots/gold", new ItemStack(ResourceItems.parts_gold,4))));
	}
}
