package futurepack.common.entity.ai.hivemind;

import java.util.List;

import futurepack.common.entity.living.EntityDungeonSpider;
import futurepack.depend.api.helper.HelperSerialisation;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.AABB;

public class PickUpItemsTask extends HVTask 
{
	public BlockPos itemPos;
	
	
	public PickUpItemsTask(BlockPos itemPos) 
	{
		this.itemPos = itemPos;
	}
	
	public PickUpItemsTask(CompoundTag nbt)
	{
		super(nbt);
	}

	@Override
	public CompoundTag serializeNBT() 
	{
		CompoundTag nbt = new CompoundTag();
		HelperSerialisation.putBlockPos(nbt, "itemPos", itemPos);
		return nbt;
	}

	@Override
	public void deserializeNBT(CompoundTag nbt) 
	{
		itemPos = HelperSerialisation.getBlockPos(nbt, "itemPos");
	}

	@Override
	public BlockPos getSpiderPosition() 
	{
		return itemPos;
	}
	
	@Override
	public boolean canExecute(EntityDungeonSpider spider, AssignedTask task) 
	{
		return spider.getMainHandItem().isEmpty();
	}
	
	@Override
	public boolean execute(EntityDungeonSpider spider) 
	{
		AABB bb = new AABB(itemPos).inflate(1, 1, 1);
		
		List<ItemEntity> items = spider.getCommandSenderWorld().getEntitiesOfClass(ItemEntity.class, bb, i -> i.isAlive());
		
		int amount = items.size();
		if(amount -1 <= 0)
		{
			isDone = true;
		}
		if(amount > 0)
		{
			spider.setItemInHand(InteractionHand.MAIN_HAND, items.get(0).getItem());
			items.get(0).discard();
		}
		
		return !spider.getMainHandItem().isEmpty();
	}

	@Override
	public boolean isFinished(Level w) 
	{
		AABB bb = new AABB(itemPos).inflate(1, 1, 1);
		List<ItemEntity> items = w.getEntitiesOfClass(ItemEntity.class, bb, i -> i.isAlive());
		return items.isEmpty();
	}
}
