package futurepack.common.sync;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public interface ISyncable 
{
	public void writeAdditional(DataOutputStream buffer) throws IOException;
	
	public void readAdditional(DataInputStream buffer) throws IOException;
}
