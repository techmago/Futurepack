package futurepack.api.interfaces;

import java.util.List;

import futurepack.api.ItemPredicateBase;
import net.minecraft.world.item.ItemStack;

public interface IRecyclerRecipe 
{
	public ItemStack[] getMaxOutput();
	
	public ItemPredicateBase getInput();

	//jei helper
	public List<ItemStack> getToolItemStacks();

	public float[] getChances();

	public String getJeiInfoText();

}
