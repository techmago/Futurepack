package futurepack.common.item.tools;

import futurepack.api.Constants;
import futurepack.common.FuturepackMain;
import futurepack.common.item.FPArmorMaterials;
import futurepack.common.item.FPToolMaterials;
import futurepack.common.item.ItemSpawnerChest;
import futurepack.common.item.tools.compositearmor.ItemCompositeArmor;
import futurepack.common.item.tools.compositearmor.ItemDungeonArmor;
import futurepack.common.item.tools.compositearmor.ItemModulBattery;
import futurepack.common.item.tools.compositearmor.ItemModulBubbleJet;
import futurepack.common.item.tools.compositearmor.ItemModulDynamo;
import futurepack.common.item.tools.compositearmor.ItemModulFireShield;
import futurepack.common.item.tools.compositearmor.ItemModulGravity;
import futurepack.common.item.tools.compositearmor.ItemModulHealthboost;
import futurepack.common.item.tools.compositearmor.ItemModulMagnet;
import futurepack.common.item.tools.compositearmor.ItemModulOxygenMask;
import futurepack.common.item.tools.compositearmor.ItemModulOxygenTank;
import futurepack.common.item.tools.compositearmor.ItemModulPanzer;
import futurepack.common.item.tools.compositearmor.ItemModulParaglider;
import futurepack.common.item.tools.compositearmor.ItemModulReanimation;
import futurepack.common.item.tools.compositearmor.ItemModulShield;
import futurepack.common.item.tools.compositearmor.ItemModulSolarHelmet;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.ArmorMaterials;
import net.minecraft.world.item.HoeItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.ShovelItem;
import net.minecraft.world.item.SwordItem;
import net.minecraft.world.item.Tiers;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.IForgeRegistry;


public class ToolItems 
{
	//TOOLS, WEAPONS & ARMORY
//	public static final Supplier<Item.Properties> defaultP = () -> new Item.Properties().group(FuturepackMain.tab_tools);
	
	public static final Item airBrush = HelperItems.setRegistryName(new ItemAirBrush(new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "airbrush");
	public static final Item grappling_hook = HelperItems.setRegistryName(new ItemGrablingHook(new Item.Properties().tab(FuturepackMain.tab_tools).stacksTo(1)), Constants.MOD_ID, "grappling_hook");
	public static final Item plasmaschneider = HelperItems.setRegistryName(new ItemPlasmaSchneider(new Item.Properties().tab(FuturepackMain.tab_tools), Tiers.DIAMOND), Constants.MOD_ID, "plasma_cutter");
	public static final Item hologramControler = HelperItems.setRegistryName(new ItemHologramControler(new Item.Properties().tab(FuturepackMain.tab_tools).stacksTo(1)), Constants.MOD_ID, "hologram_controler");
	public static final Item logisticEditor = HelperItems.setRegistryName(new ItemLogisticEditor(new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "logistic_editor");
	public static final Item escanner = HelperItems.setRegistryName(new ItemEScanner(new Item.Properties().tab(FuturepackMain.tab_tools).setNoRepair().stacksTo(1)), Constants.MOD_ID, "escanner");
	
	public static final Item composite_pickaxe = HelperItems.setRegistryName(new ItemFpPickaxe(FPToolMaterials.COMPOSITE, 1, -2.8F, new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "composite_pickaxe");
	public static final Item composite_axe = HelperItems.setRegistryName(new ItemFpAxe(FPToolMaterials.COMPOSITE, 6.0F, -3.1F, new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "composite_axe");
	public static final Item composite_hoe = HelperItems.setRegistryName(new HoeItem(FPToolMaterials.COMPOSITE, -1, 0F, new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "composite_hoe");
	public static final Item composite_spade = HelperItems.setRegistryName(new ShovelItem(FPToolMaterials.COMPOSITE, 1.5F, -3F, new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "composite_spade");
	
	public static final Item magnet_helmet = HelperItems.setRegistryName((new ItemMagnetArmor(ArmorMaterials.IRON, EquipmentSlot.HEAD, new Item.Properties().tab(FuturepackMain.tab_tools))), Constants.MOD_ID, "magnet_helmet");
	public static final Item magnet_chestplate = HelperItems.setRegistryName((new ItemMagnetArmor(ArmorMaterials.IRON, EquipmentSlot.CHEST, new Item.Properties().tab(FuturepackMain.tab_tools))), Constants.MOD_ID, "magnet_chestplate");
	public static final Item magnet_leggings = HelperItems.setRegistryName((new ItemMagnetArmor(ArmorMaterials.IRON, EquipmentSlot.LEGS, new Item.Properties().tab(FuturepackMain.tab_tools))), Constants.MOD_ID, "magnet_leggings");
	public static final Item magnet_boots = HelperItems.setRegistryName((new ItemMagnetArmor(ArmorMaterials.IRON, EquipmentSlot.FEET, new Item.Properties().tab(FuturepackMain.tab_tools))), Constants.MOD_ID, "magnet_boots");
	public static final Item composite_helmet = HelperItems.setRegistryName(new ItemCompositeArmor(FPArmorMaterials.COMPOSITE, EquipmentSlot.HEAD, 3, new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "composite_helmet");
	public static final Item composite_chestplate = HelperItems.setRegistryName(new ItemCompositeArmor(FPArmorMaterials.COMPOSITE, EquipmentSlot.CHEST, 4, new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "composite_chestplate");
	public static final Item composite_leggings = HelperItems.setRegistryName(new ItemCompositeArmor(FPArmorMaterials.COMPOSITE, EquipmentSlot.LEGS, 4, new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "composite_leggings");
	public static final Item composite_boots = HelperItems.setRegistryName(new ItemCompositeArmor(FPArmorMaterials.COMPOSITE, EquipmentSlot.FEET, 2, new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "composite_boots");
	public static final Item modul_battery = HelperItems.setRegistryName(new ItemModulBattery(10000, new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "modul_battery");    
    public static final Item modul_shield = HelperItems.setRegistryName(new ItemModulShield(new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "modul_shield");
    public static final Item modul_oxygen_mask = HelperItems.setRegistryName(new ItemModulOxygenMask(new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "modul_oxygen_mask");
    public static final Item modul_oxygen_tank = HelperItems.setRegistryName(new ItemModulOxygenTank(new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "modul_oxygen_tank");
    public static final Item modul_paraglider = HelperItems.setRegistryName(new ItemModulParaglider(new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "modul_paraglider");
    public static final Item modul_magnet = HelperItems.setRegistryName(new ItemModulMagnet(new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "modul_magnet");
    public static final Item modul_dynamo = HelperItems.setRegistryName(new ItemModulDynamo(new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "modul_dynamo");
    
    public static final Item oxygentank = HelperItems.setRegistryName(new ItemSauerstofftank(new Item.Properties().tab(FuturepackMain.tab_tools).defaultDurability(1)), Constants.MOD_ID, "oxygen_tank");
    public static final Item gleiter = HelperItems.setRegistryName(new ItemGleiterControler(new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "gleiter");
	
	public static final Item sword_neon = HelperItems.setRegistryName(new ItemNeonSwords(9F, new Item.Properties().tab(FuturepackMain.tab_tools).stacksTo(1)), Constants.MOD_ID, "sword_neon");
	public static final Item sword_retium = HelperItems.setRegistryName(new ItemNeonSwords(10F, new Item.Properties().tab(FuturepackMain.tab_tools).stacksTo(1)), Constants.MOD_ID, "sword_retium");
	public static final Item sword_glowtite = HelperItems.setRegistryName(new ItemNeonSwords(9F, new Item.Properties().tab(FuturepackMain.tab_tools).stacksTo(1)), Constants.MOD_ID, "sword_glowtite");
	public static final Item sword_bioterium = HelperItems.setRegistryName(new ItemNeonSwords(9F, new Item.Properties().tab(FuturepackMain.tab_tools).stacksTo(1)), Constants.MOD_ID, "sword_bioterium");
	
	public static final Item scrench = HelperItems.setRegistryName(new ItemScrench(new Item.Properties().tab(FuturepackMain.tab_tools).stacksTo(1)), Constants.MOD_ID, "scrench");
	public static final Item roomanalyzer = HelperItems.setRegistryName(new ItemRoomAnalyzer(new Item.Properties().tab(FuturepackMain.tab_tools).stacksTo(1)), Constants.MOD_ID, "room_analyzer");
	public static final Item composite_fishing_rod = HelperItems.setRegistryName(new ItemFpFishingRod(new Item.Properties().tab(FuturepackMain.tab_tools).stacksTo(1).defaultDurability(512)), Constants.MOD_ID, "composite_fishing_rod");
	
	public static final Item spawner_chest = HelperItems.setRegistryName(new ItemSpawnerChest(new Item.Properties().stacksTo(1).tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "spawner_chest");
	public static final Item minerbox = HelperItems.setRegistryName(new ItemMinerbox(new Item.Properties().stacksTo(64).tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "minerbox");
	public static final Item forstmasterbox = HelperItems.setRegistryName(new ItemForstmasterbox(new Item.Properties().stacksTo(64).tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "forstmasterbox");
	public static final Item monocartbox = HelperItems.setRegistryName(new ItemMonocartbox(new Item.Properties().stacksTo(64).tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "monocartbox");
	public static final Item entity_egger_full = HelperItems.setRegistryName(new ItemEntityEggerFull(new Item.Properties().stacksTo(1).rarity(Rarity.UNCOMMON)), Constants.MOD_ID, "entity_egger_full");
	public static final Item entity_egger = HelperItems.setRegistryName(new ItemEntityEgger(new Item.Properties().stacksTo(64).tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "entity_egger");
	public static final Item pearl_egger = HelperItems.setRegistryName(new ItemPearlEgger(new Item.Properties().stacksTo(1).rarity(Rarity.UNCOMMON).tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "pearl_egger");
	
	public static final Item laser_rifle = HelperItems.setRegistryName(new ItemLaserRiffle(new Item.Properties().stacksTo(1).tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "laser_rifle");
	public static final Item laser_bow = HelperItems.setRegistryName(new ItemLaserBow(new Item.Properties().stacksTo(1).defaultDurability(250).tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "laser_bow");
	public static final Item grenade_launcher = HelperItems.setRegistryName(new ItemGrenadeLauncher(new Item.Properties().stacksTo(1).tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "grenade_launcher");
	
	public static final Item modul_panzer_aramide = HelperItems.setRegistryName(new ItemModulPanzer(new Item.Properties().tab(FuturepackMain.tab_tools), FPArmorMaterials.COMPOSITE), Constants.MOD_ID, "modul_panzer_aramide");
	public static final Item modul_panzer_iron = HelperItems.setRegistryName(new ItemModulPanzer(new Item.Properties().tab(FuturepackMain.tab_tools), ArmorMaterials.IRON), Constants.MOD_ID, "modul_panzer_iron");
	public static final Item modul_panzer_diamond = HelperItems.setRegistryName(new ItemModulPanzer(new Item.Properties().tab(FuturepackMain.tab_tools), ArmorMaterials.DIAMOND), Constants.MOD_ID, "modul_panzer_diamond");
	public static final Item modul_panzer_netherite = HelperItems.setRegistryName(new ItemModulPanzer(new Item.Properties().tab(FuturepackMain.tab_tools), ArmorMaterials.NETHERITE), Constants.MOD_ID, "modul_panzer_netherite");
	public static final Item modul_healthboost_1 = HelperItems.setRegistryName(new ItemModulHealthboost(new Item.Properties().tab(FuturepackMain.tab_tools), 2.0), Constants.MOD_ID, "modul_healthboost_1");
	public static final Item modul_fire_shield = HelperItems.setRegistryName(new ItemModulFireShield(new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "modul_fire_shield");
	public static final Item modul_reanimation = HelperItems.setRegistryName(new ItemModulReanimation(new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "modul_reanimation");
	public static final Item modul_solar_helmet = HelperItems.setRegistryName(new ItemModulSolarHelmet(new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "modul_solar_helmet");
	public static final Item modul_gravity = HelperItems.setRegistryName(new ItemModulGravity(new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "modul_gravity");
	public static final Item modul_bubble_jet = HelperItems.setRegistryName(new ItemModulBubbleJet(new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "modul_bubble_jet");    

	public static final Item chainsaw_mk1 = HelperItems.setRegistryName(new ItemChainsaw(new Item.Properties().tab(FuturepackMain.tab_tools), Tiers.IRON), Constants.MOD_ID, "chainsaw_mk1");
	public static final Item drill_mk1 = HelperItems.setRegistryName(new ItemDrillMK1(new Item.Properties().tab(FuturepackMain.tab_tools), Tiers.IRON), Constants.MOD_ID, "drill_mk1");
	public static final Item drill_mk2 = HelperItems.setRegistryName(new ItemDrillMK2(new Item.Properties().tab(FuturepackMain.tab_tools), Tiers.DIAMOND), Constants.MOD_ID, "drill_mk2");
	public static final Item sword_composite = HelperItems.setRegistryName(new SwordItem(FPToolMaterials.COMPOSITE, 3, -2.4F, new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "sword_composite");
	public static final Item mindcontrol_mining_helmet = HelperItems.setRegistryName((new ItemMindControllMiningHelmet(ArmorMaterials.IRON, EquipmentSlot.HEAD, new Item.Properties().tab(FuturepackMain.tab_tools))), Constants.MOD_ID, "mindcontrol_mining_helmet");
	
	public static final Item dungeon_helmet = HelperItems.setRegistryName(new ItemDungeonArmor(FPArmorMaterials.COMPOSITE, EquipmentSlot.HEAD, 3, new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "dungeon_helmet");
	public static final Item dungeon_chestplate = HelperItems.setRegistryName(new ItemDungeonArmor(FPArmorMaterials.COMPOSITE, EquipmentSlot.CHEST, 4, new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "dungeon_chestplate");
	public static final Item dungeon_leggings = HelperItems.setRegistryName(new ItemDungeonArmor(FPArmorMaterials.COMPOSITE, EquipmentSlot.LEGS, 4, new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "dungeon_leggings");
	public static final Item dungeon_boots = HelperItems.setRegistryName(new ItemDungeonArmor(FPArmorMaterials.COMPOSITE, EquipmentSlot.FEET, 2, new Item.Properties().tab(FuturepackMain.tab_tools)), Constants.MOD_ID, "dungeon_boots");
	
	public static void register(RegistryEvent.Register<Item> event)
	{
		IForgeRegistry<Item> r = event.getRegistry();
		
		r.registerAll(airBrush, grappling_hook, plasmaschneider, hologramControler, logisticEditor, escanner);
		r.registerAll(composite_pickaxe, composite_axe, composite_hoe, composite_spade);
		r.registerAll(magnet_helmet, magnet_chestplate, magnet_leggings, magnet_boots);
		r.registerAll(composite_helmet, composite_chestplate, composite_leggings, composite_boots);
		r.registerAll(modul_battery, modul_shield, modul_oxygen_mask, modul_oxygen_tank, modul_paraglider, modul_magnet, modul_dynamo);
		r.registerAll(oxygentank,gleiter);
		r.registerAll(sword_neon, sword_retium, sword_glowtite, sword_bioterium);
		r.registerAll(scrench, roomanalyzer, composite_fishing_rod);
		r.registerAll(spawner_chest, minerbox, forstmasterbox, monocartbox, entity_egger_full, entity_egger, pearl_egger);
		r.registerAll(laser_rifle, laser_bow, grenade_launcher);
		r.registerAll(modul_panzer_aramide, modul_panzer_iron, modul_panzer_diamond, modul_panzer_netherite, modul_healthboost_1, modul_fire_shield, modul_reanimation, modul_solar_helmet, modul_gravity, modul_bubble_jet);
		r.registerAll(chainsaw_mk1, drill_mk1, drill_mk2);
		r.registerAll(sword_composite, mindcontrol_mining_helmet);
		r.registerAll(dungeon_helmet, dungeon_chestplate, dungeon_leggings, dungeon_boots);
		
	}
}
