package futurepack.world.gen.carver;

public class SuperCanyoneWorldCaver extends LargeCanyonWorldCaver
{
	public SuperCanyoneWorldCaver() 
	{
		scaleYRange = 1.2F;
		scaleplacementYBound = 4F;
		scaleplacementXZBound = 3F;
		closeTop = false;
	}
}
