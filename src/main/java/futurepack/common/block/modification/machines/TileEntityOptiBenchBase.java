package futurepack.common.block.modification.machines;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.WorldlyContainer;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public abstract class TileEntityOptiBenchBase extends TileEntityMachineSupport implements WorldlyContainer, ITilePropertyStorage
{
	protected int progress = 0;//max 10

	public TileEntityOptiBenchBase(BlockEntityType<? extends TileEntityOptiBenchBase> type, BlockPos pos, BlockState state)
	{
		super(type, 1024, EnumEnergyMode.PRODUCE, pos, state);
	}
	
	@Override
	public void configureLogisticStorage(LogisticStorage storage) 
	{
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ITEMS);
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ENERGIE);
		storage.setDefaut(EnumLogisticIO.OUT, EnumLogisticType.SUPPORT);
		storage.removeState(EnumLogisticIO.OUT, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.IN, EnumLogisticType.SUPPORT);
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.SUPPORT);
		storage.setModeForFace(Direction.DOWN, EnumLogisticIO.OUT, EnumLogisticType.ITEMS);
		
	}
	
	@Override
	protected EnumLogisticType[] getLogisticTypes()
	{
		return new EnumLogisticType[]{EnumLogisticType.ENERGIE, EnumLogisticType.ITEMS, EnumLogisticType.SUPPORT};
	}

	@Override
	public boolean isWorking()
	{
		return progress >0;
	}
	
	public abstract void updateRecipe();

	//SAVE
	

	
	@Override
	public void writeDataSynced(CompoundTag nbt)
	{
		super.writeDataSynced(nbt);	
		nbt.putInt("progress", progress);
	}
	

	
	@Override
	public void readDataSynced(CompoundTag nbt)
	{		
		super.readDataSynced(nbt);
		progress = nbt.getInt("progress");
	}
	
	//BASES
	
	/** 0..8: crafting grid
	 *  9   : output
	 *  10  : recipe
	 *  11  : AI storage
	 */
	@Override
	protected int getInventorySize()
	{
		return 12;
	}
	
	@Override
	public void setItem(int var1, ItemStack var2) 
	{
		super.setItem(var1, var2);
		updateRecipe();
	}
	
	@Override
	public EnumEnergyMode getEnergyType()
	{
		return EnumEnergyMode.USE;
	}
	
	@Override
	public int getProperty(int id) 
	{
		switch (id)
		{
		case 0:		
			return this.progress;
		case 1:		
			return this.energy.get();
		case 2:		
			return this.support.get();
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:		
			this.progress = value;
			break;
		case 1:		
			setEnergy(value);
			break;
		case 2:		
			int d = value - support.get();
			if(d>0)
				support.add(d);
			else
				support.use(-d);
			break;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount() 
	{
		return 3;
	}

	
	@Override
	public abstract boolean canPlaceItem(int var1, ItemStack var2);

	@Override
	public int[] getSlotsForFace(Direction var1)
	{
		return new int[]{0,1,2,3,4,5,6,7,8,9};
	}

	@Override
	public boolean canPlaceItemThroughFace(int var1, ItemStack var2, Direction side)
	{
		if(!storage.canInsert(side, EnumLogisticType.ITEMS))
		{
			return false;
		}
		return var1 >= 0 && var1 <= 8 && canPlaceItem(var1, var2);
	}

	@Override
	public boolean canTakeItemThroughFace(int var1, ItemStack var2, Direction side)
	{
		if(!storage.canExtract(side, EnumLogisticType.ITEMS))
		{
			return false;
		}
		return var1 == 9;
	}
	
	
}
