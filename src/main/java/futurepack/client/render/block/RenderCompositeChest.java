package futurepack.client.render.block;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.common.block.inventory.InventoryBlocks;
import futurepack.common.block.inventory.TileEntityCompositeChest;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.Sheets;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.client.renderer.blockentity.ChestRenderer;
import net.minecraft.client.resources.model.Material;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.entity.ChestBlockEntity;
import net.minecraft.world.level.block.state.properties.ChestType;

public class RenderCompositeChest<T extends ChestBlockEntity> extends ChestRenderer<T>
{
	public static final ResourceLocation TEXTURE_NORMAL = new ResourceLocation(Constants.MOD_ID, "model/comp_normal");
	public static final ResourceLocation TEXTURE_NORMAL_LEFT = new ResourceLocation(Constants.MOD_ID, "model/comp_normal_left");
	public static final ResourceLocation TEXTURE_NORMAL_RIGHT = new ResourceLocation(Constants.MOD_ID, "model/comp_normal_right");
	
	public static final Material NORMAL_MATERIAL = new Material(Sheets.CHEST_SHEET, TEXTURE_NORMAL);
	public static final Material NORMAL_MATERIAL_LEFT = new Material(Sheets.CHEST_SHEET, TEXTURE_NORMAL_LEFT);
	public static final Material NORMAL_MATERIAL_RIGHT = new Material(Sheets.CHEST_SHEET, TEXTURE_NORMAL_RIGHT);
	
	private final TileEntityCompositeChest chest = new TileEntityCompositeChest(new BlockPos(0,0,0), InventoryBlocks.composite_chest.defaultBlockState());
	
	public RenderCompositeChest(BlockEntityRendererProvider.Context renderDispatcher) {
		super(renderDispatcher);
	}
	
	@Override
	public void render(T te, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int combinedLightIn, int combinedOverlayIn) 
	{
		if(te==null)
		{
			te = (T) chest;
		}
		super.render(te, partialTicks, matrixStackIn, bufferIn, combinedLightIn, combinedOverlayIn);
	}
	
	@Override
	protected Material getMaterial(T tileEntity, ChestType chestType) {
		Material m = Sheets.chooseMaterial(tileEntity, chestType, false);
		
		
		if(m.texture().getPath().equals("entity/chest/normal")) {
			return NORMAL_MATERIAL;
		}
		else if(m.texture().getPath().equals("entity/chest/normal_left")) {
			return NORMAL_MATERIAL_LEFT;
		}
		else if(m.texture().getPath().equals("entity/chest/normal_right")) {
			return NORMAL_MATERIAL_RIGHT;
		}
		
		return m;
	}
}
