package futurepack.common.recipes;

import java.util.function.BiPredicate;

import net.minecraft.core.NonNullList;
import net.minecraft.world.Container;
import net.minecraft.world.ContainerHelper;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;

public class SingletonInventory implements Container
{
	private NonNullList<ItemStack> list;
	
	public BiPredicate<Integer, ItemStack> validForSlot = (i,s) -> true;
	
	public SingletonInventory(ItemStack...items)
	{
		list = NonNullList.of(ItemStack.EMPTY, items);
	}

	@Override
	public int getContainerSize()
	{
		return list.size();
	}

	@Override
	public boolean isEmpty()
	{
		return list.isEmpty();
	}

	@Override
	public ItemStack getItem(int index)
	{
		return list.get(index);
	}

	@Override
	public ItemStack removeItem(int index, int count)
	{
		return ContainerHelper.removeItem(list, index, count);
	}

	@Override
	public ItemStack removeItemNoUpdate(int index) 
	{
		return ContainerHelper.takeItem(this.list, index);
	}

	@Override
	public void setItem(int index, ItemStack stack)
	{
		list.set(index, stack);
	}

	@Override
	public int getMaxStackSize()
	{
		return 64;
	}

	@Override
	public void setChanged()
	{
		
	}

	@Override
	public boolean stillValid(Player player)
	{
		return true;
	}

	@Override
	public void startOpen(Player player)
	{
	}

	@Override
	public void stopOpen(Player player)
	{
		
	}

	@Override
	public boolean canPlaceItem(int index, ItemStack stack)
	{
		return validForSlot.test(index, stack);
	}

	@Override
	public void clearContent() 
	{
		
	}

}
