package futurepack.common.block.modification;

import java.util.function.Supplier;

import futurepack.api.interfaces.IBlockBothSidesTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperBoundingBoxes;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.DirectionalBlock;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockFluidPump extends BlockRotateableTile implements IBlockBothSidesTickingEntity<TileEntityFluidPump>
{
	private static final Supplier<double[][]> shape = () -> new double[][]{
		{6,6,0,10,10,16},
		{0,0,1, 5,16,10},
		{5,2,2, 16,14,14}
	};
	public static final VoxelShape SHAPE_UP = HelperBoundingBoxes.createBlockShape(shape.get(), 90, 0);
	private final VoxelShape SHAPE_DOWN = HelperBoundingBoxes.createBlockShape(shape.get(), 270F, 0F);
	private final VoxelShape SHAPE_NORTH = HelperBoundingBoxes.createBlockShape(shape.get(), 0F, 180F);
	private final VoxelShape SHAPE_SOUTH = HelperBoundingBoxes.createBlockShape(shape.get(), 0F, 0F);
	private final VoxelShape SHAPE_WEST = HelperBoundingBoxes.createBlockShape(shape.get(), 0F, 270F);
	private final VoxelShape SHAPE_EAST = HelperBoundingBoxes.createBlockShape(shape.get(), 0F, 90F);
	
	
	public BlockFluidPump(Block.Properties props) 
	{
		super(props);
	}
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context) 
	{
		switch(state.getValue(FACING))
		{
		case DOWN:
			return SHAPE_DOWN;
		case NORTH:
			return SHAPE_NORTH;
		case SOUTH:
			return SHAPE_SOUTH;
		case WEST:
			return SHAPE_WEST;
		case EAST:
			return SHAPE_EAST;
		case UP:
		default:
			return SHAPE_UP;
		}
	}
	
	@Override
	public InteractionResult use(BlockState state, Level worldIn, BlockPos pos, Player pl, InteractionHand handIn, BlockHitResult hit) 
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.FLUID_PUMP.openGui(pl, pos);
		}
		return InteractionResult.SUCCESS;
	}
	
	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		Direction f = context.getNearestLookingDirection();
		if(f != Direction.UP && f != Direction.DOWN)
		{
			f = f.getCounterClockWise();
		}
		return defaultBlockState().setValue(DirectionalBlock.FACING, f);
	}
	
	@Override
	public RenderShape getRenderShape(BlockState state)
    {
        return RenderShape.MODEL;
    }

	@Override
	public BlockEntityType<TileEntityFluidPump> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.FLUID_PUMP;
	}
}
