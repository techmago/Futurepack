package futurepack.world.gen.feature;

import java.util.Random;
import java.util.Set;
import java.util.function.Predicate;

import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.LevelSimulatedRW;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.level.levelgen.feature.configurations.TreeConfiguration;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.Vec3;

public class TyrosTreeFeature extends AbstractTyrosTreeFeature<TreeConfiguration>
{
//	int leaveGen;
	
	public TyrosTreeFeature(Codec<TreeConfiguration> codec)
	{
		super(codec);
	}

	public boolean generate(Set<BlockPos> changedLogs, Set<BlockPos> changedLeaves, LevelSimulatedRW world, Random rand, BlockPos pos, BoundingBox box, TreeConfiguration config) 
	{
		if(!canStand(world, pos, changedLogs, changedLeaves, rand, box, config))
			return false;
        pos = pos.offset(2, 0, 2);
		
		int height = 20  + rand.nextInt(15);//fixed
		double d = rand.nextDouble() * Math.PI;//
		int parts = 20 + rand.nextInt(10);//fixed
		int leaveGen = 2 + rand.nextInt(2);//fixed

		genTreeStemm(changedLogs, world, pos, height, box, config, rand);
		pos = pos.offset(0, height, 0);	
		
		genTreetop(changedLogs, changedLeaves, world, rand, pos, d, parts, leaveGen, box, config);
		
		return true;
	}

	private void genTreetop(Set<BlockPos> changedLogs, Set<BlockPos> changedLeaves, LevelSimulatedRW world, Random rand, BlockPos pos, double d, int parts, int leaveGen, BoundingBox box, TreeConfiguration config) 
	{
		for(int i=0 ; i<parts ; i++)
		{
			double y = (i + 1.0) / parts;    
			double r = Math.sqrt(1.0 - y*y);
			double phi = i * Math.PI * (3 - Math.sqrt(5.0));
		            
		    double x = Math.cos(phi+d) * r;
		    double z = Math.sin(phi+d) * r;
		    
		    int radius = parts +3;
		    
		    Vec3 vec = new Vec3(x*radius, y*radius, z*radius);
		    genBough(changedLogs, changedLeaves, world, pos, pos.offset( new BlockPos( vec ) ) , leaveGen, rand, true, box, config);
		}
	}
	
	private boolean canStand(LevelSimulatedRW w, BlockPos pos, Set<BlockPos> changedLogs, Set<BlockPos> changedLeaves, Random r, BoundingBox box, TreeConfiguration config)
	{
		if(isGround(w, pos.below()))
		{
			if(isGround(w, pos.offset(2,-1,2)) && isGround(w, pos.offset(4,-1,4)))
			{
				if(isGround(w, pos.offset(4,-1,0)) && isGround(w, pos.offset(0,-1,4)))
				{
					return true;
				}
			}
		}
		else if(r.nextInt(5)==0)
		{
			BlockPos deep = pos.below();
			Predicate<BlockState> isWater = s -> s.getMaterial() == Material.WATER;
			if(w.isStateAtPosition(deep, isWater))
			{
				
				while(w.isStateAtPosition(deep, isWater))
				{
					deep = deep.below();
				}
				int absoulute = pos.getY() - deep.getY();
				int arms = 4 + absoulute/2;
				
				for(int i=0;i< arms;i++)
				{
					BlockPos start = pos.offset(r.nextInt(6)-3, 0, r.nextInt(6)-3);
					BlockPos end = deep.offset(r.nextInt(absoulute +5)-r.nextInt(absoulute +5), 0, r.nextInt(absoulute +5)-r.nextInt(absoulute +5));
					genBough(changedLogs, changedLeaves, w, start, end, 0, r, false, box, config);
				}
				return true;
			}
		}
		return false;
	}
	
	@Override
	protected double getRadius(int i, int h)
	{
		return (1.0 - i/(double)h)*1.5 +1.5;
	}

	private boolean isGround(LevelSimulatedRW w, BlockPos pos)
	{
		return w.isStateAtPosition(pos, s -> s.getMaterial() == Material.DIRT || s.getMaterial() == Material.GRASS); //organic becuase grass
	}

	public boolean place(WorldGenLevel reader, ChunkGenerator generator, Random rand, BlockPos pos, TreeConfiguration config) 
	{
		return generate(null, null, reader, rand, pos, new BoundingBox(pos), config);
	}

	@Override
	public boolean place(FeaturePlaceContext<TreeConfiguration> pContext) 
	{
		return place(pContext.level(), pContext.chunkGenerator(), pContext.random(), pContext.origin(), pContext.config());
	}
}
