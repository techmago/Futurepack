package futurepack.common.recipes.crafting;

import com.google.gson.JsonObject;
import com.mojang.datafixers.util.Pair;

import futurepack.api.Constants;
import net.minecraft.core.NonNullList;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.CraftingContainer;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.ShapelessRecipe;
import net.minecraft.world.level.Level;

public class ShapelessRecipeWithResearch extends ShapelessRecipe
{

	public ShapelessRecipeWithResearch(ResourceLocation id, String group, ItemStack result, NonNullList<Ingredient> inputs)
	{
		super(id, group, result, inputs);		
	}
	
	public ShapelessRecipeWithResearch(ShapelessRecipe base)
	{
		this(base.getId(), base.getGroup(), base.getResultItem(), base.getIngredients());
	}

	@Override
	public boolean matches(CraftingContainer inv, Level world)
	{
		if(inv instanceof InventoryCraftingForResearch)
		{
			InventoryCraftingForResearch research = (InventoryCraftingForResearch) inv;
			
			if(isUseable(research.getUser(), this.getResultItem(), world))
			{
				if(super.matches(inv, world))
				{
					return true;
				}
			}				
		}
		else
		{
			Pair<Player, Boolean> pl = ShapedRecipeWithResearch.getPlayerFromWorkbench(inv);
			if(pl!=null)
			{
				if(pl.getSecond() || (pl.getFirst() !=null && isUseable(pl.getFirst(), this.getResultItem(), world)))
				{
					return super.matches(inv, world);	
				}				
			}
		}
		return false;
	}
	
	public boolean isLocalResearched()
	{
		return isUseable(null, this.getResultItem(), null);
	}
	
	private static boolean isUseable(Object id, ItemStack it, Level w)
	{
		return ShapedRecipeWithResearch.isUseable(id, it, w);
	}
	
	public static class Factory extends ShapelessRecipe.Serializer
	{
		public static final ResourceLocation NAME = new ResourceLocation(Constants.MOD_ID, "research_shapeless");
		
		@Override
		public ShapelessRecipeWithResearch fromJson(ResourceLocation recipeId, JsonObject json)
		{
			return new ShapelessRecipeWithResearch(super.fromJson(recipeId, json));
		}

		@Override
		public ShapelessRecipeWithResearch fromNetwork(ResourceLocation recipeId, FriendlyByteBuf buffer)
		{
			return new ShapelessRecipeWithResearch(super.fromNetwork(recipeId, buffer));
		}
	}
	
	@Override
	public RecipeSerializer<?> getSerializer()
	{
		return FPSerializers.RESEARCH_SHAPELESS;
	}
}
