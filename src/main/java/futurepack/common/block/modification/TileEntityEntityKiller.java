package futurepack.common.block.modification;

import futurepack.api.Constants;
import futurepack.common.FPTileEntitys;
import futurepack.common.modification.EnumChipType;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityEntityKiller extends TileEntityLaserBase<Mob>
{
	
	public TileEntityEntityKiller(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.ENTITY_KILLER, Mob.class, pos , state);
	}

	
	@Override
	public boolean isEntityValid(Mob entity)
	{
		if(!matchConfig(entity))
			return false;
		
		if(getConfiguration("kill.not"))
		{
			return entity.getHealth() > 1F;
		}
		return entity.isAlive();
	}

	@Override
	public void progressEntity(Mob entity)
	{
		float attack = Math.min(this.energy.get()/10, entity.getHealth());
		
		if(getConfiguration("kill.not"))
			attack-=0.5;
		else
			attack++;
		
		if(attack > 0)
		{
			int power = (int)((attack*10) / (1 + (getChipPower(EnumChipType.INDUSTRIE)/4.0)));
			energy.use(power);
			attackEntity(entity, attack);
		}
	}

	@Override
	public boolean shouldWork()
	{
		return energy.get() > 10;
	}

	@Override
	public ResourceLocation getTexture()
	{
		return new ResourceLocation(Constants.MOD_ID, "textures/model/eater_2.png");
	}

	@Override
	public int getLaserColor()
	{
		return 0xff2403;
	}
	
	@Override
	public ResourceLocation getLaser()
	{
		return new ResourceLocation(Constants.MOD_ID, "textures/model/laser_killer.png");
	}

}
