package futurepack.common;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import net.minecraftforge.fml.common.Mod.Instance;

public class MorpheusAPI
{
	public static final String morpheusID = "Morpheus";
	
	@Instance(value = morpheusID, owner = Constants.MOD_ID)
	public static Object morpheus;
	
	
	public static Object morpheusAPI;
	private static Method register;
	
	public static void init()
	{
		if(morpheus!=null)
		{
			Class cmorph = morpheus.getClass();
			try
			{
				Field f =cmorph.getField("register");
				Object reg = f.get(morpheus);
				if(reg.getClass().getName().equals("net.quetzi.morpheus.MorpheusRegistry"))
				{
					morpheusAPI = reg;
										
					Class cls = Class.forName("net.quetzi.morpheus.api.INewDayHandler");
					
					register = morpheusAPI.getClass().getMethod("registerHandler", cls, int.class);			// void registerHandler(INewDayHandler newdayhandler, int dimension);
				}
			}
			catch (NoSuchFieldException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
			catch (ClassNotFoundException e) 
			{
				e.printStackTrace();
			}
		}
		else
		{
			FPLog.logger.warn("It seems like Morpheus is not installed.");
		}
		
	}

	public static void registerHandler(Object provider, int dimID)
	{
		init();
		if(morpheusAPI!=null)
		{
			try {
				register.invoke(morpheusAPI, provider, dimID);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
	}
}
