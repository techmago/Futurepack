package futurepack.world.loot;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;

import futurepack.common.FPLootFunctions;
import futurepack.common.item.ItemChip;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.GsonHelper;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.Serializer;
import net.minecraft.world.level.storage.loot.functions.LootItemFunction;
import net.minecraft.world.level.storage.loot.functions.LootItemFunctionType;
import net.minecraft.world.level.storage.loot.providers.number.NumberProvider;

public class LootFunctionSetupChip implements LootItemFunction
{
	private final Boolean isToasted;
    private final NumberProvider power;
//    private final RandomValueRange type;
    private final String[] chipid;

    public LootFunctionSetupChip(Boolean toastedIn, NumberProvider powerRangeIn, String...chipidIn)
    {
        isToasted = toastedIn;
//        type = typeRangeIn;
        power = powerRangeIn;
        chipid = chipidIn;
    }

    @Override
	public ItemStack apply(ItemStack stack, LootContext context)
    {
    	CompoundTag innerNBT = new CompoundTag();
    	
//    	if(power.getMax() > 10 || power.getMin() < 0)
//    		FPLog.logger.warn("Chip power is out of normal range!");
    		
    	innerNBT.putFloat("power", (power.getInt(context)*0.2F) + 1);
    	
    	if(!isToasted)
    	{
    		if(ItemChip.getChip(stack)!=null)
    		{
    			stack.setTag(innerNBT);
    		}
    		else
    			throw new IllegalStateException("Only chips are working! but got " + stack);
    	}
    	else
    	{
    		CompoundTag outerNBT = new CompoundTag();
    		outerNBT.put("tag", innerNBT);
    		
    		if(chipid!=null)
    		{
	    		if(chipid.length >= 2)
	    		{
	        		outerNBT.putString("id", chipid[context.getRandom().nextInt(chipid.length)]);
	    		}
	    		else if(chipid.length == 1)
	    		{
	    			outerNBT.putString("id", chipid[0]);
	    		}
    		}
    		outerNBT.putBoolean("toasted", true);
//    		outerNBT.putInt("Damage", type.generateInt(context.getRandom())); //damage is item meta
    		
    		
    		outerNBT.putInt("Count", 1);
    		stack.setTag(outerNBT);
//    		stack.setItemDamage(ComputerItems.toasted_chip);
    	}

        return stack;
    }

    public static class Storage implements Serializer<LootFunctionSetupChip>
    {
            @Override
			public void serialize(JsonObject object, LootFunctionSetupChip functionClazz, JsonSerializationContext serializationContext)
            {
            	object.add("toasted", serializationContext.serialize(functionClazz.isToasted));
//            	object.add("type", serializationContext.serialize(functionClazz.type));
            	object.add("power", serializationContext.serialize(functionClazz.power));
            	if(functionClazz.isToasted)
            		object.add("chipid", serializationContext.serialize(functionClazz.chipid));
            }

            @Override
			public LootFunctionSetupChip deserialize(JsonObject object, JsonDeserializationContext deserializationContext)
            {
            	Boolean toasted = GsonHelper.getAsObject(object, "toasted", deserializationContext, Boolean.class);
//            	RandomValueRange type = JSONUtils.deserializeClass(object, "type", deserializationContext, RandomValueRange.class);
            	NumberProvider power = GsonHelper.getAsObject(object, "power", deserializationContext, NumberProvider.class);
            	String[] chipid = null;
            	if(toasted)
            	{
            		chipid = GsonHelper.getAsObject(object, "chipid", deserializationContext, String[].class);
            	}
            	
                return new LootFunctionSetupChip(toasted, power, chipid);
            }
    }

	@Override
	public LootItemFunctionType getType() 
	{
		return FPLootFunctions.SETUP_CHIP.get();
	}
    
}
