package futurepack.common.block.misc;

import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.player.FakePlayerFactory;
import futurepack.common.player.FuturepackPlayer;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import futurepack.depend.api.helper.HelperInventory;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;


public class TileEntitySaplingHolder extends FPTileEntityBase implements ITileServerTickable
{
	//@Nullable NO NO NO!!!!!
	private ItemStack filter = ItemStack.EMPTY;
	
	private SaplingInserter itemHandler = new SaplingInserter();
	
	public TileEntitySaplingHolder(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.SAPLING_HOLDER, pos, state);
	}
	
	public ItemStack getFilter()
	{
		return filter;
	}
	
	@Override
	public void tickServer(Level pLevel, BlockPos pPos, BlockState pState)
	{
		if(itemHandler.contained!=null)
		{
			ItemStack it = itemHandler.contained;
			itemHandler.contained = tryPlace(it);
			if(itemHandler.contained.isEmpty())
			{
				itemHandler.contained=ItemStack.EMPTY;
			}
		}
	}
	
	
	public void setFilter(ItemStack it)
	{
		filter = it;
		filter.setCount(1);
		setChanged();
	}
	

	

	

	
	public ItemStack tryPlace(ItemStack it)
	{
		if(!level.isClientSide)
		{
			if(!level.isEmptyBlock(worldPosition.above()))
				return it;
			
			if(filter.isEmpty() || HelperInventory.areItemsEqualNoSize(it, filter))
			{
				Direction face = Direction.DOWN;
				
				FuturepackPlayer pl = FakePlayerFactory.INSTANCE.getPlayer((ServerLevel) level);
				FakePlayerFactory.setupPlayer(pl, worldPosition.above(1), face);
				pl.startUsingItem(InteractionHand.MAIN_HAND);
				pl.setItemInHand(InteractionHand.MAIN_HAND, it);
				return FakePlayerFactory.itemClick(level, worldPosition.above(1), face.getOpposite(), it, pl, InteractionHand.MAIN_HAND).getObject();
			}
		}
		return it;
	}

	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		if(!filter.isEmpty())
		{
			CompoundTag tagt = new CompoundTag();
			filter.save(tagt);
			nbt.put("filter", tagt);
		}
		
		nbt.put("items", itemHandler.serializeNBT());
		return super.writeDataUnsynced(nbt);
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		if(nbt.contains("filter"))
		{
			filter = ItemStack.of(nbt.getCompound("filter"));
		}
		itemHandler.deserializeNBT(nbt.getCompound("items"));
		super.readDataUnsynced(nbt);
	}
	
	private LazyOptional<IItemHandler> item_opt;
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if(item_opt != null)
			{
				return (LazyOptional<T>) item_opt;
			}
			else
			{
				item_opt = LazyOptional.of(() -> itemHandler);
				item_opt.addListener(p -> item_opt = null);
				return (LazyOptional<T>) item_opt;
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(item_opt);
		super.setRemoved();
	}
	
	public class SaplingInserter implements IItemHandler, INBTSerializable<CompoundTag>
	{
		private ItemStack contained = ItemStack.EMPTY;
		
		@Override
		public int getSlots() 
		{
			
			return 1;
		}
		
		@Override
		public ItemStack getStackInSlot(int slot)
		{
			return contained;
		}
		
		@Override
		public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
		{
			if(stack.isEmpty())
				return ItemStack.EMPTY;
				
			if(isItemValid(slot, stack) && contained.isEmpty())
			{
				if(simulate)
				{
					ItemStack st = stack.copy();
					st.split(1);
					return st;
				}
				else
				{
					ItemStack st = stack.copy();
					contained = st.split(1);
					return st;
				}			
			}
			return stack;
		}

		@Override
		public ItemStack extractItem(int slot, int amount, boolean simulate) 
		{
			return ItemStack.EMPTY;
		}

		@Override
		public CompoundTag serializeNBT()
		{
			CompoundTag nbt = new CompoundTag();
			if(!contained.isEmpty())
			{
				contained.save(nbt);
			}
			return nbt;
		}

		@Override
		public void deserializeNBT(CompoundTag nbt)
		{
			contained = ItemStack.of(nbt);
		}

		@Override
		public int getSlotLimit(int slot)
		{
			return 1;
		}
		
		@Override
		public boolean isItemValid(int slot, ItemStack stack) 
		{
			return HelperInventory.areItemsEqualNoSize(stack, filter);
		}
		
	}
}
