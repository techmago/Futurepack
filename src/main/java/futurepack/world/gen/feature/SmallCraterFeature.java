package futurepack.world.gen.feature;

import java.util.Random;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.Heightmap.Types;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.level.levelgen.feature.configurations.NoneFeatureConfiguration;

public class SmallCraterFeature extends Feature<NoneFeatureConfiguration> 
{
	//this will be the holes

	public SmallCraterFeature()
	{
		super(NoneFeatureConfiguration.CODEC);
	}

	@Override
	public boolean place(FeaturePlaceContext<NoneFeatureConfiguration> pContext) 
	{
		return place(pContext.level(), pContext.chunkGenerator(), pContext.random(), pContext.origin(), pContext.config());
	}
	
	public boolean place(WorldGenLevel w, ChunkGenerator p_241855_2_, Random rand, BlockPos pos, NoneFeatureConfiguration p_241855_5_) 
	{
		BlockPos blockpos = w.getHeightmapPos(Types.WORLD_SURFACE_WG, pos.offset(rand.nextInt(8) - rand.nextInt(8), -1, rand.nextInt(8) - rand.nextInt(8)));
        int r = 5 + rand.nextInt(5);
		
        boolean water = false;
        
        for(int k=r-1;k>=-r;k--)
		{
        	for(int j=-r;j<+r;j++)
        	{
				for(int l=-r;l<+r;l++)
				{
					if((j*j + k*k + l*l) <= r*r+(rand.nextInt(3))-1)
					{
						BlockPos p = blockpos.offset(j,k,l);
						if(water)
							w.setBlock(p, Blocks.WATER.defaultBlockState(), 3);
						else
						{
							water = w.isWaterAt(p);
							w.removeBlock(p, false);
						}
							
					}
				}
			}
		}
		
		return true;
	}
}
