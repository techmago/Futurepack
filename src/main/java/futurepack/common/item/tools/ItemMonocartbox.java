package futurepack.common.item.tools;

import futurepack.common.block.logistic.monorail.BlockMonorailBasic;
import futurepack.common.entity.monocart.EntityMonocart;
import net.minecraft.core.BlockPos;
import net.minecraft.util.Mth;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;

public class ItemMonocartbox extends Item 
{

	public ItemMonocartbox(Properties properties) 
	{
		super(properties);
	}

	@Override
	public InteractionResult useOn(UseOnContext context)
	{
		Level w = context.getLevel();
		if(!w.isClientSide)
		{
			ItemStack it = context.getItemInHand();
			BlockPos pos = context.getClickedPos();
			
			if(BlockMonorailBasic.isMonorail(w.getBlockState(pos)))
			{
				Entity cart = new EntityMonocart(w);
				if(it.hasTag() && it.getTag().contains("entity"))
				{
					cart.load(it.getTag().getCompound("entity"));
					cart.setUUID(Mth.createInsecureUUID(w.random));
				}
				cart.setPos(pos.getX()+0.5, pos.getY()+0.2, pos.getZ()+0.5);
				w.addFreshEntity(cart);
				it.shrink(1);
				return InteractionResult.SUCCESS;
			}
		}
		return super.useOn(context);
	}
}
