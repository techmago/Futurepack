package futurepack.extensions.computercraft;

public abstract class FilterAccessBase<T> implements IFilterAccess 
{
	protected final T filter;
	
	public FilterAccessBase(T block) 
	{
		this.filter = block;
	}

}
